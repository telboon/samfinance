#! /usr/bin/env python3

import os
from datetime import datetime
from datetime import timedelta
from samFinance import *
from samFinance.samMail import sendmail
from datetime import time
from time import sleep
import subprocess


longFile="samRunFull.py"
shortFile="samRunShort.py"
textFile="statusOutput.txt"

websiteName="http://test.samuelpua.com/samFinance/results"

#sensitivity mins
senMin=60

#weekday list
weekdayList=[1,2,3,4,5]

#15 mins short cycle
shortCycle=15*60

#trading hours
startShort=time(8,30)
endShort=time(17,30)

#Full Running hours, at twice a day
fullTimes=[time(7,30),
time(19,30)]
timePass=[0]*len(fullTimes)
lastFullRun=None

def runScript(fileToRun):
	print("=============================================================================")
	timeNow=datetime.now()
	startStr="Time Now: "+timeNow.strftime("%Y%m%d %H:%M.%S")+"\n"
	print("Running script...")
	print()
	proc=subprocess.Popen(["python3",fileToRun], stdout=subprocess.PIPE)
	(out,err)=proc.communicate()
	out=str(out)
	out=out.replace("\\n","\n")
	out=out[2:]
	out=out[:-1]
	timeNow=datetime.now()
	out+="Time Finished: "+timeNow.strftime("%Y%m%d %H:%M.%S")
	out=startStr+out
	print(out)
	print("=============================================================================")
	return out

#test email login
sendmail.test()

#first run -- full run
textResults=runScript(longFile)
fOutput=open(textFile,mode="w")
fOutput.write(textResults)
fOutput.close()
sendmail.send(websiteName+"\n\n"+textResults)

#cycle mode
while True:
	#reset timePass upon full round
	if sum(timePass)==len(timePass):
		resetCount=0

		#checks through all timing to make sure it's a save time now
		for i in range(len(fullTimes)):
			timeNow=datetime.now()
			timeCheck=datetime(timeNow.year, timeNow.month, timeNow.day, fullTimes[i].hour, fullTimes[i].minute)
			if timeNow>timeCheck+timedelta(minutes=senMin):
				resetCount+=1

			#if not within 1 hour of all timers
		if resetCount==0:
			timePass=[0]*len(fullTimes)

	#only run scripts on weekdays
	if (datetime.now().isoweekday() in weekdayList):
		#running shortFile every interval
		if (datetime.now().time()>startShort) and (datetime.now().time()<endShort):
			results=runScript(shortFile)
			fOutput=open(textFile,mode="w")
			fOutput.write(results)
			fOutput.close()

			if results.count("Check")>0 or results.count("Buy")>0 or results.count("Sell")>0:
				if lastFullRun==None:
					textResults=runScript(longFile)
					fOutput=open(textFile,mode="w")
					fOutput.write(textResults)
					fOutput.close()
					sendmail.send(websiteName+"\n\n"+textResults)
					lastFullRun=datetime.now()
				elif datetime.now()>lastFullRun+timedelta(hours=1):
					textResults=runScript(longFile)
					fOutput=open(textFile,mode="w")
					fOutput.write(textResults)
					fOutput.close()
					sendmail.send(websiteName+"\n\n"+textResults)
					lastFullRun=datetime.now()

		#running longFile only on fullTimes
		#logs for full time
		for i in range(len(fullTimes)):
			timeNow=datetime.now()
			timeCheck=datetime(timeNow.year, timeNow.month, timeNow.day, fullTimes[i].hour, fullTimes[i].minute)
			if timeNow>timeCheck and timeNow<timeCheck+timedelta(minutes=senMin) and timePass[i]!=1:
				textResults=runScript(longFile)

				#writing usual text file
				fOutput=open(textFile,mode="w")
				fOutput.write(textResults)
				fOutput.close()

				#writing log
				timeNow=datetime.now()
				timeName=str(timeNow.year)
				if timeNow.month<10:
					timeName+="0"+str(timeNow.month)
				else:
					timeName+=str(timeNow.month)

				if timeNow.day<10:
					timeName+="0"+str(timeNow.day)
				else:
					timeName+=str(timeNow.day)

				fOutput=open("resultLog/"+timeName+".txt",mode="a")
				fOutput.write(textResults)
				fOutput.close()

				sendmail.send(websiteName+"\n\n"+textResults)
				lastFullRun=datetime.now()
				timePass[i]=1

	sleep(shortCycle)

