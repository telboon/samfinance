#!/usr/bin/env python3

from samFinance import *
from sys import argv


class interactive():
	def __init__(self):
		self.x=watchList("watchlist")
		self.check=[0]*len(self.x.port)
		self.startDate=None
		self.endDate=None
		self.checkout=""
		while (self.checkout!="q"):
			self.checkout=self.iMainMenu()
			if self.checkout=="s":
				self.startDate=self.askDate("Start Date")
			elif self.checkout=="e":
				self.endDate=self.askDate("End Date")
			elif self.checkout=="a":
				for i in range(len(self.x.port)):
					self.check[i]=(self.check[i]+1)%2
			elif self.checkout=="q":
				pass
			elif self.checkout=="r":
				self.runCharts()
			#check if number. else -- wrong input
			else:
				try:
					#toggle inputs
					if int(self.checkout)>=0 and int(self.checkout)<len(self.x.port):
						self.check[int(self.checkout)]=(self.check[int(self.checkout)]+1)%2
				except:
					print("Wrong input. Please repeat")

	def runCharts(self):
		for i in range(len(self.x.port)):
			if self.check[i]==1:
				tempStock=stock(self.x.port[i].ticker)
				algo.plotPriceChart(tempStock, startDate=self.startDate, endDate=self.endDate, title=self.x.port[i].ticker+": "+self.x.port[i].name)

	def askDate(self, query):
		print(query)
		print()
		year=input("Please key in the year: ")
		month=input("Please key in the month [default=1]: ")
		day=input("Please key in the day [default=1]: ")
		if month=="":
			month=1
		if day=="":
			day=1

		if year=="":
			return None
		else: 
			return datetime(int(year), int(month), int(day))

	def iMainMenu(self):
		print("SamFinance Status Check")
		print("=======================")
		print("")
		print("Start Date : "+str(self.startDate))
		print("End Date   : "+str(self.endDate))
		print("")
		#print every stock ikn portfolio
		for i in range(len(self.x.port)):
			tempText=str(i)+". "+self.x.port[i].ticker+": "+self.x.port[i].name
			if self.check[i]:
				tempText="** "+tempText+" **"
			print(tempText)

		#put choices
		print("")
		print("Choices")
		print("=======")
		print("")
		print("[Number]: Select/Unselect stock from charting")
		print("s: Change start date")
		print("e: Change end date")
		print("a: Toggle all selections")
		print("r: Run charts!")
		print("q: Quit")
		print("")

		return input("Your choice: ")

if len(argv) == 1:
	run=interactive()
elif len(argv)==2 and argv[1]!="-a":
	x=watchList("watchlist")
	x.chartUpdate(startDate=datetime(int(argv[1]),1,1))
elif len(argv)==2 and argv[1]=="-a":
	x=watchList("watchlist")
	x.chartUpdate()
elif len(argv)==3:
	x=watchList("watchlist")
	x.chartUpdate(startDate=datetime(int(argv[1]),int(argv[2]),1))

