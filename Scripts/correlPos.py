#! /usr/bin/env python3

import samFinance
import datetime
import pprint

watch=samFinance.watchList("watchlist")

s=samFinance.backtest.strategy()

print("Starting: "+str(datetime.datetime.now()))

for i in range(len(watch.port)):
   s.reset()
   x=samFinance.stock(watch.port[i].ticker)
   y=samFinance.stock(watch.port[i].benchmarkTicker)
   result=s.histPerfPos(x,y,datetime.datetime(2013,1,10))
   print(watch.port[i].name+": "+str(result))
   
   s.history.stats(datetime.datetime(2013,1,10), datetime.datetime.now())

print("Ending: "+str(datetime.datetime.now()))
