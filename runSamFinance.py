#!/usr/bin/env python3

import samFinance
import datetime
import time
from samFinance.samMail import sendmail

sendmail.test()

portfolio="watchlist"
textFile="statusOutput.txt"
htmlFile="statusHTML.htm"

websiteName="http://test.samuelpua.com/samFinance/results"

#set run timing
#minimally 30 minutes interval
runTime=[]
runTime.append(datetime.time(hour=7, minute=30))
runTime.append(datetime.time(hour=9, minute=30))
runTime.append(datetime.time(hour=12, minute=0))
runTime.append(datetime.time(hour=16, minute=00))
runTime.append(datetime.time(hour=19, minute=00))

runInterval=datetime.timedelta(minutes=30)

runWeekday=[0,1,2,3,4]

runFinish=False

def checkStatus(portfolio):
   emailHTML=""
   emailText=""

   emailHTML+="Time Now: "+datetime.datetime.now().strftime("%Y%m%d %H:%M.%S")+"<br>\n"
   emailHTML+=websiteName+"<br><br>\n"
   emailText+="Time Now: "+datetime.datetime.now().strftime("%Y%m%d %H:%M.%S")+"\n"
   emailText+=websiteName+"\n\n"

   #run checks
   watchlist=samFinance.watchList(portfolio)

   #update & see failures
   updateFailures=watchlist.update()
   emailHTML+=updateFailures.replace("\n","<br>\n")
   emailText+=updateFailures

   watchlist.updateLastP()
   HTML, text=watchlist.status("HistperfBetaPeStdmov")
   emailHTML+=HTML+"<br><br>\n"
   emailText+=text+"\n\n"
   
   emailHTML+="<b>Recommendation</b><br>\n"
   emailText+="Recommendation\n"
   emailText+="==============\n"

   targetHTML, targetText=watchlist.checkTargets()
   emailHTML+=targetHTML+"<br>\n"
   emailText+=targetText+"\n"
   emailHTML+="Time Finished: "+datetime.datetime.now().strftime("%Y%m%d %H:%M.%S")+"<br>\n"
   emailText+="Time Finished: "+datetime.datetime.now().strftime("%Y%m%d %H:%M.%S")+"\n"

   alert=targetText.count("\n")

   return emailHTML, emailText, alert

def writeFile(fileName, text):
   with open(fileName,"w") as f:
      f.write(text)
   return f.closed

HTML, Text, alert =checkStatus("watchlist")
print(Text)
writeFile(textFile, Text)
writeFile(htmlFile, HTML)
HTML="<html><head></head><body>\n"+HTML+"</body></html>\n"

writeFile("resultLog/"+datetime.datetime.now().strftime("%Y%m%d"), Text)
if alert>0:
   sendmail.send(HTML, text=Text)

while True:
   time.sleep(60)
   datetimeNow=datetime.datetime.now()

   #check for reset when it's in middle of runs
   midst=0
   for i in range(len(runTime)):
      runDateTime=datetime.datetime(datetimeNow.year,datetimeNow.month, datetimeNow.day, runTime[i].hour, runTime[i].minute)
      if (datetimeNow>runDateTime) and (datetimeNow-runDateTime<runInterval):
         midst+=1
   
   #if not in middle of any run or in weekend
   if midst==0 or not(datetimeNow.weekday() in runWeekday):
      runFinish=False
   #check for runFinish -- if haven't run, run
   elif runFinish==False:
      HTML, Text, alert =checkStatus("watchlist")
      print(Text)
      writeFile(textFile, Text)
      writeFile(htmlFile, HTML)
      writeFile("resultLog/"+datetime.datetime.now().strftime("%Y%m%d"), Text)

      HTML="<html><head></head><body>\n"+HTML+"</body></html>\n"
      if alert>0:
         sendmail.send(HTML, text=Text)
      runFinish=True
      HTML="<html><head></head><body>\n"+HTML+"</body></html>\n"
      HTML="<html><head></head><body>\n"+HTML+"</body></html>\n"
