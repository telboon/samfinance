Time Now: 20160609 07:30.46
12 tickers loaded


Starting status check...
Ticker     Name                          Last           STDMov    Hist Perf      Beta           PE Ratio  
^STI       Straits Times Index           2862.38        0.235     nan/nan        1.0            None      
Q01.SI     QAF                           1.095          0.0       -0.24/-0.19    0.447          11.061    
AGS.SI     TheHourGlass                  0.81           0.0        0.06/-0.03    0.461          10.95     
RW0U.SI    Mapletree G China             0.985          0.0       -0.34/-0.38    0.748          6.314     
D5IU.SI    Lippo Malls                   0.335          0.0        0.09/ 0.07    0.53           33.5      
ES3.SI     Straits Times ETF             2.91           0.215     -0.24/-0.24    0.924          None      
500.SI     Tai Sin Electric              0.335          0.0        0.7/ 0.48     0.469          8.171     
573.SI     Challenger Techonologies      0.45           0.0       -0.23/-0.26    0.347          8.49      
C6L.SI     SIA                           10.59          0.0       -0.24/-0.19    0.404          15.41     
E5H.SI     Golden Agri                   0.395          0.0       -0.34/-0.19    1.13           65.833    
J91U.SI    Cambridge Industrial Trust    0.55           0.0       -0.05/-0.06    0.818          13.75     
O23.SI     OSIM Intl                     1.39           0.0       -0.47/-0.29    1.135          22.79     


Recommendation
===============

Time Finished: 20160609 07:32.59Time Now: 20160609 19:41.58
12 tickers loaded


Starting status check...
Ticker     Name                          Last           STDMov    Hist Perf      Beta           PE Ratio  
^STI       Straits Times Index           2863.11        0.247     nan/nan        1.0            None      
Q01.SI     QAF                           1.095          0.0       -0.25/-0.19    0.447          11.061    
AGS.SI     TheHourGlass                  0.81           0.0        0.06/-0.03    0.461          10.95     
RW0U.SI    Mapletree G China             0.985          0.0       -0.35/-0.39    0.748          6.314     
D5IU.SI    Lippo Malls                   0.34           0.666      0.66/ 0.47    0.53           34.0      
ES3.SI     Straits Times ETF             2.92           0.212      0.42/ 0.17    0.924          None      
500.SI     Tai Sin Electric              0.335          0.0        0.69/ 0.48    0.469          8.171     
573.SI     Challenger Techonologies      0.45           0.0       -0.23/-0.27    0.347          8.49      
C6L.SI     SIA                           10.59          0.0       -0.24/-0.19    0.404          15.41     
E5H.SI     Golden Agri                   0.395          0.0       -0.35/-0.19    1.13           65.833    
J91U.SI    Cambridge Industrial Trust    0.545          -0.493    -0.48/-0.35    0.818          13.625    
O23.SI     OSIM Intl                     1.39           0.0       -0.47/-0.29    1.135          22.79     


Recommendation
===============

Time Finished: 20160609 19:44.14