Time Now: 20170120 07:36.46
24 tickers loaded


Starting status check...
Ticker     Name                          Last           STDMov    Hist Perf      Beta           PE Ratio  
^STI       Straits Times Index           3008.22        0.113     nan/nan        1.0            None      
^KLSE      FTSE Bursa Malaysia           1666.51        0.078     nan/nan        1.0            None      
^GSPC      S&P 500 Index                 2263.69        -0.144    nan/nan        1.0            None      
Q01.SI     QAF                           1.42           0.0        0.07/-0.01    0.3            9.86      
AGS.SI     TheHourGlass                  0.615          0.0       -0.29/-0.17    0.052          8.99      
RW0U.SI    Mapletree G China             0.96           0.0        0.38/ 0.22    0.625          6.71      
D5IU.SI    Lippo Malls                   0.375          0.0        0.62/ 0.38    0.063          37.5      
ES3.SI     Straits Times ETF             3.07           0.141     -0.17/-0.14    0.875          None      
500.SI     Tai Sin Electric              0.385          0.0        0.04/ 0.05    0.149          7.0       
573.SI     Challenger Techonologies      0.48           0.0        0.04/ 0.09    0.266          10.2      
C6L.SI     SIA                           9.91           0.0        0.53/ 0.37    0.296          14.24     
E5H.SI     Golden Agri                   0.425          0.0       -0.15/-0.06    1.18           15.741    
J91U.SI    Cambridge Industrial Trust    0.56           0.0        1.37/ 0.77    0.609          14.125    
O23.SI     OSIM Intl                     1.39           0.0        0.11/ 0.17    0.752          None      
D05.SI     DBS Bank                      18.28          0.0        0.25/ 0.13    1.214          10.69     
O39.SI     OCBC Bank                     9.3            0.0       -0.08/ 0.01    1.312          10.78     
U11.SI     UOB Bank                      20.8           0.0       -0.3/-0.14     1.037          11.2      
1163.KL    Allianz Malaysia              10.3           0.0        0.76/ 0.56    0.422          11.55     
C          Citigroup                     56.66          -0.169    -1.56/-0.8     2.041          12.321    
BAC        Bank of America               22.53          -0.041    -0.34/-0.16    1.986          16.059    
XOM        ExxonMobil                    84.73          -0.687    -0.84/-0.45    0.47           40.165    
CVX        Chevron Corp                  115.58         -0.064    -0.17/-0.06    0.693          None      
Z74.SI     Singapore Telecomms           3.78           -0.337    -0.43/-0.24    0.396          15.814    
3395.KL    Berjaya Corporation Berhad    0.36           0.0       -0.84/-0.59    0.445          None      


Recommendation
===============
QAF Sell. Current:1.42 (1.15) 
TheHourGlass Buy. Current:0.615 (0.7) 
SIA Buy. Current:9.91 (10.0) 
DBS Bank Sell. Current:18.28 (17.0) 
UOB Bank Sell. Current:20.8 (20.0) 
Citigroup Sell. Current:56.66 (48.0) 
Citigroup Check. Hist Perf:-1.56 (-5.0%) 
Bank of America Sell. Current:22.53 (17.0) 

Time Finished: 20170120 07:44.05Time Now: 20170120 19:44.44
24 tickers loaded


Starting status check...
Ticker     Name                          Last           STDMov    Hist Perf      Beta           PE Ratio  
^STI       Straits Times Index           3011.08        0.154     nan/nan        1.0            None      
^KLSE      FTSE Bursa Malaysia           1664.89        -0.085    nan/nan        1.0            None      
^GSPC      S&P 500 Index                 2263.69        0.0       nan/nan        1.0            None      
Q01.SI     QAF                           1.415          -0.106    -0.09/-0.11    0.3            9.825     
AGS.SI     TheHourGlass                  0.62           0.218      0.01/ 0.01    0.052          9.063     
RW0U.SI    Mapletree G China             0.97           0.311      0.81/ 0.43    0.625          6.78      
D5IU.SI    Lippo Malls                   0.375          0.0        0.62/ 0.38    0.063          37.5      
ES3.SI     Straits Times ETF             3.08           0.139      0.09/ 0.09    0.875          None      
500.SI     Tai Sin Electric              0.38           -0.811    -0.55/-0.38    0.149          6.909     
573.SI     Challenger Techonologies      0.48           0.0        0.03/ 0.08    0.266          10.2      
C6L.SI     SIA                           9.92           0.07       0.57/ 0.39    0.296          14.254    
E5H.SI     Golden Agri                   0.425          0.0       -0.19/-0.08    1.18           15.741    
J91U.SI    Cambridge Industrial Trust    0.56           0.0        1.33/ 0.75    0.609          14.125    
O23.SI     OSIM Intl                     1.39           0.0        0.09/ 0.14    0.752          None      
D05.SI     DBS Bank                      18.27          -0.009     0.16/ 0.09    1.214          10.684    
O39.SI     OCBC Bank                     9.3            0.0       -0.17/-0.06    1.312          10.78     
U11.SI     UOB Bank                      20.72          -0.09     -0.57/-0.28    1.037          11.157    
1163.KL    Allianz Malaysia              10.2           -0.511     0.13/ 0.2     0.422          11.438    
C          Citigroup                     56.66          0.0       -1.55/-0.8     2.047          12.32     
BAC        Bank of America               22.53          -0.0      -0.34/-0.16    1.993          16.55     
XOM        ExxonMobil                    84.73          -0.0      -0.84/-0.44    0.469          39.67     
CVX        Chevron Corp                  115.58         -0.0      -0.17/-0.06    0.689          None      
Z74.SI     Singapore Telecomms           3.81           0.0       -0.13/-0.04    0.396          15.94     
3395.KL    Berjaya Corporation Berhad    0.355          -0.409    -1.27/-0.9     0.434          None      


Recommendation
===============
QAF Sell. Current:1.415 (1.15) 
TheHourGlass Buy. Current:0.62 (0.7) 
SIA Buy. Current:9.92 (10.0) 
DBS Bank Sell. Current:18.27 (17.0) 
UOB Bank Sell. Current:20.72 (20.0) 
Citigroup Sell. Current:56.66 (48.0) 
Citigroup Check. Hist Perf:-1.55 (-5.0%) 
Bank of America Sell. Current:22.53 (17.0) 

Time Finished: 20170120 19:50.21