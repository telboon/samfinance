Time Now: 20160307 07:33.02
12 tickers loaded


Starting status check...
Ticker     Name                          Last           STDMov    Hist Perf      Beta           PE Ratio  
^STI       Straits Times Index           2837.0         0.0       nan/nan        1.0            None      
Q01.SI     QAF                           1.03           0.0       -0.51/-0.49    0.521          10.96     
AGS.SI     TheHourGlass                  0.72           0.0       -1.83/-1.35    0.646          8.78      
RW0U.SI    Mapletree G China             0.94           0.0       -1.0/-0.78     0.839          6.86      
D5IU.SI    Lippo Malls                   0.32           0.0       -1.34/-0.75    0.749          None      
ES3.SI     Straits Times ETF             2.84           0.0       -0.71/-0.76    0.925          None      
500.SI     Tai Sin Electric              0.315          0.0       -2.11/-1.84    0.603          8.75      
573.SI     Challenger Techonologies      0.47           0.0        2.77/ 1.99    0.23           8.87      
C6L.SI     SIA                           11.36          0.0       -2.42/-1.44    0.567          21.47     
E5H.SI     Golden Agri                   0.4            0.0        0.23/-0.0     1.12           None      
J91U.SI    Cambridge Industrial Trust    0.545          0.0       -0.93/-0.43    0.907          None      
O23.SI     OSIM Intl                     1.225          0.0        0.48/ 0.49    1.151          18.015    


Recommendation
===============
TheHourGlass Check. Hist Perf:-1.83 (0.0%) 
Tai Sin Electric Check. Hist Perf:-2.11 (-1.6%) 
Challenger Techonologies Check. Hist Perf:2.77 (10.6%) 
SIA Check. Hist Perf:-2.42 (-2.3%) 

Time Finished: 20160307 07:34.42Time Now: 20160307 19:30.40
12 tickers loaded


Starting status check...
Ticker     Name                          Last           STDMov    Hist Perf      Beta           PE Ratio  
^STI       Straits Times Index           2837.0         0.0       nan/nan        1.0            None      
Q01.SI     QAF                           1.03           0.0       -0.51/-0.49    0.521          10.96     
AGS.SI     TheHourGlass                  0.72           0.0       -1.83/-1.35    0.646          8.78      
RW0U.SI    Mapletree G China             0.94           0.0       -1.0/-0.78     0.839          6.86      
D5IU.SI    Lippo Malls                   0.32           0.0       -1.34/-0.75    0.749          None      
ES3.SI     Straits Times ETF             2.85           0.075      0.06/-0.28    0.925          None      
500.SI     Tai Sin Electric              0.315          0.0       -2.11/-1.84    0.603          8.75      
573.SI     Challenger Techonologies      0.47           0.0        2.77/ 1.99    0.23           8.87      
C6L.SI     SIA                           11.36          0.0       -2.42/-1.44    0.567          21.47     
E5H.SI     Golden Agri                   0.4            0.0        0.23/-0.0     1.12           None      
J91U.SI    Cambridge Industrial Trust    0.545          0.0       -0.93/-0.43    0.907          None      
O23.SI     OSIM Intl                     1.225          0.0        0.48/ 0.49    1.151          18.015    


Recommendation
===============
TheHourGlass Check. Hist Perf:-1.83 (0.0%) 
Tai Sin Electric Check. Hist Perf:-2.11 (-1.6%) 
Challenger Techonologies Check. Hist Perf:2.77 (10.6%) 
SIA Check. Hist Perf:-2.42 (-2.3%) 

Time Finished: 20160307 19:32.38