Time Now: 20170526 07:34.03
24 tickers loaded


Starting status check...
Ticker     Name                          Last           STDMov    Hist Perf      Beta           PE Ratio  
^STI       Straits Times Index           3234.37        -0.487    nan/nan        1.0            None      
^KLSE      FTSE Bursa Malaysia           1773.96        -0.084    nan/nan        1.0            None      
^GSPC      S&P 500 Index                 2415.07        1.341     nan/nan        1.0            None      
Q01.SI     QAF                           1.345          -0.278    -0.33/-0.21    0.277          6.29      
AGS.SI     TheHourGlass                  0.69           0.366      0.32/ 0.26    0.311          10.148    
RW0U.SI    Mapletree G China             1.075          1.061      2.08/ 0.87    0.554          8.021     
D5IU.SI    Lippo Malls                   0.415          0.0        0.1/ 0.03     0.321          46.111    
ES3.SI     Straits Times ETF             3.29           0.0        0.62/ 0.57    0.891          None      
500.SI     Tai Sin Electric              0.425          -1.22     -1.76/-0.93    0.394          7.373     
573.SI     Challenger Techonologies      0.45           -3.478    -1.57/-1.08    -0.413         12.504    
C6L.SI     SIA                           9.89           -3.283    -4.99/-2.93    0.425          16.418    
E5H.SI     Golden Agri                   0.385          0.664      1.04/ 0.46    1.262          12.42     
J91U.SI    Cambridge Industrial Trust    0.565          -0.542    -0.38/-0.18    0.377          113.0     
O23.SI     OSIM Intl                     1.39           0.0        0.15/ 0.09    0.585          None      
D05.SI     DBS Bank                      21.04          0.446      0.99/ 0.48    1.133          12.602    
O39.SI     OCBC Bank                     10.47          -0.507    -0.46/-0.28    1.159          12.738    
U11.SI     UOB Bank                      23.43          -0.34     -0.14/-0.11    1.16           12.596    
1163.KL    Allianz Malaysia              12.28          1.561      1.51/ 0.8     0.62           13.766    
C          Citigroup                     61.95          0.848     -0.21/-0.07    1.872          12.487    
BAC        Bank of America               23.25          -0.848    -1.78/-0.79    2.033          14.308    
XOM        ExxonMobil                    81.75          -1.141    -0.96/-0.36    0.689          34.106    
CVX        Chevron Corp                  105.11         -0.345    -0.77/-0.37    0.551          68.208    
Z74.SI     Singapore Telecomms           3.73           -0.101    -0.38/-0.29    0.574          15.538    
3395.KL    Berjaya Corporation Berhad    0.355          -0.658    -0.9/-0.52     0.374          None      


Recommendation
===============
QAF Sell. Current:1.345 (1.15) 
TheHourGlass Buy. Current:0.69 (0.7) 
Mapletree G China Sell. Current:1.075 (1.0) 
Mapletree G China Check. Day Delta:3.4% (3.0%) 
Mapletree G China Check. Hist Perf:2.08 (3.4%) 
Lippo Malls Sell. Current:0.415 (0.38) 
Tai Sin Electric Check. Hist Perf:-1.76 (-4.5%) 
Challenger Techonologies Check. Day STD:-3.48 (-2.0%) 
Challenger Techonologies Check. Hist Perf:-1.57 (-4.3%) 
SIA Buy. Current:9.89 (10.0) 
SIA Check. Day Delta:-7.1% (5.0%) 
SIA Check. Day STD:-3.28 (-76.0%) 
SIA Check. Hist Perf:-4.99 (-7.1%) 
DBS Bank Sell. Current:21.04 (17.0) 
OCBC Bank Sell. Current:10.47 (10.0) 
UOB Bank Sell. Current:23.43 (20.0) 
Allianz Malaysia Sell. Current:12.28 (12.0) 
Allianz Malaysia Check. Day STD:1.56 (30.0%) 
Allianz Malaysia Check. Hist Perf:1.51 (2.5%) 
Citigroup Sell. Current:61.95 (48.0) 
Bank of America Sell. Current:23.25 (17.0) 
Bank of America Check. Hist Perf:-1.78 (-3.1%) 

Time Finished: 20170526 07:39.35Time Now: 20170526 19:41.34
24 tickers loaded


Starting status check...
Ticker     Name                          Last           STDMov    Hist Perf      Beta           PE Ratio  
^STI       Straits Times Index           3219.42        -0.834    nan/nan        1.0            None      
^KLSE      FTSE Bursa Malaysia           1772.3         -0.157    nan/nan        1.0            None      
^GSPC      S&P 500 Index                 2415.07        1.341     nan/nan        1.0            None      
Q01.SI     QAF                           1.355          -0.093    -0.02/-0.08    0.277          6.337     
AGS.SI     TheHourGlass                  0.69           0.366      0.37/ 0.32    0.311          10.148    
RW0U.SI    Mapletree G China             1.075          1.061      2.22/ 0.94    0.554          8.021     
D5IU.SI    Lippo Malls                   0.415          0.0        0.17/ 0.05    0.321          46.111    
ES3.SI     Straits Times ETF             3.27           -0.386     0.4/ 0.41     0.891          None      
500.SI     Tai Sin Electric              0.44           -0.305    -0.29/-0.06    0.394          7.633     
573.SI     Challenger Techonologies      0.45           -3.478    -1.63/-1.13    -0.413         12.504    
C6L.SI     SIA                           9.88           -3.326    -4.92/-2.81    0.425          16.402    
E5H.SI     Golden Agri                   0.385          0.664      1.21/ 0.49    1.262          12.42     
J91U.SI    Cambridge Industrial Trust    0.565          -0.542    -0.27/-0.1     0.377          113.0     
O23.SI     OSIM Intl                     1.39           0.0        0.26/ 0.16    0.585          None      
D05.SI     DBS Bank                      20.8           0.05       0.68/ 0.38    1.133          12.458    
O39.SI     OCBC Bank                     10.43          -0.642    -0.36/-0.23    1.159          12.689    
U11.SI     UOB Bank                      23.37          -0.429     0.03/-0.05    1.16           12.564    
1163.KL    Allianz Malaysia              12.24          1.353      1.35/ 0.74    0.62           13.721    
C          Citigroup                     61.95          0.848     -0.21/-0.07    1.872          12.487    
BAC        Bank of America               23.25          -0.848    -1.78/-0.79    2.033          14.308    
XOM        ExxonMobil                    81.75          -1.141    -0.96/-0.36    0.689          34.106    
CVX        Chevron Corp                  105.11         -0.345    -0.77/-0.37    0.551          68.208    
Z74.SI     Singapore Telecomms           3.73           -0.101    -0.38/-0.29    0.574          15.538    
3395.KL    Berjaya Corporation Berhad    0.35           -0.987    -1.34/-0.78    0.374          None      


Recommendation
===============
QAF Sell. Current:1.355 (1.15) 
TheHourGlass Buy. Current:0.69 (0.7) 
Mapletree G China Sell. Current:1.075 (1.0) 
Mapletree G China Check. Day Delta:3.4% (3.0%) 
Mapletree G China Check. Hist Perf:2.22 (3.4%) 
Lippo Malls Sell. Current:0.415 (0.38) 
Challenger Techonologies Check. Day STD:-3.48 (-2.0%) 
Challenger Techonologies Check. Hist Perf:-1.63 (-4.3%) 
SIA Buy. Current:9.88 (10.0) 
SIA Check. Day Delta:-7.2% (5.0%) 
SIA Check. Day STD:-3.33 (-77.0%) 
SIA Check. Hist Perf:-4.92 (-7.2%) 
DBS Bank Sell. Current:20.8 (17.0) 
OCBC Bank Sell. Current:10.43 (10.0) 
UOB Bank Sell. Current:23.37 (20.0) 
Allianz Malaysia Sell. Current:12.24 (12.0) 
Citigroup Sell. Current:61.95 (48.0) 
Bank of America Sell. Current:23.25 (17.0) 
Bank of America Check. Hist Perf:-1.78 (-3.1%) 
Berjaya Corporation Berhad Check. Day Delta:-4.1% (3.0%) 

Time Finished: 20170526 19:50.09