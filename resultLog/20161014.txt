Time Now: 20161014 07:42.33
24 tickers loaded


Starting status check...
Ticker     Name                          Last           STDMov    Hist Perf      Beta           PE Ratio  
^STI       Straits Times Index           2805.48        -0.942    nan/nan        1.0            None      
^KLSE      FTSE Bursa Malaysia           1665.02        -0.162    nan/nan        1.0            None      
^GSPC      S&P 500 Index                 2132.55        -0.408    nan/nan        1.0            None      
Q01.SI     QAF                           1.255          0.0        0.98/ 0.62    0.491          9.729     
AGS.SI     TheHourGlass                  0.69           0.0        0.71/ 0.42    0.442          9.72      
RW0U.SI    Mapletree G China             1.075          0.0       -0.32/-0.17    0.737          7.396     
D5IU.SI    Lippo Malls                   0.38           0.0        0.43/ 0.3     0.551          None      
ES3.SI     Straits Times ETF             2.85           -0.595    -1.67/-0.96    0.918          None      
500.SI     Tai Sin Electric              0.38           0.0        1.32/ 1.12    0.473          7.075     
573.SI     Challenger Techonologies      0.49           0.0        0.73/ 0.62    0.283          8.89      
C6L.SI     SIA                           10.32          0.0       -0.26/-0.11    0.39           12.5      
E5H.SI     Golden Agri                   0.36           0.0        0.55/ 0.29    1.135          40.0      
J91U.SI    Cambridge Industrial Trust    0.555          0.0        0.77/ 0.54    0.822          None      
O23.SI     OSIM Intl                     1.39           0.0        0.38/ 0.26    1.075          None      
D05.SI     DBS Bank                      15.03          0.0        0.03/ 0.06    1.244          8.8       
O39.SI     OCBC Bank                     8.45           0.0       -0.18/-0.05    1.185          9.76      
U11.SI     UOB Bank                      18.3           0.0       -0.28/-0.21    1.082          9.44      
1163.KL    Allianz Malaysia              10.06          0.0       -0.41/-0.21    0.342          11.28     
C          Citigroup                     48.47          -0.15      0.06/ 0.06    1.848          10.321    
BAC        Bank of America               15.83          -0.338    -0.08/ 0.01    1.647          13.332    
XOM        ExxonMobil                    86.56          -0.22      0.17/ 0.03    0.608          34.622    
CVX        Chevron Corp                  100.79         -0.735    -0.2/-0.12     0.969          None      
Z74.SI     Singapore Telecomms           3.84           -1.216    -1.47/-0.89    0.476          15.8      
3395.KL    Berjaya Corporation Berhad    0.325          0.0       -0.49/-0.27    0.303          None      


Recommendation
===============
Straits Times Index Check. Day Delta:-2.1% (1.5%) 
QAF Sell. Current:1.255 (1.15) 
TheHourGlass Buy. Current:0.69 (0.7) 
Mapletree G China Sell. Current:1.075 (1.0) 
Lippo Malls Sell. Current:0.38 (0.38) 
Straits Times ETF Check. Hist Perf:-1.67 (-2.7%) 
Citigroup Sell. Current:48.47 (48.0) 
Singapore Telecomms Check. Day Delta:-4.2% (3.0%) 

Time Finished: 20161014 07:47.18Time Now: 20161014 19:41.01
24 tickers loaded


Starting status check...
Ticker     Name                          Last           STDMov    Hist Perf      Beta           PE Ratio  
^STI       Straits Times Index           2815.24        -0.787    nan/nan        1.0            None      
^KLSE      FTSE Bursa Malaysia           1658.97        -0.493    nan/nan        1.0            None      
^GSPC      S&P 500 Index                 2132.55        -0.0      nan/nan        1.0            None      
Q01.SI     QAF                           1.255          0.0        0.91/ 0.57    0.491          9.729     
AGS.SI     TheHourGlass                  0.69           0.0        0.65/ 0.37    0.442          9.72      
RW0U.SI    Mapletree G China             1.07           -0.246    -0.63/-0.45    0.737          7.362     
D5IU.SI    Lippo Malls                   0.38           0.0        0.36/ 0.25    0.551          None      
ES3.SI     Straits Times ETF             2.87           0.605     -0.93/-0.51    0.918          None      
500.SI     Tai Sin Electric              0.38           0.0        1.26/ 1.06    0.473          7.075     
573.SI     Challenger Techonologies      0.49           0.0        0.71/ 0.58    0.283          8.89      
C6L.SI     SIA                           10.23          -0.425    -0.68/-0.34    0.39           12.391    
E5H.SI     Golden Agri                   0.36           0.0        0.46/ 0.24    1.135          40.0      
J91U.SI    Cambridge Industrial Trust    0.555          0.0        0.64/ 0.45    0.822          None      
O23.SI     OSIM Intl                     1.39           0.0        0.32/ 0.22    1.075          None      
D05.SI     DBS Bank                      15.03          0.0       -0.27/-0.09    1.244          8.8       
O39.SI     OCBC Bank                     8.41           -0.269    -0.93/-0.44    1.185          9.714     
U11.SI     UOB Bank                      18.32          0.043     -0.45/-0.3     1.082          9.45      
1163.KL    Allianz Malaysia              10.06          0.0       -0.33/-0.09    0.342          11.28     
C          Citigroup                     48.47          -0.0       0.06/ 0.06    1.846          10.26     
BAC        Bank of America               15.83          0.0       -0.08/ 0.01    1.647          13.15     
XOM        ExxonMobil                    86.56          0.0        0.17/ 0.03    0.608          34.38     
CVX        Chevron Corp                  100.79         -0.0      -0.2/-0.12     0.971          None      
Z74.SI     Singapore Telecomms           3.88           -0.93     -1.08/-0.63    0.476          15.965    
3395.KL    Berjaya Corporation Berhad    0.33           0.618      0.04/ 0.08    0.304          None      


Recommendation
===============
Straits Times Index Check. Day Delta:-1.7% (1.5%) 
QAF Sell. Current:1.255 (1.15) 
TheHourGlass Buy. Current:0.69 (0.7) 
Mapletree G China Sell. Current:1.07 (1.0) 
Lippo Malls Sell. Current:0.38 (0.38) 
Citigroup Sell. Current:48.47 (48.0) 
Singapore Telecomms Check. Day Delta:-3.2% (3.0%) 

Time Finished: 20161014 19:45.50