Time Now: 20170125 07:34.06
24 tickers loaded


Starting status check...
Ticker     Name                          Last           STDMov    Hist Perf      Beta           PE Ratio  
^STI       Straits Times Index           3041.95        0.229     nan/nan        1.0            None      
^KLSE      FTSE Bursa Malaysia           1680.69        0.505     nan/nan        1.0            None      
^GSPC      S&P 500 Index                 2280.07        0.263     nan/nan        1.0            None      
Q01.SI     QAF                           1.42           0.0       -0.17/ 0.03    0.301          9.86      
AGS.SI     TheHourGlass                  0.63           0.0        0.57/ 0.34    0.049          9.13      
RW0U.SI    Mapletree G China             0.965          0.0       -0.82/-0.49    0.616          6.748     
D5IU.SI    Lippo Malls                   0.38           0.0        0.59/ 0.39    0.037          None      
ES3.SI     Straits Times ETF             3.09           0.132     -0.28/-0.29    0.886          None      
500.SI     Tai Sin Electric              0.39           0.0        0.48/ 0.31    0.163          7.09      
573.SI     Challenger Techonologies      0.48           0.0       -1.23/-0.98    0.27           9.8       
C6L.SI     SIA                           10.02          0.0        0.36/ 0.09    0.293          14.4      
E5H.SI     Golden Agri                   0.425          0.0       -0.13/-0.15    1.16           15.741    
J91U.SI    Cambridge Industrial Trust    0.56           0.0        0.04/-0.06    0.599          None      
O23.SI     OSIM Intl                     1.39           0.0       -0.29/-0.42    0.745          None      
D05.SI     DBS Bank                      18.66          0.0        0.4/ 0.14     1.219          10.91     
O39.SI     OCBC Bank                     9.38           0.0       -0.14/-0.22    1.314          10.87     
U11.SI     UOB Bank                      20.96          0.0       -0.33/-0.17    1.038          11.29     
1163.KL    Allianz Malaysia              10.3           0.0       -0.3/-0.41     0.468          11.46     
C          Citigroup                     56.74          0.262     -0.71/-0.31    2.066          12.025    
BAC        Bank of America               22.95          0.171      0.21/ 0.1     1.998          15.286    
XOM        ExxonMobil                    85.09          0.052     -0.79/-0.41    0.464          39.836    
CVX        Chevron Corp                  116.37         0.189      0.06/0.0      0.676          None      
Z74.SI     Singapore Telecomms           3.84           0.337      0.25/ 0.13    0.396          16.066    
3395.KL    Berjaya Corporation Berhad    0.355          0.0       -1.04/-0.84    0.471          None      


Recommendation
===============
QAF Sell. Current:1.42 (1.15) 
TheHourGlass Buy. Current:0.63 (0.7) 
Lippo Malls Sell. Current:0.38 (0.38) 
DBS Bank Sell. Current:18.66 (17.0) 
UOB Bank Sell. Current:20.96 (20.0) 
Citigroup Sell. Current:56.74 (48.0) 
Bank of America Sell. Current:22.95 (17.0) 

Time Finished: 20170125 07:39.12Time Now: 20170125 19:31.44
24 tickers loaded


Starting status check...
Ticker     Name                          Last           STDMov    Hist Perf      Beta           PE Ratio  
^STI       Straits Times Index           3039.94        -0.027    nan/nan        1.0            None      
^KLSE      FTSE Bursa Malaysia           1683.93        0.173     nan/nan        1.0            None      
^GSPC      S&P 500 Index                 2280.07        -0.0      nan/nan        1.0            None      
Q01.SI     QAF                           1.415          -0.114    -0.31/-0.08    0.299          9.825     
AGS.SI     TheHourGlass                  0.63           0.0        0.57/ 0.34    0.05           9.13      
RW0U.SI    Mapletree G China             0.96           -0.213    -1.03/-0.6     0.616          6.713     
D5IU.SI    Lippo Malls                   0.385          0.601      1.21/ 0.77    0.036          None      
ES3.SI     Straits Times ETF             3.1            0.13       0.15/ 0.1     0.887          None      
500.SI     Tai Sin Electric              0.39           0.0        0.49/ 0.32    0.164          7.09      
573.SI     Challenger Techonologies      0.48           0.0       -1.23/-0.97    0.271          9.8       
C6L.SI     SIA                           10.04          0.161      0.49/ 0.18    0.293          14.429    
E5H.SI     Golden Agri                   0.42           -0.226    -0.47/-0.33    1.158          15.556    
J91U.SI    Cambridge Industrial Trust    0.565          0.522      0.64/ 0.27    0.598          None      
O23.SI     OSIM Intl                     1.39           0.0       -0.28/-0.4     0.745          None      
D05.SI     DBS Bank                      18.85          0.187      0.95/ 0.39    1.221          11.021    
O39.SI     OCBC Bank                     9.34           -0.143    -0.38/-0.35    1.314          10.824    
U11.SI     UOB Bank                      20.99          0.035     -0.21/-0.11    1.038          11.306    
1163.KL    Allianz Malaysia              10.4           0.503      0.29/-0.1     0.482          11.571    
C          Citigroup                     56.74          -0.0      -0.71/-0.31    2.063          12.02     
BAC        Bank of America               22.95          -0.0       0.21/ 0.1     1.996          15.3      
XOM        ExxonMobil                    85.09          0.0       -0.78/-0.41    0.457          39.84     
CVX        Chevron Corp                  116.37         -0.0       0.06/0.0      0.665          None      
Z74.SI     Singapore Telecomms           3.86           0.561      0.45/ 0.27    0.396          16.149    
3395.KL    Berjaya Corporation Berhad    0.355          0.0       -1.08/-0.89    0.479          None      


Recommendation
===============
QAF Sell. Current:1.415 (1.15) 
TheHourGlass Buy. Current:0.63 (0.7) 
Lippo Malls Sell. Current:0.385 (0.38) 
DBS Bank Sell. Current:18.85 (17.0) 
UOB Bank Sell. Current:20.99 (20.0) 
Citigroup Sell. Current:56.74 (48.0) 
Bank of America Sell. Current:22.95 (17.0) 

Time Finished: 20170125 19:36.52