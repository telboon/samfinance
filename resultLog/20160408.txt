Time Now: 20160408 07:34.39
12 tickers loaded


Starting status check...
Ticker     Name                          Last           STDMov    Hist Perf      Beta           PE Ratio  
^STI       Straits Times Index           2813.59        0.02      nan/nan        1.0            None      
Q01.SI     QAF                           1.03           0.0       -0.86/-0.6     0.485          10.96     
AGS.SI     TheHourGlass                  0.75           0.0       -0.25/-0.12    0.529          9.15      
RW0U.SI    Mapletree G China             0.965          0.0        1.31/ 1.07    0.786          7.044     
D5IU.SI    Lippo Malls                   0.33           0.0        0.65/ 0.45    0.617          None      
ES3.SI     Straits Times ETF             2.82           0.0       -0.4/-0.25     0.929          None      
500.SI     Tai Sin Electric              0.32           0.0        0.62/ 0.5     0.514          8.89      
573.SI     Challenger Techonologies      0.465          0.0       -0.29/-0.2     0.29           8.774     
C6L.SI     SIA                           11.38          0.0       -0.18/-0.09    0.483          21.51     
E5H.SI     Golden Agri                   0.42           0.0        0.05/ 0.02    1.109          None      
J91U.SI    Cambridge Industrial Trust    0.55           0.0        0.07/ 0.05    0.882          None      
O23.SI     OSIM Intl                     1.37           0.0        0.04/ 0.02    1.292          20.15     


Recommendation
===============

Time Finished: 20160408 07:36.32Time Now: 20160408 19:38.08
12 tickers loaded


Starting status check...
Ticker     Name                          Last           STDMov    Hist Perf      Beta           PE Ratio  
^STI       Straits Times Index           2808.23        -0.046    nan/nan        1.0            None      
Q01.SI     QAF                           1.03           0.0       -0.83/-0.58    0.484          10.96     
AGS.SI     TheHourGlass                  0.75           0.0       -0.2/-0.1      0.527          9.15      
RW0U.SI    Mapletree G China             0.965          0.0        1.38/ 1.14    0.788          7.044     
D5IU.SI    Lippo Malls                   0.33           0.0        0.69/ 0.48    0.617          None      
ES3.SI     Straits Times ETF             2.82           0.095     -0.03/-0.01    0.929          None      
500.SI     Tai Sin Electric              0.32           0.0        0.66/ 0.54    0.515          8.89      
573.SI     Challenger Techonologies      0.465          0.0       -0.28/-0.18    0.287          8.774     
C6L.SI     SIA                           11.32          -0.266    -0.36/-0.17    0.482          21.397    
E5H.SI     Golden Agri                   0.41           -0.323    -0.47/-0.21    1.111          None      
J91U.SI    Cambridge Industrial Trust    0.545          -0.211    -0.27/-0.18    0.884          None      
O23.SI     OSIM Intl                     1.37           0.0        0.07/ 0.04    1.297          20.15     


Recommendation
===============

Time Finished: 20160408 19:39.54