Time Now: 20170313 07:37.15
24 tickers loaded


Starting status check...
Ticker     Name                          Last           STDMov    Hist Perf      Beta           PE Ratio  
^STI       Straits Times Index           3133.35        -0.0      nan/nan        1.0            None      
^KLSE      FTSE Bursa Malaysia           1717.58        0.0       nan/nan        1.0            None      
^GSPC      S&P 500 Index                 2372.6         -0.0      nan/nan        1.0            None      
Q01.SI     QAF                           1.365          0.0       -0.3/-0.19     0.322          6.379     
AGS.SI     TheHourGlass                  0.69           0.0        0.52/ 0.29    0.097          10.15     
RW0U.SI    Mapletree G China             0.97           0.0       -0.84/-0.41    0.511          6.83      
D5IU.SI    Lippo Malls                   0.39           0.0       -0.64/-0.37    0.092          39.0      
ES3.SI     Straits Times ETF             3.13           0.0       -0.01/-0.01    0.874          None      
500.SI     Tai Sin Electric              0.45           0.0        3.16/ 2.2     0.266          7.89      
573.SI     Challenger Techonologies      0.47           0.0       -0.63/-0.43    0.014          13.06     
C6L.SI     SIA                           9.93           0.0        0.11/ 0.01    0.336          16.25     
E5H.SI     Golden Agri                   0.375          0.0       -0.14/-0.07    1.178          12.097    
J91U.SI    Cambridge Industrial Trust    0.57           0.0       -0.67/-0.37    0.431          114.0     
O23.SI     OSIM Intl                     1.39           0.0       -0.09/-0.06    0.585          None      
D05.SI     DBS Bank                      18.93          0.0       -0.54/-0.25    1.05           11.24     
O39.SI     OCBC Bank                     9.56           0.0        0.25/ 0.15    1.288          11.63     
U11.SI     UOB Bank                      21.39          0.0       -0.8/-0.36     1.183          11.56     
1163.KL    Allianz Malaysia              11.6           0.0        1.45/ 0.84    0.545          13.0      
C          Citigroup                     61.49          -0.0       0.89/ 0.4     1.971          13.03     
BAC        Bank of America               25.31          0.0        0.15/ 0.07    1.949          16.91     
XOM        ExxonMobil                    81.61          -0.0      -0.77/-0.34    0.653          43.48     
CVX        Chevron Corp                  110.61         -0.0      -1.19/-0.56    0.575          None      
Z74.SI     Singapore Telecomms           3.94           0.089      0.57/ 0.35    0.633          16.422    
3395.KL    Berjaya Corporation Berhad    0.385          0.0       -0.75/-0.41    0.589          None      


Recommendation
===============
QAF Sell. Current:1.365 (1.15) 
TheHourGlass Buy. Current:0.69 (0.7) 
Lippo Malls Sell. Current:0.39 (0.38) 
Tai Sin Electric Check. Hist Perf:3.16 (7.1%) 
SIA Buy. Current:9.93 (10.0) 
DBS Bank Sell. Current:18.93 (17.0) 
UOB Bank Sell. Current:21.39 (20.0) 
Citigroup Sell. Current:61.49 (48.0) 
Bank of America Sell. Current:25.31 (17.0) 

Time Finished: 20170313 07:42.39Time Now: 20170313 19:39.12
24 tickers loaded


Starting status check...
Ticker     Name                          Last           STDMov    Hist Perf      Beta           PE Ratio  
^STI       Straits Times Index           3147.15        0.168     nan/nan        1.0            None      
^KLSE      FTSE Bursa Malaysia           1721.92        0.142     nan/nan        1.0            None      
^GSPC      S&P 500 Index                 2372.6         -0.0      nan/nan        1.0            None      
Q01.SI     QAF                           1.355          -0.136    -0.61/-0.38    0.322          6.332     
AGS.SI     TheHourGlass                  0.685          -0.234     0.24/ 0.11    0.097          10.076    
RW0U.SI    Mapletree G China             0.975          0.291     -0.7/-0.34     0.511          6.865     
D5IU.SI    Lippo Malls                   0.39           0.0       -0.66/-0.38    0.092          39.0      
ES3.SI     Straits Times ETF             3.16           0.458      0.61/ 0.53    0.874          None      
500.SI     Tai Sin Electric              0.455          0.276      3.64/ 2.51    0.266          7.978     
573.SI     Challenger Techonologies      0.47           0.0       -0.63/-0.45    0.014          13.06     
C6L.SI     SIA                           9.99           0.586      0.41/ 0.12    0.336          16.348    
E5H.SI     Golden Agri                   0.385          0.53       0.52/ 0.28    1.178          12.42     
J91U.SI    Cambridge Industrial Trust    0.57           0.0       -0.79/-0.46    0.431          114.0     
O23.SI     OSIM Intl                     1.39           0.0       -0.19/-0.12    0.585          None      
D05.SI     DBS Bank                      19.11          0.301     -0.3/-0.16     1.05           11.347    
O39.SI     OCBC Bank                     9.61           0.223      0.22/ 0.13    1.288          11.691    
U11.SI     UOB Bank                      21.49          0.25      -0.84/-0.38    1.183          11.614    
1163.KL    Allianz Malaysia              11.6           0.0        1.37/ 0.75    0.545          13.0      
C          Citigroup                     61.49          -0.0       0.89/ 0.4     1.971          13.03     
BAC        Bank of America               25.31          0.0        0.15/ 0.07    1.949          16.91     
XOM        ExxonMobil                    81.61          -0.0      -0.77/-0.34    0.653          43.48     
CVX        Chevron Corp                  110.61         -0.0      -1.19/-0.56    0.575          None      
Z74.SI     Singapore Telecomms           3.97           0.356      0.89/ 0.55    0.633          16.547    
3395.KL    Berjaya Corporation Berhad    0.395          0.425      0.06/ 0.07    0.589          None      


Recommendation
===============
QAF Sell. Current:1.355 (1.15) 
TheHourGlass Buy. Current:0.685 (0.7) 
Lippo Malls Sell. Current:0.39 (0.38) 
Tai Sin Electric Check. Hist Perf:3.64 (8.3%) 
SIA Buy. Current:9.99 (10.0) 
DBS Bank Sell. Current:19.11 (17.0) 
UOB Bank Sell. Current:21.49 (20.0) 
Citigroup Sell. Current:61.49 (48.0) 
Bank of America Sell. Current:25.31 (17.0) 

Time Finished: 20170313 19:43.42