Time Now: 20160909 07:36.49
24 tickers loaded


Starting status check...
Ticker     Name                          Last           STDMov    Hist Perf      Beta           PE Ratio  
^STI       Straits Times Index           2894.48        0.475     nan/nan        1.0            None      
^KLSE      FTSE Bursa Malaysia           1691.38        0.089     nan/nan        1.0            None      
^GSPC      S&P 500 Index                 2181.3         -0.103    nan/nan        1.0            None      
Q01.SI     QAF                           1.21           0.0       -0.04/-0.03    0.491          9.38      
AGS.SI     TheHourGlass                  0.68           0.0       -1.43/-0.74    0.442          9.58      
RW0U.SI    Mapletree G China             1.135          0.0        0.64/ 0.5     0.737          7.882     
D5IU.SI    Lippo Malls                   0.385          0.0        1.31/ 0.93    0.551          None      
ES3.SI     Straits Times ETF             2.94           0.203      3.69/ 2.28    0.918          None      
500.SI     Tai Sin Electric              0.38           0.0       -0.18/-0.18    0.473          7.17      
573.SI     Challenger Techonologies      0.495          0.0        0.49/ 0.27    0.283          9.167     
C6L.SI     SIA                           10.76          0.0        0.42/ 0.21    0.39           13.04     
E5H.SI     Golden Agri                   0.36           -0.519     0.05/ 0.02    1.135          40.0      
J91U.SI    Cambridge Industrial Trust    0.545          0.502      0.46/ 0.29    0.822          None      
O23.SI     OSIM Intl                     1.39           0.0       -0.19/-0.13    1.075          22.79     
D05.SI     DBS Bank                      15.55          0.0        1.5/ 0.71     1.244          9.09      
O39.SI     OCBC Bank                     8.85           -0.101     1.57/ 0.78    1.185          10.27     
U11.SI     UOB Bank                      18.78          0.0        2.12/ 1.27    1.082          9.73      
1163.KL    Allianz Malaysia              10.3           0.0        0.26/-0.05    0.329          11.55     
C          Citigroup                     47.79          0.142      0.18/ 0.09    1.782          10.116    
BAC        Bank of America               15.86          0.159     -0.3/-0.15     1.522          13.294    
XOM        ExxonMobil                    89.05          0.293      0.79/ 0.39    0.685          35.361    
CVX        Chevron Corp                  104.23         0.695      1.23/ 0.74    1.149          None      
Z74.SI     Singapore Telecomms           4.06           0.192      1.85/ 1.31    0.582          16.703    
3395.KL    Berjaya Corporation Berhad    0.34           0.0        0.19/-0.05    0.711          None      


Recommendation
===============
QAF Sell. Current:1.21 (1.15) 
TheHourGlass Buy. Current:0.68 (0.7) 
Mapletree G China Sell. Current:1.135 (1.0) 
Lippo Malls Sell. Current:0.385 (0.38) 
Straits Times ETF Check. Hist Perf:3.69 (2.8%) 
DBS Bank Check. Hist Perf:1.5 (3.5%) 
OCBC Bank Check. Hist Perf:1.57 (3.0%) 
UOB Bank Check. Hist Perf:2.12 (4.3%) 
Singapore Telecomms Check. Hist Perf:1.85 (4.9%) 

Time Finished: 20160909 07:41.20Time Now: 20160909 19:32.52
24 tickers loaded
Download failed. Retrying in 60 sec...
Download failed. Retrying in 60 sec...
Download failed. Retrying in 60 sec...
Download failed. Retrying in 60 sec...


Starting status check...
Ticker     Name                          Last           STDMov    Hist Perf      Beta           PE Ratio  
^STI       Straits Times Index           2873.33        0.138     nan/nan        1.0            None      
^KLSE      FTSE Bursa Malaysia           1691.38        -0.0      nan/nan        1.0            None      
^GSPC      S&P 500 Index                 2181.3         -0.103    nan/nan        1.0            None      
Q01.SI     QAF                           1.21           0.0        0.12/ 0.07    0.491          9.38      
AGS.SI     TheHourGlass                  0.68           0.0       -1.29/-0.64    0.442          9.58      
RW0U.SI    Mapletree G China             1.135          0.0        0.87/ 0.74    0.737          7.882     
D5IU.SI    Lippo Malls                   0.38           -0.346     0.95/ 0.68    0.551          None      
ES3.SI     Straits Times ETF             2.92           -0.203     3.65/ 2.28    0.918          None      
500.SI     Tai Sin Electric              0.38           0.0       -0.05/-0.05    0.473          7.17      
573.SI     Challenger Techonologies      0.495          0.0        0.54/ 0.35    0.283          9.167     
C6L.SI     SIA                           10.64          -0.571     0.07/ 0.03    0.39           12.895    
E5H.SI     Golden Agri                   0.365          0.0        0.58/ 0.28    1.135          40.556    
J91U.SI    Cambridge Industrial Trust    0.54           0.0        0.31/ 0.2     0.822          None      
O23.SI     OSIM Intl                     1.39           0.0       -0.06/-0.04    1.075          22.79     
D05.SI     DBS Bank                      15.55          0.0        2.14/ 1.03    1.244          9.09      
O39.SI     OCBC Bank                     8.75           -0.607     1.32/ 0.66    1.185          10.154    
U11.SI     UOB Bank                      18.69          -0.214     2.32/ 1.37    1.082          9.683     
1163.KL    Allianz Malaysia              10.3           0.0        0.25/-0.05    0.339          11.55     
C          Citigroup                     47.79          -0.0       0.18/ 0.09    1.782          10.11     
BAC        Bank of America               15.86          0.0       -0.3/-0.15     1.522          13.29     
XOM        ExxonMobil                    89.05          -0.0       0.79/ 0.39    0.685          35.37     
CVX        Chevron Corp                  104.23         0.056      1.23/ 0.74    1.149          None      
Z74.SI     Singapore Telecomms           4.01           -0.128     1.36/ 0.96    0.582          16.498    
3395.KL    Berjaya Corporation Berhad    0.34           0.0        0.18/-0.05    0.736          None      


Recommendation
===============
QAF Sell. Current:1.21 (1.15) 
TheHourGlass Buy. Current:0.68 (0.7) 
Mapletree G China Sell. Current:1.135 (1.0) 
Lippo Malls Sell. Current:0.38 (0.38) 
Straits Times ETF Check. Hist Perf:3.65 (2.1%) 
DBS Bank Check. Hist Perf:2.14 (3.5%) 
UOB Bank Check. Hist Perf:2.32 (3.8%) 

Time Finished: 20160909 19:46.54