Time Now: 20160513 07:39.55
12 tickers loaded


Starting status check...
Ticker     Name                          Last           STDMov    Hist Perf      Beta           PE Ratio  
^STI       Straits Times Index           2745.39        0.132     nan/nan        1.0            None      
Q01.SI     QAF                           1.065          0.0        0.66/ 0.43    0.442          10.758    
AGS.SI     TheHourGlass                  0.755          0.0        0.19/ 0.08    0.415          9.207     
RW0U.SI    Mapletree G China             0.965          0.0        0.05/ 0.02    0.741          6.186     
D5IU.SI    Lippo Malls                   0.325          0.0       -1.25/-0.95    0.533          None      
ES3.SI     Straits Times ETF             2.8            0.314      0.47/ 0.28    0.924          None      
500.SI     Tai Sin Electric              0.32           0.0        0.49/ 0.39    0.458          8.89      
573.SI     Challenger Techonologies      0.45           0.0       -0.05/-0.06    0.355          8.49      
C6L.SI     SIA                           11.63          0.0        1.68/ 0.88    0.46           21.98     
E5H.SI     Golden Agri                   0.36           0.0       -0.79/-0.36    1.116          None      
J91U.SI    Cambridge Industrial Trust    0.535          0.0        0.23/ 0.15    0.807          None      
O23.SI     OSIM Intl                     1.395          0.0       -0.04/-0.04    1.133          20.515    


Recommendation
===============
SIA Check. Hist Perf:1.68 (4.3%) 

Time Finished: 20160513 07:41.51Time Now: 20160513 19:33.24
12 tickers loaded


Starting status check...
Ticker     Name                          Last           STDMov    Hist Perf      Beta           PE Ratio  
^STI       Straits Times Index           2734.91        0.021     nan/nan        1.0            None      
Q01.SI     QAF                           1.065          0.0        0.72/ 0.48    0.442          10.758    
AGS.SI     TheHourGlass                  0.755          0.0        0.26/ 0.13    0.415          9.207     
RW0U.SI    Mapletree G China             0.965          0.0        0.17/ 0.14    0.741          6.186     
D5IU.SI    Lippo Malls                   0.33           0.718     -0.6/-0.46     0.533          None      
ES3.SI     Straits Times ETF             2.79           -0.111     0.46/ 0.3     0.924          None      
500.SI     Tai Sin Electric              0.32           0.0        0.55/ 0.46    0.458          8.89      
573.SI     Challenger Techonologies      0.45           0.0       -0.01/-0.02    0.355          8.49      
C6L.SI     SIA                           11.63          0.0        1.75/ 0.92    0.46           21.98     
E5H.SI     Golden Agri                   0.37           0.446     -0.04/-0.02    1.116          None      
J91U.SI    Cambridge Industrial Trust    0.535          0.0        0.37/ 0.26    0.807          None      
O23.SI     OSIM Intl                     1.385          -0.086    -0.09/-0.05    1.133          20.368    


Recommendation
===============
SIA Check. Hist Perf:1.75 (4.3%) 

Time Finished: 20160513 19:35.14