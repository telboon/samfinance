Time Now: 20170112 07:42.06
24 tickers loaded


Starting status check...
Ticker     Name                          Last           STDMov    Hist Perf      Beta           PE Ratio  
^STI       Straits Times Index           3000.94        -0.076    nan/nan        1.0            None      
^KLSE      FTSE Bursa Malaysia           1675.21        0.171     nan/nan        1.0            None      
^GSPC      S&P 500 Index                 2275.32        0.113     nan/nan        1.0            None      
Q01.SI     QAF                           1.41           0.0       -0.37/-0.06    0.357          9.86      
AGS.SI     TheHourGlass                  0.62           0.0       -0.35/-0.21    0.098          8.99      
RW0U.SI    Mapletree G China             0.97           0.0        0.21/ 0.04    0.684          6.748     
D5IU.SI    Lippo Malls                   0.37           0.0       -0.67/-0.39    0.111          None      
ES3.SI     Straits Times ETF             3.06           0.0        0.29/ 0.22    0.877          None      
500.SI     Tai Sin Electric              0.375          0.0        0.49/ 0.31    0.167          6.818     
573.SI     Challenger Techonologies      0.485          0.0       -1.03/-0.85    0.357          9.898     
C6L.SI     SIA                           9.84           0.0        0.28/ 0.01    0.291          14.14     
E5H.SI     Golden Agri                   0.43           0.0        0.15/-0.01    1.18           15.93     
J91U.SI    Cambridge Industrial Trust    0.555          0.0        0.46/ 0.22    0.681          None      
O23.SI     OSIM Intl                     1.39           0.0       -0.46/-0.49    1.296          None      
D05.SI     DBS Bank                      18.04          0.0       -0.3/-0.17     1.252          10.52     
O39.SI     OCBC Bank                     9.3            0.0       -0.54/-0.49    1.318          10.88     
U11.SI     UOB Bank                      20.87          0.0       -0.95/-0.47    1.025          11.24     
1163.KL    Allianz Malaysia              10.2           0.0       -0.28/-0.36    0.455          11.43     
C          Citigroup                     59.96          -0.058    -0.45/-0.18    2.066          13.171    
BAC        Bank of America               23.07          0.05       0.33/ 0.15    1.988          16.78     
XOM        ExxonMobil                    86.81          0.391     -1.05/-0.55    0.473          40.642    
CVX        Chevron Corp                  115.93         0.158     -0.65/-0.41    0.706          None      
Z74.SI     Singapore Telecomms           3.79           1.087      1.2/ 0.75     0.415          15.86     
3395.KL    Berjaya Corporation Berhad    0.355          0.0        0.85/ 0.46    0.446          None      


Recommendation
===============
QAF Sell. Current:1.41 (1.15) 
TheHourGlass Buy. Current:0.62 (0.7) 
SIA Buy. Current:9.84 (10.0) 
DBS Bank Sell. Current:18.04 (17.0) 
UOB Bank Sell. Current:20.87 (20.0) 
Citigroup Sell. Current:59.96 (48.0) 
Bank of America Sell. Current:23.07 (17.0) 
Singapore Telecomms Check. Day Delta:3.3% (3.0%) 

Time Finished: 20170112 07:47.22Time Now: 20170112 19:31.04
24 tickers loaded


Starting status check...
Ticker     Name                          Last           STDMov    Hist Perf      Beta           PE Ratio  
^STI       Straits Times Index           2993.0         -0.194    nan/nan        1.0            None      
^KLSE      FTSE Bursa Malaysia           1677.76        0.136     nan/nan        1.0            None      
^GSPC      S&P 500 Index                 2275.32        -0.0      nan/nan        1.0            None      
Q01.SI     QAF                           1.415          0.1       -0.19/ 0.04    0.357          9.895     
AGS.SI     TheHourGlass                  0.62           0.0       -0.34/-0.2     0.098          8.99      
RW0U.SI    Mapletree G China             0.96           -0.22     -0.16/-0.14    0.684          6.678     
D5IU.SI    Lippo Malls                   0.375          0.464     -0.06/-0.01    0.111          None      
ES3.SI     Straits Times ETF             3.06           0.0        0.54/ 0.44    0.877          None      
500.SI     Tai Sin Electric              0.385          1.732      1.72/ 1.2     0.167          7.0       
573.SI     Challenger Techonologies      0.475          -0.551    -1.57/-1.21    0.357          9.694     
C6L.SI     SIA                           9.76           -0.415    -0.17/-0.22    0.291          14.025    
E5H.SI     Golden Agri                   0.425          -0.204    -0.11/-0.14    1.18           15.745    
J91U.SI    Cambridge Industrial Trust    0.545          -0.955    -0.55/-0.34    0.681          None      
O23.SI     OSIM Intl                     1.39           0.0       -0.38/-0.41    1.296          None      
D05.SI     DBS Bank                      18.12          0.069      0.07/0.0      1.252          10.567    
O39.SI     OCBC Bank                     9.25           -0.176    -0.68/-0.55    1.318          10.822    
U11.SI     UOB Bank                      20.87          0.0       -0.79/-0.4     1.025          11.24     
1163.KL    Allianz Malaysia              10.24          0.213     -0.06/-0.27    0.446          11.475    
C          Citigroup                     59.96          0.0       -0.45/-0.19    2.049          13.04     
BAC        Bank of America               23.07          0.0        0.33/ 0.15    1.982          16.88     
XOM        ExxonMobil                    86.81          0.0       -1.05/-0.55    0.486          40.64     
CVX        Chevron Corp                  115.93         0.0       -0.65/-0.41    0.71           None      
Z74.SI     Singapore Telecomms           3.79           1.087      1.2/ 0.75     0.415          15.86     
3395.KL    Berjaya Corporation Berhad    0.36           0.624      1.31/ 0.77    0.456          None      


Recommendation
===============
QAF Sell. Current:1.415 (1.15) 
TheHourGlass Buy. Current:0.62 (0.7) 
Tai Sin Electric Check. Day STD:1.73 (1.0%) 
Tai Sin Electric Check. Hist Perf:1.72 (4.1%) 
Challenger Techonologies Check. Hist Perf:-1.57 (-5.0%) 
SIA Buy. Current:9.76 (10.0) 
DBS Bank Sell. Current:18.12 (17.0) 
UOB Bank Sell. Current:20.87 (20.0) 
Citigroup Sell. Current:59.96 (48.0) 
Bank of America Sell. Current:23.07 (17.0) 
Singapore Telecomms Check. Day Delta:3.3% (3.0%) 

Time Finished: 20170112 19:36.04