Time Now: 20170221 07:39.42
24 tickers loaded


Starting status check...
Ticker     Name                          Last           STDMov    Hist Perf      Beta           PE Ratio  
^STI       Straits Times Index           3096.69        -0.142    nan/nan        1.0            None      
^KLSE      FTSE Bursa Malaysia           1712.58        0.175     nan/nan        1.0            None      
^GSPC      S&P 500 Index                 2351.16        0.0       nan/nan        1.0            None      
Q01.SI     QAF                           1.54           0.0        0.21/ 0.17    0.195          10.66     
AGS.SI     TheHourGlass                  0.65           0.0       -0.29/-0.19    0.031          9.56      
RW0U.SI    Mapletree G China             0.965          0.0       -0.42/-0.22    0.475          6.796     
D5IU.SI    Lippo Malls                   0.395          0.0        1.19/ 0.73    0.017          39.5      
ES3.SI     Straits Times ETF             3.11           -0.143     0.29/ 0.26    0.892          None      
500.SI     Tai Sin Electric              0.405          0.0       -0.06/-0.09    0.167          7.105     
573.SI     Challenger Techonologies      0.48           0.0       -0.07/-0.06    0.292          9.8       
C6L.SI     SIA                           9.85           0.096     -0.05/-0.16    0.223          16.116    
E5H.SI     Golden Agri                   0.425          0.0       -0.66/-0.38    1.149          15.741    
J91U.SI    Cambridge Industrial Trust    0.585          0.0       -0.79/-0.46    0.544          117.0     
O23.SI     OSIM Intl                     1.39           0.0       -0.19/-0.11    0.585          None      
D05.SI     DBS Bank                      18.5           0.0        0.16/ 0.06    1.238          10.99     
O39.SI     OCBC Bank                     9.48           0.0       -0.35/-0.22    1.31           11.53     
U11.SI     UOB Bank                      21.11          0.0        0.56/ 0.25    1.184          None      
1163.KL    Allianz Malaysia              11.58          0.0       -0.08/-0.08    0.651          13.0      
C          Citigroup                     60.17          0.0       -0.25/-0.05    2.06           12.75     
BAC        Bank of America               24.52          0.0        0.25/ 0.09    1.974          16.35     
XOM        ExxonMobil                    81.76          -0.0      -0.91/-0.38    0.639          43.49     
CVX        Chevron Corp                  110.33         -0.0      -1.14/-0.63    0.663          None      
Z74.SI     Singapore Telecomms           3.97           0.771      0.49/ 0.29    0.581          16.542    
3395.KL    Berjaya Corporation Berhad    0.375          0.0       -0.91/-0.59    0.553          None      


Recommendation
===============
QAF Sell. Current:1.54 (1.15) 
TheHourGlass Buy. Current:0.65 (0.7) 
Lippo Malls Sell. Current:0.395 (0.38) 
SIA Buy. Current:9.85 (10.0) 
DBS Bank Sell. Current:18.5 (17.0) 
UOB Bank Sell. Current:21.11 (20.0) 
Citigroup Sell. Current:60.17 (48.0) 
Bank of America Sell. Current:24.52 (17.0) 

Time Finished: 20170221 07:44.44Time Now: 20170221 19:34.25
24 tickers loaded


Starting status check...
Ticker     Name                          Last           STDMov    Hist Perf      Beta           PE Ratio  
^STI       Straits Times Index           3094.19        -0.033    nan/nan        1.0            None      
^KLSE      FTSE Bursa Malaysia           1706.55        -0.21     nan/nan        1.0            None      
^GSPC      S&P 500 Index                 2351.16        0.0       nan/nan        1.0            None      
Q01.SI     QAF                           1.555          0.199      0.62/ 0.43    0.198          10.764    
AGS.SI     TheHourGlass                  0.65           0.0       -0.29/-0.19    0.032          9.56      
RW0U.SI    Mapletree G China             0.985          1.423      0.58/ 0.28    0.48           6.937     
D5IU.SI    Lippo Malls                   0.39           -0.549     0.61/ 0.36    0.027          39.0      
ES3.SI     Straits Times ETF             3.11           0.0        0.37/ 0.33    0.892          None      
500.SI     Tai Sin Electric              0.405          0.0       -0.05/-0.09    0.164          7.105     
573.SI     Challenger Techonologies      0.475          -0.31     -0.36/-0.24    0.299          9.698     
C6L.SI     SIA                           9.88           0.386      0.16/-0.02    0.223          16.165    
E5H.SI     Golden Agri                   0.4            -3.961    -2.5/-1.34     1.154          14.815    
J91U.SI    Cambridge Industrial Trust    0.59           0.275     -0.24/-0.14    0.543          118.0     
O23.SI     OSIM Intl                     1.39           0.0       -0.17/-0.1     0.585          None      
D05.SI     DBS Bank                      18.46          -0.072     0.11/ 0.04    1.239          10.966    
O39.SI     OCBC Bank                     9.48           0.0       -0.28/-0.17    1.314          11.53     
U11.SI     UOB Bank                      21.32          0.795      1.3/ 0.58     1.182          None      
1163.KL    Allianz Malaysia              11.46          -0.195    -0.54/-0.27    0.656          12.865    
C          Citigroup                     60.17          0.0       -0.25/-0.05    2.06           12.75     
BAC        Bank of America               24.52          0.0        0.25/ 0.09    1.974          16.35     
XOM        ExxonMobil                    81.76          -0.0      -0.91/-0.38    0.639          43.49     
CVX        Chevron Corp                  110.33         -0.0      -1.14/-0.63    0.663          None      
Z74.SI     Singapore Telecomms           3.97           0.771      0.49/ 0.29    0.581          16.542    
3395.KL    Berjaya Corporation Berhad    0.385          0.423      0.02/ 0.03    0.534          None      


Recommendation
===============
QAF Sell. Current:1.555 (1.15) 
TheHourGlass Buy. Current:0.65 (0.7) 
Lippo Malls Sell. Current:0.39 (0.38) 
SIA Buy. Current:9.88 (10.0) 
Golden Agri Check. Day Delta:-5.9% (5.0%) 
Golden Agri Check. Day STD:-3.96 (-2.5%) 
Golden Agri Check. Hist Perf:-2.5 (-7.0%) 
DBS Bank Sell. Current:18.46 (17.0) 
UOB Bank Sell. Current:21.32 (20.0) 
Citigroup Sell. Current:60.17 (48.0) 
Bank of America Sell. Current:24.52 (17.0) 

Time Finished: 20170221 19:39.34