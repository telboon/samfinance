Time Now: 20160707 07:35.53
12 tickers loaded


Starting status check...
Ticker     Name                          Last           STDMov    Hist Perf      Beta           PE Ratio  
^STI       Straits Times Index           2864.67        0.0       nan/nan        1.0            None      
Q01.SI     QAF                           1.11           0.0        1.9/ 1.19     0.491          11.21     
AGS.SI     TheHourGlass                  0.81           0.0        0.66/ 0.29    0.442          10.95     
RW0U.SI    Mapletree G China             1.02           0.0        0.16/ 0.1     0.737          6.54      
D5IU.SI    Lippo Malls                   0.35           0.0       -0.17/-0.12    0.551          None      
ES3.SI     Straits Times ETF             2.9            0.0       -0.15/-0.13    0.918          None      
500.SI     Tai Sin Electric              0.345          0.0       -0.14/-0.14    0.473          8.415     
573.SI     Challenger Techonologies      0.445          0.0       -0.06/-0.09    0.283          8.396     
C6L.SI     SIA                           10.73          0.0        0.14/ 0.06    0.39           15.62     
E5H.SI     Golden Agri                   0.36           0.0        0.45/ 0.21    1.135          60.0      
J91U.SI    Cambridge Industrial Trust    0.555          0.0        0.1/ 0.06     0.822          None      
O23.SI     OSIM Intl                     1.39           0.0       -0.16/-0.11    1.075          22.79     


Recommendation
===============
QAF Check. Hist Perf:1.9 (4.7%) 
Mapletree G China Sell. Current:1.02 (1.0) 

Time Finished: 20160707 07:39.18Time Now: 20160707 19:43.14
12 tickers loaded


Starting status check...
Ticker     Name                          Last           STDMov    Hist Perf      Beta           PE Ratio  
^STI       Straits Times Index           2864.67        0.0       nan/nan        1.0            None      
Q01.SI     QAF                           1.11           0.0        1.9/ 1.19     0.491          11.21     
AGS.SI     TheHourGlass                  0.81           0.0        0.66/ 0.29    0.442          10.95     
RW0U.SI    Mapletree G China             1.02           0.0        0.16/ 0.1     0.737          6.54      
D5IU.SI    Lippo Malls                   0.355          0.614      0.36/ 0.26    0.551          None      
ES3.SI     Straits Times ETF             2.9            0.0       -0.15/-0.13    0.918          None      
500.SI     Tai Sin Electric              0.345          0.0       -0.14/-0.14    0.473          8.415     
573.SI     Challenger Techonologies      0.445          0.0       -0.06/-0.09    0.283          8.396     
C6L.SI     SIA                           10.73          0.0        0.14/ 0.06    0.39           15.62     
E5H.SI     Golden Agri                   0.36           0.0        0.45/ 0.21    1.135          60.0      
J91U.SI    Cambridge Industrial Trust    0.555          0.0        0.1/ 0.06     0.822          None      
O23.SI     OSIM Intl                     1.39           0.0       -0.16/-0.11    1.075          22.79     


Recommendation
===============
QAF Check. Hist Perf:1.9 (4.7%) 
Mapletree G China Sell. Current:1.02 (1.0) 

Time Finished: 20160707 19:45.43