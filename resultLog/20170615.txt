Time Now: 20170615 07:42.15
26 tickers loaded


Starting status check...
Ticker     Name                          Last           STDMov    Hist Perf      Beta           PE Ratio  
^STI       Straits Times Index           3253.43        -0.09     nan/nan        1.0            None      
^KLSE      FTSE Bursa Malaysia           1792.35        0.314     nan/nan        1.0            None      
^GSPC      S&P 500 Index                 2437.92        -0.09     nan/nan        1.0            None      
Q01.SI     QAF                           1.335          -0.392     0.08/ 0.09    0.22           6.333     
AGS.SI     TheHourGlass                  0.665          -0.38     -1.6/-1.17     0.226          9.786     
AXL        American Axle                 15.84          -0.225    -0.66/-0.58    -0.397         4.865     
CALM       Cal-Maine Foods               39.65          0.024      1.28/ 1.08    -0.449         None      
D05.SI     DBS Bank                      20.59          -0.274    -0.04/-0.08    1.205          12.326    
O39.SI     OCBC Bank                     10.72          -0.049     0.37/ 0.16    1.109          12.636    
U11.SI     UOB Bank                      23.68          0.063      0.37/ 0.19    1.126          12.747    
RW0U.SI    Mapletree G China             1.085          -0.504    -0.88/-0.39    0.525          8.061     
D5IU.SI    Lippo Malls                   0.425          -0.358    -0.13/-0.01    0.375          38.187    
ES3.SI     Straits Times ETF             3.3            0.0       -0.34/-0.35    0.866          None      
500.SI     Tai Sin Electric              0.435          0.404     -0.13/-0.13    0.438          7.628     
573.SI     Challenger Techonologies      0.465          0.0        1.25/ 0.9     -0.391         13.676    
C6L.SI     SIA                           10.04          0.137      0.12/-0.03    0.591          33.239    
E5H.SI     Golden Agri                   0.375          0.0        0.64/ 0.38    1.064          13.7      
J91U.SI    Cambridge Industrial Trust    0.58           0.591     -0.15/-0.09    0.344          116.0     
O23.SI     OSIM Intl                     1.39           0.0       nan/nan        0.0            None      
1163.KL    Allianz Malaysia              12.8           0.0       -0.13/-0.15    0.659          14.37     
C          Citigroup                     64.72          0.018      2.06/ 0.96    1.824          12.966    
BAC        Bank of America               23.76          -0.011     1.6/ 0.69     1.881          14.624    
XOM        ExxonMobil                    82.07          -1.229     0.76/ 0.39    0.706          34.229    
CVX        Chevron Corp                  106.6          -0.631     1.49/ 0.7     0.478          69.207    
Z74.SI     Singapore Telecomms           3.77           0.211      0.2/ 0.11     0.493          15.854    
3395.KL    Berjaya Corporation Berhad    0.36           0.311      1.4/ 0.79     0.263          None      


Recommendation
===============
TheHourGlass Check. Hist Perf:-1.6 (-4.3%) 
Citigroup Check. Hist Perf:2.06 (4.6%) 
Bank of America Check. Hist Perf:1.6 (5.1%) 

Time Finished: 20170615 07:50.50Time Now: 20170615 19:39.24
26 tickers loaded
Download failed. Retrying in 60 sec...
Download failed. Retrying in 60 sec...


Starting status check...
Ticker     Name                          Last           STDMov    Hist Perf      Beta           PE Ratio  
^STI       Straits Times Index           3228.62        -0.637    nan/nan        1.0            None      
^KLSE      FTSE Bursa Malaysia           1790.01        0.221     nan/nan        1.0            None      
^GSPC      S&P 500 Index                 2437.92        -0.09     nan/nan        1.0            None      
Q01.SI     QAF                           1.33           -0.588    0.0/-0.0       0.22           6.309     
AGS.SI     TheHourGlass                  0.67           0.0       -1.28/-0.84    0.226          9.86      
AXL        American Axle                 15.84          -0.225    -0.66/-0.58    -0.397         4.865     
CALM       Cal-Maine Foods               39.65          0.024      1.28/ 1.08    -0.449         None      
D05.SI     DBS Bank                      20.41          -0.534    -0.01/-0.0     1.205          12.218    
O39.SI     OCBC Bank                     10.62          -0.296     0.31/ 0.16    1.109          12.519    
U11.SI     UOB Bank                      23.15          -0.607    -0.48/-0.23    1.126          12.462    
RW0U.SI    Mapletree G China             1.1            -0.126     0.02/ 0.01    0.525          8.173     
D5IU.SI    Lippo Malls                   0.425          -0.358     0.01/0.0      0.375          38.187    
ES3.SI     Straits Times ETF             3.27           -0.512    -0.61/-0.53    0.866          None      
500.SI     Tai Sin Electric              0.435          0.404      0.01/ 0.01    0.438          7.628     
573.SI     Challenger Techonologies      0.455          -1.382     0.38/ 0.27    -0.391         13.382    
C6L.SI     SIA                           9.94           -0.32     -0.24/-0.14    0.591          32.908    
E5H.SI     Golden Agri                   0.37           -0.433     0.46/ 0.22    1.064          13.517    
J91U.SI    Cambridge Industrial Trust    0.59           1.774      1.06/ 0.56    0.344          118.0     
O23.SI     OSIM Intl                     1.39           0.0       nan/nan        0.0            None      
1163.KL    Allianz Malaysia              12.76          -0.096    -0.25/-0.19    0.659          14.325    
C          Citigroup                     64.72          0.018      2.06/ 0.96    1.824          12.966    
BAC        Bank of America               23.76          -0.011     1.6/ 0.69     1.881          14.624    
XOM        ExxonMobil                    82.07          -1.229     0.76/ 0.39    0.706          34.229    
CVX        Chevron Corp                  106.6          -0.631     1.49/ 0.7     0.478          69.207    
Z74.SI     Singapore Telecomms           3.77           0.211      0.2/ 0.11     0.493          15.854    
3395.KL    Berjaya Corporation Berhad    0.355          0.0        0.93/ 0.53    0.263          None      


Recommendation
===============
Cambridge Industrial Trust Check. Day STD:1.77 (1.5%) 
Citigroup Check. Hist Perf:2.06 (4.6%) 
Bank of America Check. Hist Perf:1.6 (5.1%) 

Time Finished: 20170615 19:52.35