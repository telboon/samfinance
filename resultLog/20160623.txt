Time Now: 20160623 07:34.12
12 tickers loaded


Starting status check...
Ticker     Name                          Last           STDMov    Hist Perf      Beta           PE Ratio  
^STI       Straits Times Index           2786.13        -0.055    nan/nan        1.0            None      
Q01.SI     QAF                           1.06           0.0       -1.08/-0.68    0.469          10.71     
AGS.SI     TheHourGlass                  0.805          0.0        0.02/-0.03    0.461          10.878    
RW0U.SI    Mapletree G China             0.97           0.0       -0.86/-0.79    0.741          6.22      
D5IU.SI    Lippo Malls                   0.345          0.0        0.88/ 0.63    0.519          None      
ES3.SI     Straits Times ETF             2.85           0.0        2.01/ 1.19    0.927          None      
500.SI     Tai Sin Electric              0.34           0.0        0.32/ 0.21    0.472          8.29      
573.SI     Challenger Techonologies      0.475          0.0        1.42/ 0.9     0.334          8.962     
C6L.SI     SIA                           10.56          0.0        0.03/-0.0     0.395          15.37     
E5H.SI     Golden Agri                   0.35           0.0       -0.34/-0.18    1.158          58.33     
J91U.SI    Cambridge Industrial Trust    0.54           0.0       -0.88/-0.6     0.818          None      
O23.SI     OSIM Intl                     1.39           0.0       -0.24/-0.15    1.129          22.79     


Recommendation
===============
Straits Times ETF Check. Hist Perf:2.01 (2.2%) 

Time Finished: 20160623 07:36.16Time Now: 20160623 19:38.29
12 tickers loaded


Starting status check...
Ticker     Name                          Last           STDMov    Hist Perf      Beta           PE Ratio  
^STI       Straits Times Index           2802.24        0.213     nan/nan        1.0            None      
Q01.SI     QAF                           1.06           0.0       -1.2/-0.76     0.469          10.71     
AGS.SI     TheHourGlass                  0.805          0.0       -0.1/-0.1      0.461          10.878    
RW0U.SI    Mapletree G China             0.97           0.0       -1.05/-0.98    0.741          6.22      
D5IU.SI    Lippo Malls                   0.345          0.0        0.76/ 0.54    0.519          None      
ES3.SI     Straits Times ETF             2.85           0.0        0.9/ 0.49     0.927          None      
500.SI     Tai Sin Electric              0.34           0.0        0.22/ 0.11    0.472          8.29      
573.SI     Challenger Techonologies      0.475          0.0        1.36/ 0.83    0.334          8.962     
C6L.SI     SIA                           10.62          0.143      0.17/ 0.06    0.395          15.457    
E5H.SI     Golden Agri                   0.35           0.0       -0.5/-0.26     1.158          58.33     
J91U.SI    Cambridge Industrial Trust    0.54           0.0       -1.09/-0.76    0.818          None      
O23.SI     OSIM Intl                     1.39           0.0       -0.36/-0.22    1.129          22.79     


Recommendation
===============

Time Finished: 20160623 19:40.34