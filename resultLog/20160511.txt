Time Now: 20160511 07:35.13
12 tickers loaded


Starting status check...
Ticker     Name                          Last           STDMov    Hist Perf      Beta           PE Ratio  
^STI       Straits Times Index           2741.15        -0.236    nan/nan        1.0            None      
Q01.SI     QAF                           1.055          0.0       -0.7/-0.46     0.442          11.223    
AGS.SI     TheHourGlass                  0.755          0.0        0.49/ 0.3     0.418          9.207     
RW0U.SI    Mapletree G China             0.97           0.0        0.81/ 0.77    0.741          7.08      
D5IU.SI    Lippo Malls                   0.33           0.0       -0.34/-0.24    0.532          None      
ES3.SI     Straits Times ETF             2.79           -0.192     0.73/ 0.55    0.925          None      
500.SI     Tai Sin Electric              0.325          0.0        0.76/ 0.7     0.46           9.028     
573.SI     Challenger Techonologies      0.45           0.0       -1.07/-0.69    0.348          8.49      
C6L.SI     SIA                           11.34          0.0        0.52/ 0.28    0.463          21.44     
E5H.SI     Golden Agri                   0.365          0.0       -0.94/-0.41    1.108          None      
J91U.SI    Cambridge Industrial Trust    0.525          0.0       -0.83/-0.56    0.807          None      
O23.SI     OSIM Intl                     1.39           0.0        0.22/ 0.14    1.138          20.44     


Recommendation
===============

Time Finished: 20160511 07:37.09Time Now: 20160511 19:43.55
12 tickers loaded


Starting status check...
Ticker     Name                          Last           STDMov    Hist Perf      Beta           PE Ratio  
^STI       Straits Times Index           2725.27        -0.387    nan/nan        1.0            None      
Q01.SI     QAF                           1.055          0.0       -0.61/-0.38    0.442          11.223    
AGS.SI     TheHourGlass                  0.755          0.0        0.59/ 0.37    0.418          9.207     
RW0U.SI    Mapletree G China             0.975          0.133      1.21/ 1.15    0.741          7.116     
D5IU.SI    Lippo Malls                   0.325          -0.662    -0.79/-0.58    0.532          None      
ES3.SI     Straits Times ETF             2.78           -0.099     1.09/ 0.82    0.925          None      
500.SI     Tai Sin Electric              0.325          0.0        0.86/ 0.81    0.46           9.028     
573.SI     Challenger Techonologies      0.45           0.0       -1.01/-0.63    0.348          8.49      
C6L.SI     SIA                           11.65          3.0        1.77/ 0.94    0.463          22.026    
E5H.SI     Golden Agri                   0.365          0.0       -0.79/-0.34    1.108          None      
J91U.SI    Cambridge Industrial Trust    0.525          0.0       -0.62/-0.4     0.807          None      
O23.SI     OSIM Intl                     1.39           0.0        0.33/ 0.21    1.138          20.44     


Recommendation
===============
SIA Check. Day STD:3.0 (31.0%) 
SIA Check. Hist Perf:1.77 (3.5%) 

Time Finished: 20160511 19:45.51