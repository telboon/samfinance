Time Now: 20160701 07:32.32
12 tickers loaded


Starting status check...
Ticker     Name                          Last           STDMov    Hist Perf      Beta           PE Ratio  
^STI       Straits Times Index           2834.29        0.674     nan/nan        1.0            None      
Q01.SI     QAF                           1.06           0.0        0.1/ 0.03     0.47           10.71     
AGS.SI     TheHourGlass                  0.795          0.0       -1.52/-0.85    0.456          10.743    
RW0U.SI    Mapletree G China             1.015          0.298      0.85/ 0.55    0.739          6.506     
D5IU.SI    Lippo Malls                   0.355          0.751      1.54/ 1.1     0.523          None      
ES3.SI     Straits Times ETF             2.88           0.851     -0.99/-0.74    0.926          None      
500.SI     Tai Sin Electric              0.345          0.0        0.45/ 0.22    0.477          8.415     
573.SI     Challenger Techonologies      0.445          0.0       -1.81/-1.39    0.35           8.396     
C6L.SI     SIA                           10.68          0.047      0.54/ 0.23    0.384          15.55     
E5H.SI     Golden Agri                   0.35           0.0       -0.29/-0.19    1.164          58.33     
J91U.SI    Cambridge Industrial Trust    0.55           0.0        0.37/ 0.19    0.816          None      
O23.SI     OSIM Intl                     1.39           0.0       -0.71/-0.45    1.143          22.79     


Recommendation
===============
TheHourGlass Check. Hist Perf:-1.52 (-1.9%) 
Mapletree G China Sell. Current:1.015 (1.0) 
Lippo Malls Check. Hist Perf:1.54 (6.0%) 
Challenger Techonologies Check. Hist Perf:-1.81 (-5.3%) 

Time Finished: 20160701 07:34.35Time Now: 20160701 19:32.14
12 tickers loaded


Starting status check...
Ticker     Name                          Last           STDMov    Hist Perf      Beta           PE Ratio  
^STI       Straits Times Index           2841.02        0.783     nan/nan        1.0            None      
Q01.SI     QAF                           1.06           0.0        0.05/-0.0     0.47           10.71     
AGS.SI     TheHourGlass                  0.795          0.0       -1.56/-0.88    0.456          10.743    
RW0U.SI    Mapletree G China             1.01           0.0        0.55/ 0.28    0.739          6.474     
D5IU.SI    Lippo Malls                   0.35           0.0        0.93/ 0.66    0.523          None      
ES3.SI     Straits Times ETF             2.88           0.0       -1.45/-1.03    0.926          None      
500.SI     Tai Sin Electric              0.345          0.0        0.41/ 0.18    0.477          8.415     
573.SI     Challenger Techonologies      0.445          0.0       -1.83/-1.42    0.35           8.396     
C6L.SI     SIA                           10.67          0.023      0.46/ 0.18    0.384          15.535    
E5H.SI     Golden Agri                   0.35           0.0       -0.36/-0.23    1.164          58.33     
J91U.SI    Cambridge Industrial Trust    0.55           0.0        0.28/ 0.13    0.816          None      
O23.SI     OSIM Intl                     1.39           0.0       -0.76/-0.48    1.143          22.79     


Recommendation
===============
Straits Times Index Check. Day Delta:1.7% (1.5%) 
TheHourGlass Check. Hist Perf:-1.56 (-1.9%) 
Mapletree G China Sell. Current:1.01 (1.0) 
Challenger Techonologies Check. Hist Perf:-1.83 (-5.3%) 

Time Finished: 20160701 19:34.22