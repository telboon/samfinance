Time Now: 20170427 07:43.23
24 tickers loaded


Starting status check...
Ticker     Name                          Last           STDMov    Hist Perf      Beta           PE Ratio  
^STI       Straits Times Index           3173.76        0.25      nan/nan        1.0            None      
^KLSE      FTSE Bursa Malaysia           1768.92        0.132     nan/nan        1.0            None      
^GSPC      S&P 500 Index                 2387.45        -0.039    nan/nan        1.0            None      
Q01.SI     QAF                           1.38           0.0        0.1/ 0.14     0.405          6.45      
AGS.SI     TheHourGlass                  0.67           0.0       -0.07/-0.16    0.183          9.85      
RW0U.SI    Mapletree G China             1.06           0.0        0.6/ 0.26     0.718          7.43      
D5IU.SI    Lippo Malls                   0.415          0.0        1.12/ 0.66    0.19           45.56     
ES3.SI     Straits Times ETF             3.19           0.258      0.3/ 0.18     0.868          None      
500.SI     Tai Sin Electric              0.435          0.0       -0.16/-0.21    0.342          7.632     
573.SI     Challenger Techonologies      0.47           0.0       -0.02/ 0.09    0.051          13.06     
C6L.SI     SIA                           10.3           0.0        0.68/ 0.24    0.373          16.86     
E5H.SI     Golden Agri                   0.36           0.0        0.86/ 0.56    1.306          11.61     
J91U.SI    Cambridge Industrial Trust    0.58           0.0       -0.33/-0.22    0.473          116.0     
O23.SI     OSIM Intl                     1.39           0.0       -0.27/-0.17    0.585          None      
D05.SI     DBS Bank                      19.31          0.0        0.48/ 0.11    1.017          11.64     
O39.SI     OCBC Bank                     9.76           0.0        0.26/ 0.17    1.148          11.87     
U11.SI     UOB Bank                      21.71          0.0       -0.96/-0.38    1.111          11.74     
1163.KL    Allianz Malaysia              11.3           0.0       -1.64/-1.18    0.65           12.67     
C          Citigroup                     60.1           -0.072     0.15/ 0.1     1.897          11.958    
BAC        Bank of America               23.89          -0.093     0.27/-0.0     2.057          14.456    
XOM        ExxonMobil                    81.4           -0.339    -0.23/-0.01    0.659          43.036    
CVX        Chevron Corp                  106.08         -0.252     0.22/ 0.08    0.567          None      
Z74.SI     Singapore Telecomms           3.74           -0.132    -0.33/-0.31    0.573          15.578    
3395.KL    Berjaya Corporation Berhad    0.355          0.0        0.24/-0.03    0.463          None      


Recommendation
===============
QAF Sell. Current:1.38 (1.15) 
TheHourGlass Buy. Current:0.67 (0.7) 
Mapletree G China Sell. Current:1.06 (1.0) 
Lippo Malls Sell. Current:0.415 (0.38) 
DBS Bank Sell. Current:19.31 (17.0) 
UOB Bank Sell. Current:21.71 (20.0) 
Allianz Malaysia Check. Hist Perf:-1.64 (-1.7%) 
Citigroup Sell. Current:60.1 (48.0) 
Bank of America Sell. Current:23.89 (17.0) 

Time Finished: 20170427 07:48.22Time Now: 20170427 19:41.58
24 tickers loaded


Starting status check...
Ticker     Name                          Last           STDMov    Hist Perf      Beta           PE Ratio  
^STI       Straits Times Index           3171.36        0.189     nan/nan        1.0            None      
^KLSE      FTSE Bursa Malaysia           1767.92        -0.042    nan/nan        1.0            None      
^GSPC      S&P 500 Index                 2387.45        0.0       nan/nan        1.0            None      
Q01.SI     QAF                           1.35           -0.423    -0.69/-0.28    0.405          6.31      
AGS.SI     TheHourGlass                  0.67           0.0       -0.07/-0.15    0.183          9.85      
RW0U.SI    Mapletree G China             1.045          -0.493    -0.15/-0.07    0.718          7.325     
D5IU.SI    Lippo Malls                   0.43           2.072      2.96/ 1.72    0.19           47.207    
ES3.SI     Straits Times ETF             3.19           0.0        0.37/ 0.25    0.868          None      
500.SI     Tai Sin Electric              0.44           0.209      0.32/ 0.08    0.342          7.72      
573.SI     Challenger Techonologies      0.47           0.0       -0.02/ 0.08    0.051          13.06     
C6L.SI     SIA                           10.33          0.206      0.89/ 0.38    0.373          16.909    
E5H.SI     Golden Agri                   0.355          -0.196     0.45/ 0.35    1.306          11.449    
J91U.SI    Cambridge Industrial Trust    0.575          -0.45     -0.84/-0.5     0.473          115.0     
O23.SI     OSIM Intl                     1.39           0.0       -0.25/-0.16    0.585          None      
D05.SI     DBS Bank                      19.21          -0.395     0.25/ 0.02    1.017          11.58     
O39.SI     OCBC Bank                     9.74           -0.205     0.17/ 0.12    1.148          11.846    
U11.SI     UOB Bank                      21.47          -0.533    -1.63/-0.69    1.111          11.61     
1163.KL    Allianz Malaysia              11.5           0.934     -0.58/-0.61    0.646          12.894    
C          Citigroup                     59.94          -0.106     0.03/ 0.05    1.895          12.098    
BAC        Bank of America               23.89          0.0        0.27/-0.0     2.051          14.73     
XOM        ExxonMobil                    81.4           -0.0      -0.24/-0.01    0.664          43.54     
CVX        Chevron Corp                  106.08         -0.0       0.22/ 0.08    0.569          None      
Z74.SI     Singapore Telecomms           3.75           0.128     -0.21/-0.24    0.562          15.622    
3395.KL    Berjaya Corporation Berhad    0.35           -0.375    -0.22/-0.31    0.438          None      


Recommendation
===============
QAF Sell. Current:1.35 (1.15) 
TheHourGlass Buy. Current:0.67 (0.7) 
Mapletree G China Sell. Current:1.045 (1.0) 
Lippo Malls Sell. Current:0.43 (0.38) 
Lippo Malls Check. Day Delta:3.6% (3.0%) 
Lippo Malls Check. Day STD:2.07 (1.5%) 
Lippo Malls Check. Hist Perf:2.96 (6.2%) 
DBS Bank Sell. Current:19.21 (17.0) 
UOB Bank Sell. Current:21.47 (20.0) 
UOB Bank Check. Hist Perf:-1.63 (-1.3%) 
Citigroup Sell. Current:59.94 (48.0) 
Bank of America Sell. Current:23.89 (17.0) 

Time Finished: 20170427 19:46.44