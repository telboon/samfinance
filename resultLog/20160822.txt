Time Now: 20160822 07:35.57
24 tickers loaded


Starting status check...
Ticker     Name                          Last           STDMov    Hist Perf      Beta           PE Ratio  
^STI       Straits Times Index           2844.02        -0.329    nan/nan        1.0            None      
^KLSE      FTSE Bursa Malaysia           1687.68        -0.0      nan/nan        1.0            None      
^GSPC      S&P 500 Index                 2183.87        -0.0      nan/nan        1.0            None      
Q01.SI     QAF                           1.2            0.0        0.53/ 0.33    0.491          9.3       
AGS.SI     TheHourGlass                  0.75           0.0        0.14/ 0.09    0.442          10.56     
RW0U.SI    Mapletree G China             1.115          0.0        1.42/ 1.26    0.737          7.743     
D5IU.SI    Lippo Malls                   0.38           0.0        0.65/ 0.46    0.551          None      
ES3.SI     Straits Times ETF             2.89           0.0       -0.05/-0.0     0.918          None      
500.SI     Tai Sin Electric              0.36           0.0       -0.37/-0.26    0.473          8.78      
573.SI     Challenger Techonologies      0.485          0.0       -0.22/-0.11    0.283          8.981     
C6L.SI     SIA                           10.69          0.0       -0.84/-0.44    0.39           12.96     
E5H.SI     Golden Agri                   0.365          0.0       -0.13/-0.05    1.135          40.556    
J91U.SI    Cambridge Industrial Trust    0.54           0.0        0.27/ 0.19    0.822          None      
O23.SI     OSIM Intl                     1.39           0.0        0.13/ 0.09    1.075          22.79     
D05.SI     DBS Bank                      14.89          0.0        0.76/ 0.39    1.244          8.7       
O39.SI     OCBC Bank                     8.45           0.0        0.34/ 0.18    1.185          9.8       
U11.SI     UOB Bank                      17.56          0.0        0.18/ 0.09    1.082          9.09      
1163.KL    Allianz Malaysia              10.26          0.0        1.76/ 0.93    0.357          11.5      
C          Citigroup                     46.53          0.0        0.3/ 0.18     1.76           9.85      
BAC        Bank of America               15.22          0.0        0.57/ 0.3     1.535          12.76     
XOM        ExxonMobil                    87.8           -0.0       0.08/ 0.03    0.72           34.87     
CVX        Chevron Corp                  102.32         0.0       -0.04/-0.02    1.171          None      
Z74.SI     Singapore Telecomms           4.19           0.62       1.15/ 0.8     0.545          17.243    
3395.KL    Berjaya Corporation Berhad    0.345          0.0        0.03/ 0.04    0.671          None      


Recommendation
===============
QAF Sell. Current:1.2 (1.15) 
Mapletree G China Sell. Current:1.115 (1.0) 
Lippo Malls Sell. Current:0.38 (0.38) 
Allianz Malaysia Check. Hist Perf:1.76 (2.6%) 

Time Finished: 20160822 07:41.08Time Now: 20160822 19:40.29
24 tickers loaded


Starting status check...
Ticker     Name                          Last           STDMov    Hist Perf      Beta           PE Ratio  
^STI       Straits Times Index           2844.02        -0.329    nan/nan        1.0            None      
^KLSE      FTSE Bursa Malaysia           1687.68        -0.0      nan/nan        1.0            None      
^GSPC      S&P 500 Index                 2183.87        -0.0      nan/nan        1.0            None      
Q01.SI     QAF                           1.195          -0.146     0.34/ 0.22    0.491          9.261     
AGS.SI     TheHourGlass                  0.745          -0.204    -0.15/-0.05    0.442          10.49     
RW0U.SI    Mapletree G China             1.11           -0.117     1.22/ 1.09    0.737          7.708     
D5IU.SI    Lippo Malls                   0.38           0.0        0.65/ 0.46    0.551          None      
ES3.SI     Straits Times ETF             2.88           -0.191    -0.74/-0.43    0.918          None      
500.SI     Tai Sin Electric              0.365          0.392      0.12/ 0.12    0.473          8.902     
573.SI     Challenger Techonologies      0.485          0.0       -0.22/-0.11    0.283          8.981     
C6L.SI     SIA                           10.69          0.0       -0.84/-0.44    0.39           12.96     
E5H.SI     Golden Agri                   0.365          0.0       -0.13/-0.05    1.135          40.556    
J91U.SI    Cambridge Industrial Trust    0.545          0.508      0.68/ 0.47    0.822          None      
O23.SI     OSIM Intl                     1.39           0.0        0.13/ 0.09    1.075          22.79     
D05.SI     DBS Bank                      14.89          0.0        0.76/ 0.39    1.244          8.7       
O39.SI     OCBC Bank                     8.45           0.0        0.34/ 0.18    1.185          9.8       
U11.SI     UOB Bank                      17.56          0.0        0.18/ 0.09    1.082          9.09      
1163.KL    Allianz Malaysia              10.26          0.0        1.76/ 0.93    0.357          11.5      
C          Citigroup                     46.53          0.0        0.3/ 0.18     1.76           9.85      
BAC        Bank of America               15.22          0.0        0.57/ 0.3     1.535          12.76     
XOM        ExxonMobil                    87.8           -0.0       0.08/ 0.03    0.72           34.87     
CVX        Chevron Corp                  102.32         0.0       -0.04/-0.02    1.171          None      
Z74.SI     Singapore Telecomms           4.19           0.62       1.15/ 0.8     0.545          17.243    
3395.KL    Berjaya Corporation Berhad    0.34           -0.636    -0.39/-0.19    0.671          None      


Recommendation
===============
QAF Sell. Current:1.195 (1.15) 
Mapletree G China Sell. Current:1.11 (1.0) 
Lippo Malls Sell. Current:0.38 (0.38) 
Allianz Malaysia Check. Hist Perf:1.76 (2.6%) 

Time Finished: 20160822 19:44.13