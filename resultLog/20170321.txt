Time Now: 20170321 07:37.37
24 tickers loaded


Starting status check...
Ticker     Name                          Last           STDMov    Hist Perf      Beta           PE Ratio  
^STI       Straits Times Index           3165.7         -0.044    nan/nan        1.0            None      
^KLSE      FTSE Bursa Malaysia           1749.41        0.131     nan/nan        1.0            None      
^GSPC      S&P 500 Index                 2373.47        -0.101    nan/nan        1.0            None      
Q01.SI     QAF                           1.39           0.0        0.71/ 0.46    0.301          6.519     
AGS.SI     TheHourGlass                  0.665          0.0       -0.29/-0.19    0.106          9.779     
RW0U.SI    Mapletree G China             0.97           0.0       -0.42/-0.21    0.465          6.83      
D5IU.SI    Lippo Malls                   0.385          0.0       -0.66/-0.38    0.094          39.0      
ES3.SI     Straits Times ETF             3.18           -0.148     0.36/ 0.31    0.872          None      
500.SI     Tai Sin Electric              0.45           0.0        0.89/ 0.55    0.316          7.89      
573.SI     Challenger Techonologies      0.48           0.0       -0.01/-0.04    0.034          13.33     
C6L.SI     SIA                           10.0           0.0       -0.22/-0.26    0.338          16.37     
E5H.SI     Golden Agri                   0.385          0.0        0.56/ 0.3     1.179          12.419    
J91U.SI    Cambridge Industrial Trust    0.575          0.0        1.51/ 0.78    0.409          115.0     
O23.SI     OSIM Intl                     1.39           0.0       -0.17/-0.1     0.585          None      
D05.SI     DBS Bank                      19.01          0.0       -0.84/-0.39    1.025          11.29     
O39.SI     OCBC Bank                     9.63           0.0       -0.56/-0.33    1.264          11.67     
U11.SI     UOB Bank                      21.86          0.0        0.42/ 0.19    1.182          11.82     
1163.KL    Allianz Malaysia              11.88          0.0        0.93/ 0.16    0.546          13.16     
C          Citigroup                     59.59          -0.438    -1.46/-0.65    1.973          12.625    
BAC        Bank of America               24.44          -0.377    -1.34/-0.59    1.977          16.329    
XOM        ExxonMobil                    82.0           0.0        0.57/ 0.28    0.645          43.69     
CVX        Chevron Corp                  107.66         -0.006     0.05/ 0.03    0.565          None      
Z74.SI     Singapore Telecomms           4.0            0.459      0.34/ 0.19    0.61           16.665    
3395.KL    Berjaya Corporation Berhad    0.395          0.0       -0.3/-0.38     0.557          None      


Recommendation
===============
QAF Sell. Current:1.39 (1.15) 
TheHourGlass Buy. Current:0.665 (0.7) 
Lippo Malls Sell. Current:0.385 (0.38) 
SIA Buy. Current:10.0 (10.0) 
Cambridge Industrial Trust Check. Hist Perf:1.51 (2.7%) 
DBS Bank Sell. Current:19.01 (17.0) 
UOB Bank Sell. Current:21.86 (20.0) 
Citigroup Sell. Current:59.59 (48.0) 
Bank of America Sell. Current:24.44 (17.0) 

Time Finished: 20170321 07:42.55Time Now: 20170321 19:37.05
24 tickers loaded


Starting status check...
Ticker     Name                          Last           STDMov    Hist Perf      Beta           PE Ratio  
^STI       Straits Times Index           3158.57        -0.129    nan/nan        1.0            None      
^KLSE      FTSE Bursa Malaysia           1754.67        0.161     nan/nan        1.0            None      
^GSPC      S&P 500 Index                 2373.47        0.0       nan/nan        1.0            None      
Q01.SI     QAF                           1.385          -0.074     0.6/ 0.39     0.301          6.496     
AGS.SI     TheHourGlass                  0.665          0.0       -0.29/-0.18    0.106          9.779     
RW0U.SI    Mapletree G China             0.975          0.329     -0.11/-0.06    0.465          6.865     
D5IU.SI    Lippo Malls                   0.395          1.054      0.6/ 0.36     0.094          40.013    
ES3.SI     Straits Times ETF             3.18           0.148      0.58/ 0.49    0.872          None      
500.SI     Tai Sin Electric              0.465          0.661      2.39/ 1.56    0.316          8.153     
573.SI     Challenger Techonologies      0.48           0.0       -0.01/-0.02    0.034          13.33     
C6L.SI     SIA                           10.04          0.392      0.09/-0.03    0.338          16.435    
E5H.SI     Golden Agri                   0.385          0.0        0.64/ 0.33    1.179          12.419    
J91U.SI    Cambridge Industrial Trust    0.58           0.29       2.14/ 1.13    0.409          116.0     
O23.SI     OSIM Intl                     1.39           0.0       -0.11/-0.07    0.585          None      
D05.SI     DBS Bank                      19.0           -0.017    -0.75/-0.34    1.025          11.284    
O39.SI     OCBC Bank                     9.62           -0.047    -0.43/-0.25    1.264          11.658    
U11.SI     UOB Bank                      21.8           -0.136     0.41/ 0.19    1.182          11.788    
1163.KL    Allianz Malaysia              11.9           0.033      0.92/ 0.11    0.563          13.182    
C          Citigroup                     59.59          0.0       -1.46/-0.65    1.962          12.63     
BAC        Bank of America               24.44          -0.0      -1.33/-0.59    1.98           16.389    
XOM        ExxonMobil                    82.0           0.0        0.57/ 0.28    0.646          43.69     
CVX        Chevron Corp                  107.66         -0.0       0.05/ 0.03    0.553          None      
Z74.SI     Singapore Telecomms           3.94           -0.092    -0.3/-0.2      0.61           16.415    
3395.KL    Berjaya Corporation Berhad    0.39           -0.241    -0.78/-0.74    0.545          None      


Recommendation
===============
QAF Sell. Current:1.385 (1.15) 
TheHourGlass Buy. Current:0.665 (0.7) 
Lippo Malls Sell. Current:0.395 (0.38) 
Tai Sin Electric Check. Hist Perf:2.39 (5.7%) 
Cambridge Industrial Trust Check. Hist Perf:2.14 (3.6%) 
DBS Bank Sell. Current:19.0 (17.0) 
UOB Bank Sell. Current:21.8 (20.0) 
Citigroup Sell. Current:59.59 (48.0) 
Bank of America Sell. Current:24.44 (17.0) 

Time Finished: 20170321 19:41.55