Time Now: 20170505 07:31.22
24 tickers loaded


Starting status check...
Ticker     Name                          Last           STDMov    Hist Perf      Beta           PE Ratio  
^STI       Straits Times Index           3228.62        -0.249    nan/nan        1.0            None      
^KLSE      FTSE Bursa Malaysia           1758.67        -0.585    nan/nan        1.0            None      
^GSPC      S&P 500 Index                 2389.52        0.059     nan/nan        1.0            None      
Q01.SI     QAF                           1.33           0.0       -0.43/-0.14    0.269          6.21      
AGS.SI     TheHourGlass                  0.69           0.0       -0.17/-0.25    0.297          10.15     
RW0U.SI    Mapletree G China             1.05           0.0       -1.61/-0.69    0.579          7.84      
D5IU.SI    Lippo Malls                   0.42           0.0       -0.87/-0.39    0.367          46.67     
ES3.SI     Straits Times ETF             3.26           0.0        0.43/ 0.28    0.883          None      
500.SI     Tai Sin Electric              0.445          0.0       -0.29/-0.32    0.423          7.807     
573.SI     Challenger Techonologies      0.47           0.0       -0.52/-0.34    -0.349         13.06     
C6L.SI     SIA                           10.35          0.0        0.25/-0.09    0.35           16.94     
E5H.SI     Golden Agri                   0.365          0.0       -0.22/ 0.07    1.273          11.61     
J91U.SI    Cambridge Industrial Trust    0.565          0.0       -2.57/-1.47    0.445          113.0     
O23.SI     OSIM Intl                     1.39           0.0       -0.4/-0.24     0.585          None      
D05.SI     DBS Bank                      20.45          0.0        1.91/ 0.73    1.089          12.32     
O39.SI     OCBC Bank                     10.16          0.0        1.31/ 0.78    1.112          12.36     
U11.SI     UOB Bank                      23.3           0.0        3.27/ 1.59    1.089          12.59     
1163.KL    Allianz Malaysia              11.62          0.0        0.93/ 0.58    0.654          13.07     
C          Citigroup                     60.21          -0.024     0.64/ 0.28    1.856          12.134    
BAC        Bank of America               23.85          0.086      0.59/ 0.21    2.011          14.499    
XOM        ExxonMobil                    81.64          -1.443    -0.09/-0.03    0.676          43.495    
CVX        Chevron Corp                  104.81         -0.73     -1.08/-0.51    0.561          None      
Z74.SI     Singapore Telecomms           3.73           0.0       -0.17/-0.12    0.547          15.54     
3395.KL    Berjaya Corporation Berhad    0.35           0.0        0.07/ 0.09    0.366          None      


Recommendation
===============
QAF Sell. Current:1.33 (1.15) 
TheHourGlass Buy. Current:0.69 (0.7) 
Mapletree G China Sell. Current:1.05 (1.0) 
Mapletree G China Check. Hist Perf:-1.61 (-1.9%) 
Lippo Malls Sell. Current:0.42 (0.38) 
Cambridge Industrial Trust Check. Hist Perf:-2.57 (-3.4%) 
DBS Bank Sell. Current:20.45 (17.0) 
DBS Bank Check. Hist Perf:1.91 (5.7%) 
OCBC Bank Sell. Current:10.16 (10.0) 
UOB Bank Sell. Current:23.3 (20.0) 
UOB Bank Check. Hist Perf:3.27 (6.9%) 
Citigroup Sell. Current:60.21 (48.0) 
Bank of America Sell. Current:23.85 (17.0) 

Time Finished: 20170505 07:36.41Time Now: 20170505 19:31.22
24 tickers loaded


Starting status check...
Ticker     Name                          Last           STDMov    Hist Perf      Beta           PE Ratio  
^STI       Straits Times Index           3229.73        0.03      nan/nan        1.0            None      
^KLSE      FTSE Bursa Malaysia           1762.74        0.175     nan/nan        1.0            None      
^GSPC      S&P 500 Index                 2389.52        -0.0      nan/nan        1.0            None      
Q01.SI     QAF                           1.34           0.141     -0.16/0.0      0.257          6.257     
AGS.SI     TheHourGlass                  0.69           0.0       -0.18/-0.25    0.31           10.15     
RW0U.SI    Mapletree G China             1.04           -0.302    -2.16/-0.91    0.585          7.765     
D5IU.SI    Lippo Malls                   0.425          0.443     -0.29/-0.07    0.346          47.226    
ES3.SI     Straits Times ETF             3.26           0.0        0.39/ 0.25    0.884          None      
500.SI     Tai Sin Electric              0.445          0.0       -0.3/-0.32     0.417          7.807     
573.SI     Challenger Techonologies      0.465          -1.116    -0.87/-0.59    -0.365         12.921    
C6L.SI     SIA                           10.42          0.451      0.68/ 0.16    0.365          17.055    
E5H.SI     Golden Agri                   0.36           -0.213    -0.65/-0.14    1.265          11.451    
J91U.SI    Cambridge Industrial Trust    0.56           -0.544    -3.08/-1.77    0.432          112.0     
O23.SI     OSIM Intl                     1.39           0.0       -0.4/-0.25     0.585          None      
D05.SI     DBS Bank                      20.42          -0.073     1.81/ 0.68    1.091          12.302    
O39.SI     OCBC Bank                     10.22          0.41       1.71/ 1.02    1.117          12.433    
U11.SI     UOB Bank                      23.35          0.107      3.31/ 1.65    1.1            12.617    
1163.KL    Allianz Malaysia              11.7           0.473      1.25/ 0.7     0.649          13.16     
C          Citigroup                     60.21          0.0        0.64/ 0.28    1.856          12.13     
BAC        Bank of America               23.85          0.0        0.59/ 0.21    2.012          14.65     
XOM        ExxonMobil                    81.64          0.0       -0.09/-0.03    0.675          43.49     
CVX        Chevron Corp                  104.81         0.0       -1.08/-0.51    0.561          None      
Z74.SI     Singapore Telecomms           3.75           0.221      0.06/ 0.02    0.546          15.623    
3395.KL    Berjaya Corporation Berhad    0.35           0.0        0.04/ 0.05    0.367          None      


Recommendation
===============
QAF Sell. Current:1.34 (1.15) 
TheHourGlass Buy. Current:0.69 (0.7) 
Mapletree G China Sell. Current:1.04 (1.0) 
Mapletree G China Check. Hist Perf:-2.16 (-2.8%) 
Lippo Malls Sell. Current:0.425 (0.38) 
Cambridge Industrial Trust Check. Hist Perf:-3.08 (-4.3%) 
DBS Bank Sell. Current:20.42 (17.0) 
DBS Bank Check. Hist Perf:1.81 (5.5%) 
OCBC Bank Sell. Current:10.22 (10.0) 
OCBC Bank Check. Hist Perf:1.71 (4.3%) 
UOB Bank Sell. Current:23.35 (20.0) 
UOB Bank Check. Hist Perf:3.31 (7.1%) 
Citigroup Sell. Current:60.21 (48.0) 
Bank of America Sell. Current:23.85 (17.0) 

Time Finished: 20170505 19:36.21