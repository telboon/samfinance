Time Now: 20170217 07:42.49
24 tickers loaded


Starting status check...
Ticker     Name                          Last           STDMov    Hist Perf      Beta           PE Ratio  
^STI       Straits Times Index           3096.69        0.105     nan/nan        1.0            None      
^KLSE      FTSE Bursa Malaysia           1707.59        -0.082    nan/nan        1.0            None      
^GSPC      S&P 500 Index                 2347.22        -0.054    nan/nan        1.0            None      
Q01.SI     QAF                           1.535          0.0       -0.52/-0.36    0.191          10.729    
AGS.SI     TheHourGlass                  0.655          0.0        0.57/ 0.34    0.033          9.632     
RW0U.SI    Mapletree G China             0.975          0.0        0.27/ 0.14    0.471          6.866     
D5IU.SI    Lippo Malls                   0.385          0.0       0.0/0.0        0.007          38.5      
ES3.SI     Straits Times ETF             3.1            0.0       -0.58/-0.51    0.893          None      
500.SI     Tai Sin Electric              0.4            0.0        1.17/ 0.86    0.16           7.453     
573.SI     Challenger Techonologies      0.48           0.0        0.31/ 0.21    0.289          9.8       
C6L.SI     SIA                           9.83           -0.473    -0.05/-0.01    0.223          16.088    
E5H.SI     Golden Agri                   0.44           0.0        1.17/ 0.62    1.146          16.3      
J91U.SI    Cambridge Industrial Trust    0.59           0.0        0.04/ 0.02    0.543          117.0     
O23.SI     OSIM Intl                     1.39           0.0        0.03/ 0.02    0.584          None      
D05.SI     DBS Bank                      18.54          0.0       -1.02/-0.47    1.242          10.82     
O39.SI     OCBC Bank                     9.45           0.0       -2.06/-1.22    1.307          10.94     
U11.SI     UOB Bank                      20.82          0.0       -0.19/-0.09    1.185          11.21     
1163.KL    Allianz Malaysia              11.6           0.0       -0.29/-0.24    0.649          13.0      
C          Citigroup                     60.38          -0.065     0.79/ 0.48    2.045          12.655    
BAC        Bank of America               24.58          0.0        1.24/ 0.49    1.958          16.39     
XOM        ExxonMobil                    82.3           -0.306    -0.62/-0.22    0.64           43.594    
CVX        Chevron Corp                  110.68         -0.674    -1.46/-0.85    0.67           None      
Z74.SI     Singapore Telecomms           3.98           0.899      0.41/ 0.22    0.581          16.583    
3395.KL    Berjaya Corporation Berhad    0.375          0.0       -1.8/-1.17     0.575          None      


Recommendation
===============
QAF Sell. Current:1.535 (1.15) 
TheHourGlass Buy. Current:0.655 (0.7) 
Lippo Malls Sell. Current:0.385 (0.38) 
SIA Buy. Current:9.83 (10.0) 
DBS Bank Sell. Current:18.54 (17.0) 
OCBC Bank Check. Hist Perf:-2.06 (-3.1%) 
UOB Bank Sell. Current:20.82 (20.0) 
Citigroup Sell. Current:60.38 (48.0) 
Bank of America Sell. Current:24.58 (17.0) 
Berjaya Corporation Berhad Check. Hist Perf:-1.8 (-5.1%) 

Time Finished: 20170217 07:48.48Time Now: 20170217 19:37.04
24 tickers loaded


Starting status check...
Ticker     Name                          Last           STDMov    Hist Perf      Beta           PE Ratio  
^STI       Straits Times Index           3107.65        0.246     nan/nan        1.0            None      
^KLSE      FTSE Bursa Malaysia           1707.68        0.003     nan/nan        1.0            None      
^GSPC      S&P 500 Index                 2347.22        0.0       nan/nan        1.0            None      
Q01.SI     QAF                           1.535          0.0       -0.55/-0.36    0.191          10.729    
AGS.SI     TheHourGlass                  0.65           -0.268     0.28/ 0.16    0.033          9.558     
RW0U.SI    Mapletree G China             0.97           -0.357    -0.05/-0.03    0.471          6.831     
D5IU.SI    Lippo Malls                   0.395          1.173      1.19/ 0.74    0.007          39.5      
ES3.SI     Straits Times ETF             3.12           0.282     -0.23/-0.2     0.893          None      
500.SI     Tai Sin Electric              0.405          0.49       1.73/ 1.24    0.16           7.546     
573.SI     Challenger Techonologies      0.48           0.0        0.28/ 0.18    0.289          9.8       
C6L.SI     SIA                           9.84           -0.379    -0.03/-0.06    0.223          16.105    
E5H.SI     Golden Agri                   0.43           -1.57      0.29/ 0.14    1.146          15.93     
J91U.SI    Cambridge Industrial Trust    0.59           0.0       -0.08/-0.05    0.543          117.0     
O23.SI     OSIM Intl                     1.39           0.0       -0.05/-0.04    0.584          None      
D05.SI     DBS Bank                      18.6           0.102     -1.08/-0.5     1.242          10.855    
O39.SI     OCBC Bank                     9.52           0.318     -1.88/-1.12    1.307          11.021    
U11.SI     UOB Bank                      21.18          1.211      0.67/ 0.3     1.185          11.404    
1163.KL    Allianz Malaysia              11.6           0.0       -0.29/-0.24    0.646          13.0      
C          Citigroup                     60.38          -0.0       0.79/ 0.48    2.047          12.82     
BAC        Bank of America               24.58          0.0        1.24/ 0.49    1.961          16.39     
XOM        ExxonMobil                    82.3           -0.0      -0.62/-0.21    0.639          43.78     
CVX        Chevron Corp                  110.68         0.0       -1.46/-0.84    0.663          None      
Z74.SI     Singapore Telecomms           4.0            1.156      0.62/ 0.35    0.581          16.667    
3395.KL    Berjaya Corporation Berhad    0.375          0.0       -1.8/-1.17     0.562          None      


Recommendation
===============
QAF Sell. Current:1.535 (1.15) 
TheHourGlass Buy. Current:0.65 (0.7) 
Lippo Malls Sell. Current:0.395 (0.38) 
Tai Sin Electric Check. Hist Perf:1.73 (3.8%) 
SIA Buy. Current:9.84 (10.0) 
Golden Agri Check. Day STD:-1.57 (-1.0%) 
DBS Bank Sell. Current:18.6 (17.0) 
OCBC Bank Check. Hist Perf:-1.88 (-2.4%) 
UOB Bank Sell. Current:21.18 (20.0) 
Citigroup Sell. Current:60.38 (48.0) 
Bank of America Sell. Current:24.58 (17.0) 
Berjaya Corporation Berhad Check. Hist Perf:-1.8 (-5.1%) 

Time Finished: 20170217 19:41.55