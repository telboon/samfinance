Time Now: 20161101 07:42.39
24 tickers loaded


Starting status check...
Ticker     Name                          Last           STDMov    Hist Perf      Beta           PE Ratio  
^STI       Straits Times Index           2813.87        -0.809    nan/nan        1.0            None      
^KLSE      FTSE Bursa Malaysia           1672.46        0.183     nan/nan        1.0            None      
^GSPC      S&P 500 Index                 2126.15        -0.013    nan/nan        1.0            None      
Q01.SI     QAF                           1.235          0.0       -0.15/-0.09    0.491          9.574     
AGS.SI     TheHourGlass                  0.675          0.0        0.02/ 0.07    0.442          9.72      
RW0U.SI    Mapletree G China             1.04           0.0       -1.03/-0.8     0.737          7.22      
D5IU.SI    Lippo Malls                   0.39           0.0        0.37/ 0.26    0.551          None      
ES3.SI     Straits Times ETF             2.86           0.0        0.5/ 0.39     0.918          None      
500.SI     Tai Sin Electric              0.38           0.0        0.78/ 0.68    0.473          7.17      
573.SI     Challenger Techonologies      0.505          0.0        0.14/ 0.2     0.283          9.26      
C6L.SI     SIA                           10.13          0.0       -0.28/-0.13    0.39           12.39     
E5H.SI     Golden Agri                   0.385          0.0       -0.41/-0.18    1.135          42.778    
J91U.SI    Cambridge Industrial Trust    0.565          0.0        1.06/ 0.74    0.822          None      
O23.SI     OSIM Intl                     1.39           0.0        0.33/ 0.22    1.075          None      
D05.SI     DBS Bank                      15.0           0.0        1.25/ 0.65    1.244          8.77      
O39.SI     OCBC Bank                     8.48           0.0        0.12/ 0.09    1.185          9.83      
U11.SI     UOB Bank                      18.78          0.0        0.81/ 0.44    1.082          10.01     
1163.KL    Allianz Malaysia              10.14          0.0       -0.19/-0.04    0.398          11.41     
C          Citigroup                     49.15          -0.304     0.22/ 0.12    1.873          10.691    
BAC        Bank of America               16.5           -0.328    -0.0/ 0.03     1.641          11.96     
XOM        ExxonMobil                    83.32          -0.931    -1.49/-0.88    0.599          33.09     
CVX        Chevron Corp                  104.75         0.634      2.06/ 1.35    0.981          None      
Z74.SI     Singapore Telecomms           3.88           0.0       -0.26/-0.09    0.467          15.97     
3395.KL    Berjaya Corporation Berhad    0.325          0.0       -0.48/-0.22    0.407          None      


Recommendation
===============
Straits Times Index Check. Day Delta:-1.8% (1.5%) 
QAF Sell. Current:1.235 (1.15) 
TheHourGlass Buy. Current:0.675 (0.7) 
Mapletree G China Sell. Current:1.04 (1.0) 
Lippo Malls Sell. Current:0.39 (0.38) 
Citigroup Sell. Current:49.15 (48.0) 
Chevron Corp Check. Hist Perf:2.06 (3.9%) 

Time Finished: 20161101 07:53.18Time Now: 20161101 19:30.44
24 tickers loaded


Starting status check...
Ticker     Name                          Last           STDMov    Hist Perf      Beta           PE Ratio  
^STI       Straits Times Index           2813.69        -0.811    nan/nan        1.0            None      
^KLSE      FTSE Bursa Malaysia           1670.93        -0.128    nan/nan        1.0            None      
^GSPC      S&P 500 Index                 2126.15        0.0       nan/nan        1.0            None      
Q01.SI     QAF                           1.245          0.273      0.21/ 0.13    0.491          9.652     
AGS.SI     TheHourGlass                  0.675          0.0        0.02/ 0.07    0.442          9.72      
RW0U.SI    Mapletree G China             1.04           0.0       -1.03/-0.8     0.737          7.22      
D5IU.SI    Lippo Malls                   0.395          0.626      0.85/ 0.6     0.551          None      
ES3.SI     Straits Times ETF             2.86           0.0        0.51/ 0.4     0.918          None      
500.SI     Tai Sin Electric              0.365          -2.257    -0.66/-0.45    0.473          6.887     
573.SI     Challenger Techonologies      0.5            -0.63     -0.13/ 0.02    0.283          9.168     
C6L.SI     SIA                           10.16          0.139     -0.16/-0.06    0.39           12.427    
E5H.SI     Golden Agri                   0.38           -0.457    -0.7/-0.33     1.135          42.222    
J91U.SI    Cambridge Industrial Trust    0.55           -1.557    -0.15/-0.07    0.822          None      
O23.SI     OSIM Intl                     1.39           0.0        0.33/ 0.22    1.075          None      
D05.SI     DBS Bank                      14.99          -0.051     1.21/ 0.63    1.244          8.764     
O39.SI     OCBC Bank                     8.5            0.183      0.33/ 0.2     1.185          9.853     
U11.SI     UOB Bank                      18.72          -0.132     0.61/ 0.32    1.082          9.978     
1163.KL    Allianz Malaysia              10.08          -0.572    -0.58/-0.23    0.4            11.342    
C          Citigroup                     49.15          -0.0       0.22/ 0.11    1.861          10.69     
BAC        Bank of America               16.5           0.0       -0.01/ 0.03    1.625          11.96     
XOM        ExxonMobil                    83.32          0.0       -1.49/-0.9     0.601          33.09     
CVX        Chevron Corp                  104.75         0.0        2.04/ 1.37    0.965          None      
Z74.SI     Singapore Telecomms           3.91           0.224      0.05/ 0.12    0.471          16.093    
3395.KL    Berjaya Corporation Berhad    0.325          0.0       -0.46/-0.2     0.417          None      


Recommendation
===============
Straits Times Index Check. Day Delta:-1.8% (1.5%) 
QAF Sell. Current:1.245 (1.15) 
TheHourGlass Buy. Current:0.675 (0.7) 
Mapletree G China Sell. Current:1.04 (1.0) 
Lippo Malls Sell. Current:0.395 (0.38) 
Tai Sin Electric Check. Day STD:-2.26 (-1.5%) 
Cambridge Industrial Trust Check. Day STD:-1.56 (-1.5%) 
Citigroup Sell. Current:49.15 (48.0) 
Chevron Corp Check. Hist Perf:2.04 (3.9%) 

Time Finished: 20161101 19:36.35