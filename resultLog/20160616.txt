Time Now: 20160616 07:30.07
12 tickers loaded


Starting status check...
Ticker     Name                          Last           STDMov    Hist Perf      Beta           PE Ratio  
^STI       Straits Times Index           2774.25        0.097     nan/nan        1.0            None      
Q01.SI     QAF                           1.09           0.0       -0.09/-0.04    0.468          11.01     
AGS.SI     TheHourGlass                  0.805          0.0        0.49/ 0.31    0.461          10.878    
RW0U.SI    Mapletree G China             0.99           0.0        0.58/ 0.59    0.745          6.35      
D5IU.SI    Lippo Malls                   0.34           0.0        0.48/ 0.35    0.521          None      
ES3.SI     Straits Times ETF             2.82           0.0       -0.32/-0.11    0.926          None      
500.SI     Tai Sin Electric              0.335          0.0       -0.11/0.0      0.473          8.171     
573.SI     Challenger Techonologies      0.465          0.0        1.16/ 0.89    0.347          8.774     
C6L.SI     SIA                           10.52          0.0        0.09/ 0.09    0.399          15.31     
E5H.SI     Golden Agri                   0.365          0.0       -0.28/-0.09    1.131          60.833    
J91U.SI    Cambridge Industrial Trust    0.55           0.0        0.91/ 0.64    0.821          None      
O23.SI     OSIM Intl                     1.39           0.0        0.47/ 0.3     1.129          22.79     


Recommendation
===============

Time Finished: 20160616 07:32.00Time Now: 20160616 19:42.27
12 tickers loaded


Starting status check...
Ticker     Name                          Last           STDMov    Hist Perf      Beta           PE Ratio  
^STI       Straits Times Index           2750.72        -0.389    nan/nan        1.0            None      
Q01.SI     QAF                           1.08           -0.393    -0.33/-0.17    0.464          10.909    
AGS.SI     TheHourGlass                  0.805          0.0        0.65/ 0.41    0.458          10.878    
RW0U.SI    Mapletree G China             0.985          -0.284     0.62/ 0.67    0.743          6.318     
D5IU.SI    Lippo Malls                   0.335          -0.749     0.09/ 0.07    0.52           None      
ES3.SI     Straits Times ETF             2.8            -0.424    -0.17/ 0.01    0.926          None      
500.SI     Tai Sin Electric              0.335          0.0        0.03/ 0.15    0.472          8.171     
573.SI     Challenger Techonologies      0.45           -0.897     0.31/ 0.36    0.339          8.491     
C6L.SI     SIA                           10.51          -0.025     0.18/ 0.15    0.398          15.295    
E5H.SI     Golden Agri                   0.355          -0.439    -0.67/-0.26    1.137          59.166    
J91U.SI    Cambridge Industrial Trust    0.545          -0.551     0.8/ 0.58     0.817          None      
O23.SI     OSIM Intl                     1.39           0.0        0.63/ 0.4     1.129          22.79     


Recommendation
===============

Time Finished: 20160616 19:44.28