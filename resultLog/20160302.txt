Time Now: 20160302 07:30.31
12 tickers loaded


Starting status check...
Ticker     Name                          Last           STDMov    Hist Perf      Beta           PE Ratio  
^STI       Straits Times Index           2682.39        0.125     nan/nan        1.0            None      
Q01.SI     QAF                           1.02           0.0       -0.5/-0.4      0.559          11.21     
AGS.SI     TheHourGlass                  0.73           0.0       -0.78/-0.56    0.716          8.9       
RW0U.SI    Mapletree G China             0.965          0.0        2.25/ 1.93    0.862          7.044     
D5IU.SI    Lippo Malls                   0.32           0.0       -0.15/-0.0     0.817          None      
ES3.SI     Straits Times ETF             2.69           0.0        0.17/-0.03    0.923          None      
500.SI     Tai Sin Electric              0.32           0.0       -0.61/-0.5     0.649          8.89      
573.SI     Challenger Techonologies      0.44           0.0        1.36/ 1.0     0.153          8.3       
C6L.SI     SIA                           11.5           0.0       -0.31/-0.22    0.616          21.74     
E5H.SI     Golden Agri                   0.37           0.0       -0.98/-0.5     1.169          92.5      
J91U.SI    Cambridge Industrial Trust    0.535          0.0        0.83/ 0.66    0.89           None      
O23.SI     OSIM Intl                     1.225          0.0        1.81/ 1.04    1.031          18.015    


Recommendation
===============
Mapletree G China Check. Hist Perf:2.25 (6.6%) 
OSIM Intl Check. Hist Perf:1.81 (13.4%) 

Time Finished: 20160302 07:32.39Time Now: 20160302 19:32.03
12 tickers loaded


Starting status check...
Ticker     Name                          Last           STDMov    Hist Perf      Beta           PE Ratio  
^STI       Straits Times Index           2682.39        0.125     nan/nan        1.0            None      
Q01.SI     QAF                           1.02           0.0       -0.5/-0.4      0.559          11.21     
AGS.SI     TheHourGlass                  0.73           0.0       -0.78/-0.56    0.716          8.9       
RW0U.SI    Mapletree G China             0.965          0.0        2.25/ 1.93    0.862          7.044     
D5IU.SI    Lippo Malls                   0.32           0.0       -0.15/-0.0     0.817          None      
ES3.SI     Straits Times ETF             2.69           0.0        0.17/-0.03    0.923          None      
500.SI     Tai Sin Electric              0.32           0.0       -0.61/-0.5     0.649          8.89      
573.SI     Challenger Techonologies      0.44           0.0        1.36/ 1.0     0.153          8.3       
C6L.SI     SIA                           11.5           0.0       -0.31/-0.22    0.616          21.74     
E5H.SI     Golden Agri                   0.37           0.0       -0.98/-0.5     1.169          92.5      
J91U.SI    Cambridge Industrial Trust    0.535          0.0        0.83/ 0.66    0.89           None      
O23.SI     OSIM Intl                     1.225          0.0        1.81/ 1.04    1.031          18.015    


Recommendation
===============
Mapletree G China Check. Hist Perf:2.25 (6.6%) 
OSIM Intl Check. Hist Perf:1.81 (13.4%) 

Time Finished: 20160302 19:34.24