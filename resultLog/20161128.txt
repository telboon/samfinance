Time Now: 20161128 07:30.16
24 tickers loaded


Starting status check...
Ticker     Name                          Last           STDMov    Hist Perf      Beta           PE Ratio  
^STI       Straits Times Index           2859.33        -0.0      nan/nan        1.0            None      
^KLSE      FTSE Bursa Malaysia           1627.26        -0.0      nan/nan        1.0            None      
^GSPC      S&P 500 Index                 2213.35        -0.0      nan/nan        1.0            None      
Q01.SI     QAF                           1.315          0.0       -0.71/-0.42    0.376          9.132     
AGS.SI     TheHourGlass                  0.67           0.0       -0.66/-0.41    0.206          9.71      
RW0U.SI    Mapletree G China             0.97           0.0        0.45/ 0.15    0.7            6.78      
D5IU.SI    Lippo Malls                   0.37           0.0       -0.16/-0.17    0.258          None      
ES3.SI     Straits Times ETF             2.91           0.0       -0.33/-0.37    0.896          None      
500.SI     Tai Sin Electric              0.37           0.0        0.44/ 0.21    0.261          6.73      
573.SI     Challenger Techonologies      0.47           0.0       -0.2/-0.31     0.437          9.59      
C6L.SI     SIA                           9.8            0.0        0.8/ 0.42     0.223          14.08     
E5H.SI     Golden Agri                   0.435          0.0        1.91/ 1.15    1.066          16.111    
J91U.SI    Cambridge Industrial Trust    0.535          0.0       -0.1/-0.18     0.739          None      
O23.SI     OSIM Intl                     1.39           0.0       -0.44/-0.47    1.296          None      
D05.SI     DBS Bank                      17.05          0.0        0.76/ 0.32    1.255          9.97      
O39.SI     OCBC Bank                     8.89           0.0       -0.65/-0.47    1.291          10.3      
U11.SI     UOB Bank                      19.88          0.0        0.08/ 0.11    1.05           10.71     
1163.KL    Allianz Malaysia              9.8            0.0       -0.71/-0.38    0.431          10.99     
C          Citigroup                     56.78          0.0        0.31/ 0.14    2.01           12.35     
BAC        Bank of America               20.86          -0.0       0.38/ 0.15    1.884          15.26     
XOM        ExxonMobil                    87.12          -0.0       0.16/ 0.14    0.539          40.79     
CVX        Chevron Corp                  111.0          0.0        0.06/ 0.01    0.88           None      
Z74.SI     Singapore Telecomms           3.75           0.0        0.94/ 0.55    0.382          15.69     
3395.KL    Berjaya Corporation Berhad    0.32           0.0       -0.55/-0.3     0.513          None      


Recommendation
===============
QAF Sell. Current:1.315 (1.15) 
TheHourGlass Buy. Current:0.67 (0.7) 
SIA Buy. Current:9.8 (10.0) 
Golden Agri Check. Hist Perf:1.91 (8.7%) 
DBS Bank Sell. Current:17.05 (17.0) 
Citigroup Sell. Current:56.78 (48.0) 
Bank of America Sell. Current:20.86 (17.0) 

Time Finished: 20161128 07:35.12Time Now: 20161128 19:37.06
24 tickers loaded


Starting status check...
Ticker     Name                          Last           STDMov    Hist Perf      Beta           PE Ratio  
^STI       Straits Times Index           2874.65        0.418     nan/nan        1.0            None      
^KLSE      FTSE Bursa Malaysia           1628.66        0.077     nan/nan        1.0            None      
^GSPC      S&P 500 Index                 2213.35        -0.0      nan/nan        1.0            None      
Q01.SI     QAF                           1.33           0.313     -0.32/-0.17    0.376          9.236     
AGS.SI     TheHourGlass                  0.67           0.0       -0.7/-0.43     0.206          9.71      
RW0U.SI    Mapletree G China             0.96           -0.178    -0.17/-0.26    0.7            6.71      
D5IU.SI    Lippo Malls                   0.37           0.0       -0.22/-0.23    0.258          None      
ES3.SI     Straits Times ETF             2.93           0.739     -0.11/-0.23    0.896          None      
500.SI     Tai Sin Electric              0.365          -0.726    -0.24/-0.3     0.261          6.639     
573.SI     Challenger Techonologies      0.47           0.0       -0.27/-0.43    0.437          9.59      
C6L.SI     SIA                           9.8            0.0        0.73/ 0.37    0.223          14.08     
E5H.SI     Golden Agri                   0.445          0.528      2.43/ 1.47    1.066          16.481    
J91U.SI    Cambridge Industrial Trust    0.53           -0.48     -0.84/-0.72    0.739          None      
O23.SI     OSIM Intl                     1.39           0.0       -0.6/-0.63     1.296          None      
D05.SI     DBS Bank                      17.35          0.558      1.31/ 0.58    1.255          10.145    
O39.SI     OCBC Bank                     9.03           1.107     -0.03/-0.13    1.291          10.462    
U11.SI     UOB Bank                      20.04          0.424      0.22/ 0.21    1.05           10.796    
1163.KL    Allianz Malaysia              9.79           -0.08     -0.81/-0.45    0.431          10.979    
C          Citigroup                     56.78          0.0        0.31/ 0.14    2.01           12.35     
BAC        Bank of America               20.86          -0.0       0.38/ 0.15    1.884          15.26     
XOM        ExxonMobil                    87.12          -0.0       0.16/ 0.14    0.539          40.79     
CVX        Chevron Corp                  111.0          0.0        0.06/ 0.01    0.88           None      
Z74.SI     Singapore Telecomms           3.76           0.089      1.05/ 0.62    0.382          15.732    
3395.KL    Berjaya Corporation Berhad    0.325          0.777     -0.02/-0.02    0.513          None      


Recommendation
===============
QAF Sell. Current:1.33 (1.15) 
TheHourGlass Buy. Current:0.67 (0.7) 
SIA Buy. Current:9.8 (10.0) 
Golden Agri Check. Hist Perf:2.43 (11.3%) 
DBS Bank Sell. Current:17.35 (17.0) 
UOB Bank Sell. Current:20.04 (20.0) 
Citigroup Sell. Current:56.78 (48.0) 
Bank of America Sell. Current:20.86 (17.0) 

Time Finished: 20161128 19:42.21