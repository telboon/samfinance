Time Now: 20170601 07:35.17
26 tickers loaded


Starting status check...
Ticker     Name                          Last           STDMov    Hist Perf      Beta           PE Ratio  
^STI       Straits Times Index           3210.82        -1.035    nan/nan        1.0            None      
^KLSE      FTSE Bursa Malaysia           1765.87        -0.439    nan/nan        1.0            None      
^GSPC      S&P 500 Index                 2412.91        1.221     nan/nan        1.0            None      
Q01.SI     QAF                           1.355          -0.093    0.0/-0.08      0.277          6.337     
AGS.SI     TheHourGlass                  0.695          0.732      0.65/ 0.52    0.311          10.221    
AXL        American Axle                 15.24          -4.03     -7.88/-4.07    1.044          6.397     
CALM       Cal-Maine Foods               38.0           -2.728    -4.32/-1.99    1.073          5.267     
D05.SI     DBS Bank                      20.47          -0.496     0.05/ 0.11    1.133          12.26     
O39.SI     OCBC Bank                     10.49          -0.439     0.25/ 0.12    1.159          12.762    
U11.SI     UOB Bank                      22.98          -1.007    -0.77/-0.46    1.16           12.354    
RW0U.SI    Mapletree G China             1.065          0.758      1.76/ 0.76    0.554          7.947     
D5IU.SI    Lippo Malls                   0.415          0.0        0.22/ 0.06    0.321          46.111    
ES3.SI     Straits Times ETF             3.26           -0.579     0.33/ 0.36    0.891          None      
500.SI     Tai Sin Electric              0.44           -0.305    -0.24/-0.02    0.394          7.633     
573.SI     Challenger Techonologies      0.46           -1.739    -0.93/-0.64    -0.413         12.782    
C6L.SI     SIA                           10.01          -2.764    -3.95/-2.17    0.425          16.618    
E5H.SI     Golden Agri                   0.365          -0.664    -0.28/-0.3     1.262          11.774    
J91U.SI    Cambridge Industrial Trust    0.575          0.542      0.82/ 0.53    0.377          115.0     
O23.SI     OSIM Intl                     1.39           0.0        0.32/ 0.2     0.585          None      
1163.KL    Allianz Malaysia              12.5           2.705      2.75/ 1.54    0.62           14.013    
C          Citigroup                     61.64          0.549     -0.36/-0.13    1.872          12.425    
BAC        Bank of America               22.835         -1.318    -2.3/-0.99     2.033          14.053    
XOM        ExxonMobil                    81.1           -2.068    -1.38/-0.57    0.689          33.835    
CVX        Chevron Corp                  104.06         -0.772    -1.31/-0.62    0.551          67.527    
Z74.SI     Singapore Telecomms           3.76           0.202     0.0/-0.07      0.574          15.663    
3395.KL    Berjaya Corporation Berhad    0.34           -1.644    -2.21/-1.26    0.374          None      


Recommendation
===============
American Axle Check. Day Delta:-23.3% (3.0%) 
American Axle Check. Day STD:-4.03 (-463.0%) 
American Axle Check. Hist Perf:-7.88 (-23.3%) 
Cal-Maine Foods Check. Day Delta:-23.2% (3.0%) 
Cal-Maine Foods Check. Day STD:-2.73 (-1149.0%) 
Cal-Maine Foods Check. Hist Perf:-4.32 (-23.2%) 
Mapletree G China Check. Hist Perf:1.76 (2.4%) 
Challenger Techonologies Check. Day STD:-1.74 (-1.0%) 
SIA Check. Day Delta:-6.0% (5.0%) 
SIA Check. Day STD:-2.76 (-64.0%) 
SIA Check. Hist Perf:-3.95 (-6.0%) 
Allianz Malaysia Check. Day STD:2.71 (52.0%) 
Allianz Malaysia Check. Hist Perf:2.75 (4.3%) 
Bank of America Check. Hist Perf:-2.3 (-4.9%) 
ExxonMobil Check. Day STD:-2.07 (-145.0%) 
Berjaya Corporation Berhad Check. Day Delta:-6.8% (3.0%) 
Berjaya Corporation Berhad Check. Day STD:-1.64 (-2.5%) 
Berjaya Corporation Berhad Check. Hist Perf:-2.21 (-6.8%) 

Time Finished: 20170601 07:47.13Time Now: 20170601 19:44.39
26 tickers loaded


Starting status check...
Ticker     Name                          Last           STDMov    Hist Perf      Beta           PE Ratio  
^STI       Straits Times Index           3235.96        -0.45     nan/nan        1.0            None      
^KLSE      FTSE Bursa Malaysia           1763.11        -0.56     nan/nan        1.0            None      
^GSPC      S&P 500 Index                 2411.8         1.16      nan/nan        1.0            None      
Q01.SI     QAF                           1.34           -0.37     -0.46/-0.28    0.277          6.266     
AGS.SI     TheHourGlass                  0.695          0.732      0.57/ 0.41    0.311          10.221    
AXL        American Axle                 15.11          -4.144    -8.07/-4.17    1.044          6.342     
CALM       Cal-Maine Foods               37.2           -2.918    -4.6/-2.12     1.073          5.156     
D05.SI     DBS Bank                      20.65          -0.198     0.05/ 0.06    1.133          12.368    
O39.SI     OCBC Bank                     10.56          -0.203     0.09/ 0.04    1.159          12.847    
U11.SI     UOB Bank                      23.22          -0.651    -0.7/-0.38     1.16           12.483    
RW0U.SI    Mapletree G China             1.09           1.515      2.86/ 1.2     0.554          8.133     
D5IU.SI    Lippo Malls                   0.415          0.0        0.09/ 0.03    0.321          46.111    
ES3.SI     Straits Times ETF             3.28           -0.193     0.24/ 0.24    0.891          None      
500.SI     Tai Sin Electric              0.435          -0.61     -0.83/-0.42    0.394          7.547     
573.SI     Challenger Techonologies      0.46           -1.739    -0.82/-0.57    -0.413         12.782    
C6L.SI     SIA                           10.01          -2.764    -4.19/-2.45    0.425          16.618    
E5H.SI     Golden Agri                   0.36           -0.996    -0.97/-0.56    1.262          11.613    
J91U.SI    Cambridge Industrial Trust    0.575          0.542      0.65/ 0.39    0.377          115.0     
O23.SI     OSIM Intl                     1.39           0.0        0.14/ 0.09    0.585          None      
1163.KL    Allianz Malaysia              12.5           2.705      2.81/ 1.6     0.62           14.013    
C          Citigroup                     60.54          -0.511    -1.15/-0.46    1.872          12.203    
BAC        Bank of America               22.41          -1.799    -2.88/-1.21    2.033          13.791    
XOM        ExxonMobil                    80.5           -2.923    -1.78/-0.76    0.689          33.585    
CVX        Chevron Corp                  103.48         -1.007    -1.6/-0.76     0.551          67.151    
Z74.SI     Singapore Telecomms           3.8            0.606      0.49/ 0.21    0.574          15.83     
3395.KL    Berjaya Corporation Berhad    0.34           -1.644    -2.19/-1.24    0.374          None      


Recommendation
===============
American Axle Check. Day Delta:-24.0% (3.0%) 
American Axle Check. Day STD:-4.14 (-476.0%) 
American Axle Check. Hist Perf:-8.07 (-24.0%) 
Cal-Maine Foods Check. Day Delta:-24.8% (3.0%) 
Cal-Maine Foods Check. Day STD:-2.92 (-1229.0%) 
Cal-Maine Foods Check. Hist Perf:-4.6 (-24.8%) 
Mapletree G China Check. Day Delta:4.8% (3.0%) 
Mapletree G China Check. Day STD:1.52 (5.0%) 
Mapletree G China Check. Hist Perf:2.86 (4.8%) 
Challenger Techonologies Check. Day STD:-1.74 (-1.0%) 
SIA Check. Day Delta:-6.0% (5.0%) 
SIA Check. Day STD:-2.76 (-64.0%) 
SIA Check. Hist Perf:-4.19 (-6.0%) 
Allianz Malaysia Check. Day STD:2.71 (52.0%) 
Allianz Malaysia Check. Hist Perf:2.81 (4.3%) 
Bank of America Check. Day Delta:-6.6% (5.0%) 
Bank of America Check. Day STD:-1.8 (-159.0%) 
Bank of America Check. Hist Perf:-2.88 (-6.6%) 
ExxonMobil Check. Day STD:-2.92 (-205.0%) 
ExxonMobil Check. Hist Perf:-1.78 (-2.5%) 
Chevron Corp Check. Hist Perf:-1.6 (-2.3%) 
Berjaya Corporation Berhad Check. Day Delta:-6.8% (3.0%) 
Berjaya Corporation Berhad Check. Day STD:-1.64 (-2.5%) 
Berjaya Corporation Berhad Check. Hist Perf:-2.19 (-6.8%) 

Time Finished: 20170601 19:57.09