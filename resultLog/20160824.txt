Time Now: 20160824 07:34.48
24 tickers loaded


Starting status check...
Ticker     Name                          Last           STDMov    Hist Perf      Beta           PE Ratio  
^STI       Straits Times Index           2850.43        -0.227    nan/nan        1.0            None      
^KLSE      FTSE Bursa Malaysia           1683.62        -0.367    nan/nan        1.0            None      
^GSPC      S&P 500 Index                 2182.64        0.0       nan/nan        1.0            None      
Q01.SI     QAF                           1.185          0.0       -0.44/-0.28    0.491          9.186     
AGS.SI     TheHourGlass                  0.77           0.0        1.25/ 0.63    0.442          10.85     
RW0U.SI    Mapletree G China             1.1            0.0        0.35/ 0.33    0.737          7.64      
D5IU.SI    Lippo Malls                   0.375          0.0       -0.39/-0.28    0.551          None      
ES3.SI     Straits Times ETF             2.9            0.191      1.61/ 1.03    0.918          None      
500.SI     Tai Sin Electric              0.37           0.0        0.58/ 0.47    0.473          9.02      
573.SI     Challenger Techonologies      0.48           0.0       -0.24/-0.14    0.283          8.89      
C6L.SI     SIA                           10.81          0.0        0.31/ 0.18    0.39           13.1      
E5H.SI     Golden Agri                   0.37           0.0        0.13/ 0.07    1.135          41.11     
J91U.SI    Cambridge Industrial Trust    0.54           -0.523     0.18/ 0.13    0.822          None      
O23.SI     OSIM Intl                     1.39           0.0        0.09/ 0.06    1.075          22.79     
D05.SI     DBS Bank                      15.0           0.0        1.6/ 0.79     1.244          8.77      
O39.SI     OCBC Bank                     8.47           -0.05      0.83/ 0.43    1.185          9.83      
U11.SI     UOB Bank                      17.69          0.0        0.84/ 0.49    1.082          9.16      
1163.KL    Allianz Malaysia              10.32          0.359      0.91/ 0.54    0.365          11.57     
C          Citigroup                     46.66          0.0        0.01/ 0.01    1.76           9.88      
BAC        Bank of America               15.18          0.0        0.05/ 0.03    1.532          12.72     
XOM        ExxonMobil                    87.99          0.0       -0.06/-0.03    0.716          34.94     
CVX        Chevron Corp                  101.94         -0.0      -0.11/-0.06    1.165          None      
Z74.SI     Singapore Telecomms           4.21           0.733      1.28/ 0.85    0.545          17.325    
3395.KL    Berjaya Corporation Berhad    0.34           0.0       -0.29/-0.06    0.684          None      


Recommendation
===============
QAF Sell. Current:1.185 (1.15) 
Mapletree G China Sell. Current:1.1 (1.0) 
Straits Times ETF Check. Hist Perf:1.61 (0.3%) 
DBS Bank Check. Hist Perf:1.6 (1.7%) 
Singapore Telecomms Check. Day Delta:3.2% (3.0%) 

Time Finished: 20160824 07:38.52Time Now: 20160824 19:36.16
24 tickers loaded


Starting status check...
Ticker     Name                          Last           STDMov    Hist Perf      Beta           PE Ratio  
^STI       Straits Times Index           2869.1         0.071     nan/nan        1.0            None      
^KLSE      FTSE Bursa Malaysia           1683.22        0.007     nan/nan        1.0            None      
^GSPC      S&P 500 Index                 2186.9         0.092     nan/nan        1.0            None      
Q01.SI     QAF                           1.185          0.0       -0.59/-0.37    0.491          9.186     
AGS.SI     TheHourGlass                  0.77           0.0        1.12/ 0.54    0.442          10.85     
RW0U.SI    Mapletree G China             1.1            0.0        0.15/ 0.12    0.737          7.64      
D5IU.SI    Lippo Malls                   0.38           0.307     -0.03/-0.02    0.551          None      
ES3.SI     Straits Times ETF             2.9            0.191      0.41/ 0.25    0.918          None      
500.SI     Tai Sin Electric              0.37           0.0        0.47/ 0.36    0.473          9.02      
573.SI     Challenger Techonologies      0.48           0.0       -0.29/-0.21    0.283          8.89      
C6L.SI     SIA                           10.77          -0.185     0.05/ 0.03    0.39           13.052    
E5H.SI     Golden Agri                   0.37           0.0       -0.04/-0.02    1.135          41.11     
J91U.SI    Cambridge Industrial Trust    0.545          0.0        0.36/ 0.24    0.822          None      
O23.SI     OSIM Intl                     1.39           0.0       -0.03/-0.02    1.075          22.79     
D05.SI     DBS Bank                      15.21          0.466      2.03/ 0.98    1.244          8.893     
O39.SI     OCBC Bank                     8.61           0.649      1.6/ 0.81     1.185          9.992     
U11.SI     UOB Bank                      17.88          0.446      1.09/ 0.65    1.082          9.258     
1163.KL    Allianz Malaysia              10.28          0.0        0.66/ 0.41    0.355          11.525    
C          Citigroup                     46.59          0.0       -0.17/-0.1     1.76           9.865     
BAC        Bank of America               15.35          0.0        0.32/ 0.14    1.532          12.862    
XOM        ExxonMobil                    87.72          -0.0      -0.25/-0.12    0.716          34.833    
CVX        Chevron Corp                  101.68         0.0       -0.28/-0.17    1.165          None      
Z74.SI     Singapore Telecomms           4.24           0.902      1.53/ 1.0     0.545          17.448    
3395.KL    Berjaya Corporation Berhad    0.34           0.0       -0.29/-0.05    0.692          None      


Recommendation
===============
QAF Sell. Current:1.185 (1.15) 
Mapletree G China Sell. Current:1.1 (1.0) 
Lippo Malls Sell. Current:0.38 (0.38) 
DBS Bank Check. Hist Perf:2.03 (3.1%) 
OCBC Bank Check. Hist Perf:1.6 (2.0%) 
Singapore Telecomms Check. Day Delta:3.9% (3.0%) 
Singapore Telecomms Check. Hist Perf:1.53 (3.9%) 

Time Finished: 20160824 19:40.03