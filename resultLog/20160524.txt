Time Now: 20160524 07:36.29
12 tickers loaded


Starting status check...
Ticker     Name                          Last           STDMov    Hist Perf      Beta           PE Ratio  
^STI       Straits Times Index           2766.93        0.039     nan/nan        1.0            None      
Q01.SI     QAF                           1.085          0.0        1.22/ 0.81    0.439          10.96     
AGS.SI     TheHourGlass                  0.75           0.0       -0.48/-0.23    0.413          9.15      
RW0U.SI    Mapletree G China             0.98           0.0        0.16/ 0.16    0.739          6.28      
D5IU.SI    Lippo Malls                   0.34           0.0        0.1/ 0.08     0.537          None      
ES3.SI     Straits Times ETF             2.82           0.136      0.24/ 0.18    0.924          None      
500.SI     Tai Sin Electric              0.32           0.0       -1.02/-0.82    0.463          7.8       
573.SI     Challenger Techonologies      0.45           0.0        0.05/ 0.06    0.353          8.49      
C6L.SI     SIA                           10.54          0.0       -0.24/-0.13    0.442          15.34     
E5H.SI     Golden Agri                   0.375          0.0        0.46/ 0.21    1.107          62.5      
J91U.SI    Cambridge Industrial Trust    0.545          0.0        0.6/ 0.43     0.81           None      
O23.SI     OSIM Intl                     1.39           0.0        0.28/ 0.14    1.127          22.79     


Recommendation
===============

Time Finished: 20160524 07:38.19Time Now: 20160524 19:34.12
12 tickers loaded


Starting status check...
Ticker     Name                          Last           STDMov    Hist Perf      Beta           PE Ratio  
^STI       Straits Times Index           2749.59        -0.231    nan/nan        1.0            None      
Q01.SI     QAF                           1.085          0.0        1.32/ 0.89    0.439          10.96     
AGS.SI     TheHourGlass                  0.75           0.0       -0.37/-0.15    0.409          9.15      
RW0U.SI    Mapletree G China             0.98           0.0        0.36/ 0.35    0.743          6.28      
D5IU.SI    Lippo Malls                   0.34           0.0        0.23/ 0.17    0.545          None      
ES3.SI     Straits Times ETF             2.82           0.0        1.43/ 0.93    0.923          None      
500.SI     Tai Sin Electric              0.32           0.0       -0.91/-0.7     0.469          7.8       
573.SI     Challenger Techonologies      0.45           0.0        0.11/ 0.13    0.354          8.49      
C6L.SI     SIA                           10.5           -0.156    -0.28/-0.14    0.43           15.282    
E5H.SI     Golden Agri                   0.37           -0.213     0.3/ 0.15     1.099          61.667    
J91U.SI    Cambridge Industrial Trust    0.54           -0.488     0.41/ 0.3     0.815          None      
O23.SI     OSIM Intl                     1.39           0.0        0.4/ 0.22     1.126          22.79     


Recommendation
===============

Time Finished: 20160524 19:36.02