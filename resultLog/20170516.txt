Time Now: 20170516 07:37.27
24 tickers loaded


Starting status check...
Ticker     Name                          Last           STDMov    Hist Perf      Beta           PE Ratio  
^STI       Straits Times Index           3264.21        0.208     nan/nan        1.0            None      
^KLSE      FTSE Bursa Malaysia           1778.65        0.122     nan/nan        1.0            None      
^GSPC      S&P 500 Index                 2402.32        0.634     nan/nan        1.0            None      
Q01.SI     QAF                           1.36           0.0       -0.68/-0.33    0.277          6.36      
AGS.SI     TheHourGlass                  0.685          0.0       -0.3/-0.23     0.311          10.074    
RW0U.SI    Mapletree G China             1.04           0.0       -0.67/-0.29    0.554          7.76      
D5IU.SI    Lippo Malls                   0.415          0.0       -0.07/-0.02    0.321          46.111    
ES3.SI     Straits Times ETF             3.29           0.0       -0.42/-0.39    0.891          None      
500.SI     Tai Sin Electric              0.445          0.0       -0.07/-0.08    0.394          7.72      
573.SI     Challenger Techonologies      0.47           0.0        0.44/ 0.3     -0.413         13.06     
C6L.SI     SIA                           10.65          0.0       -0.48/-0.37    0.425          17.68     
E5H.SI     Golden Agri                   0.375          0.0       -0.56/-0.23    1.262          12.097    
J91U.SI    Cambridge Industrial Trust    0.57           0.0       -0.1/-0.08     0.377          114.0     
O23.SI     OSIM Intl                     1.39           0.0       -0.1/-0.06     0.585          None      
D05.SI     DBS Bank                      20.77          0.0        0.4/ 0.15     1.133          12.44     
O39.SI     OCBC Bank                     10.62          0.0        0.71/ 0.42    1.159          12.92     
U11.SI     UOB Bank                      23.66          0.0       -0.02/ 0.02    1.16           12.72     
1163.KL    Allianz Malaysia              11.98          0.0        0.85/ 0.3     0.62           13.43     
C          Citigroup                     61.42          0.337      0.71/ 0.3     1.872          12.381    
BAC        Bank of America               24.06          0.068     -0.04/-0.04    2.033          14.807    
XOM        ExxonMobil                    82.8           0.356      0.25/ 0.14    0.689          34.544    
CVX        Chevron Corp                  106.85         0.361      0.89/ 0.41    0.551          69.338    
Z74.SI     Singapore Telecomms           3.74           0.0        0.18/ 0.09    0.574          15.58     
3395.KL    Berjaya Corporation Berhad    0.365          0.0       -0.53/-0.38    0.374          None      


Recommendation
===============
QAF Sell. Current:1.36 (1.15) 
TheHourGlass Buy. Current:0.685 (0.7) 
Mapletree G China Sell. Current:1.04 (1.0) 
Lippo Malls Sell. Current:0.415 (0.38) 
DBS Bank Sell. Current:20.77 (17.0) 
OCBC Bank Sell. Current:10.62 (10.0) 
UOB Bank Sell. Current:23.66 (20.0) 
Citigroup Sell. Current:61.42 (48.0) 
Bank of America Sell. Current:24.06 (17.0) 

Time Finished: 20170516 07:42.29Time Now: 20170516 19:35.30
24 tickers loaded


Starting status check...
Ticker     Name                          Last           STDMov    Hist Perf      Beta           PE Ratio  
^STI       Straits Times Index           3227.71        -0.642    nan/nan        1.0            None      
^KLSE      FTSE Bursa Malaysia           1778.15        0.1       nan/nan        1.0            None      
^GSPC      S&P 500 Index                 2402.32        0.634     nan/nan        1.0            None      
Q01.SI     QAF                           1.36           0.0       -0.57/-0.34    0.277          6.36      
AGS.SI     TheHourGlass                  0.69           0.366      0.07/ 0.1     0.311          10.148    
RW0U.SI    Mapletree G China             1.045          0.152     -0.05/-0.0     0.554          7.797     
D5IU.SI    Lippo Malls                   0.415          0.0        0.11/ 0.03    0.321          46.111    
ES3.SI     Straits Times ETF             3.26           -0.579    -0.33/-0.25    0.891          None      
500.SI     Tai Sin Electric              0.44           -0.305    -0.35/-0.14    0.394          7.633     
573.SI     Challenger Techonologies      0.47           0.0        0.27/ 0.19    -0.413         13.06     
C6L.SI     SIA                           10.49          -0.691    -1.22/-0.62    0.425          17.414    
E5H.SI     Golden Agri                   0.37           -0.332    -0.53/-0.34    1.262          11.936    
J91U.SI    Cambridge Industrial Trust    0.565          -0.542    -0.37/-0.17    0.377          113.0     
O23.SI     OSIM Intl                     1.39           0.0        0.16/ 0.1     0.585          None      
D05.SI     DBS Bank                      20.65          -0.198     0.73/ 0.37    1.133          12.368    
O39.SI     OCBC Bank                     10.54          -0.27      1.08/ 0.61    1.159          12.823    
U11.SI     UOB Bank                      23.25          -0.607    -0.29/-0.18    1.16           12.5      
1163.KL    Allianz Malaysia              12.12          0.728      1.56/ 0.67    0.62           13.587    
C          Citigroup                     61.42          0.337      0.71/ 0.3     1.872          12.381    
BAC        Bank of America               24.06          0.068     -0.04/-0.04    2.033          14.807    
XOM        ExxonMobil                    82.8           0.356      0.25/ 0.14    0.689          34.544    
CVX        Chevron Corp                  106.85         0.361      0.89/ 0.41    0.551          69.338    
Z74.SI     Singapore Telecomms           3.73           -0.101     0.06/ 0.02    0.574          15.538    
3395.KL    Berjaya Corporation Berhad    0.355          -0.658    -1.43/-0.91    0.374          None      


Recommendation
===============
QAF Sell. Current:1.36 (1.15) 
TheHourGlass Buy. Current:0.69 (0.7) 
Mapletree G China Sell. Current:1.045 (1.0) 
Lippo Malls Sell. Current:0.415 (0.38) 
DBS Bank Sell. Current:20.65 (17.0) 
OCBC Bank Sell. Current:10.54 (10.0) 
UOB Bank Sell. Current:23.25 (20.0) 
Allianz Malaysia Sell. Current:12.12 (12.0) 
Allianz Malaysia Check. Hist Perf:1.56 (3.1%) 
Citigroup Sell. Current:61.42 (48.0) 
Bank of America Sell. Current:24.06 (17.0) 

Time Finished: 20170516 19:40.27