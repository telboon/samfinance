Time Now: 20160328 07:37.03
12 tickers loaded


Starting status check...
Ticker     Name                          Last           STDMov    Hist Perf      Beta           PE Ratio  
^STI       Straits Times Index           2847.39        0.0       nan/nan        1.0            None      
Q01.SI     QAF                           1.01           0.0       -0.89/-0.62    0.482          10.74     
AGS.SI     TheHourGlass                  0.73           0.0        0.27/ 0.15    0.554          8.9       
RW0U.SI    Mapletree G China             0.965          0.0        0.43/ 0.39    0.798          7.044     
D5IU.SI    Lippo Malls                   0.32           0.0        0.93/ 0.62    0.637          None      
ES3.SI     Straits Times ETF             2.85           0.0       -0.65/-0.35    0.929          None      
500.SI     Tai Sin Electric              0.325          0.0        1.43/ 1.19    0.526          9.028     
573.SI     Challenger Techonologies      0.445          0.0        0.44/ 0.35    0.308          8.396     
C6L.SI     SIA                           11.47          0.0       -0.16/-0.05    0.48           21.68     
E5H.SI     Golden Agri                   0.43           0.0        0.59/ 0.29    1.11           None      
J91U.SI    Cambridge Industrial Trust    0.56           0.0        0.48/ 0.32    0.891          None      
O23.SI     OSIM Intl                     1.37           0.0        0.12/ 0.08    1.289          20.15     


Recommendation
===============

Time Finished: 20160328 07:39.03Time Now: 20160328 19:33.33
12 tickers loaded


Starting status check...
Ticker     Name                          Last           STDMov    Hist Perf      Beta           PE Ratio  
^STI       Straits Times Index           2830.5         -0.14     nan/nan        1.0            None      
Q01.SI     QAF                           1.025          0.717     -0.23/-0.15    0.482          10.9      
AGS.SI     TheHourGlass                  0.73           0.0        0.41/ 0.23    0.554          8.9       
RW0U.SI    Mapletree G China             0.965          0.0        0.65/ 0.59    0.798          7.044     
D5IU.SI    Lippo Malls                   0.325          0.578      1.72/ 1.16    0.637          None      
ES3.SI     Straits Times ETF             2.84           -0.085    -0.24/-0.07    0.929          None      
500.SI     Tai Sin Electric              0.32           -0.608     0.95/ 0.82    0.526          8.889     
573.SI     Challenger Techonologies      0.45           0.257      0.83/ 0.65    0.308          8.49      
C6L.SI     SIA                           11.45          -0.085    -0.12/-0.01    0.48           21.642    
E5H.SI     Golden Agri                   0.42           -0.333     0.18/ 0.1     1.11           None      
J91U.SI    Cambridge Industrial Trust    0.56           0.0        0.72/ 0.48    0.891          None      
O23.SI     OSIM Intl                     1.365          -0.028     0.18/ 0.12    1.289          20.076    


Recommendation
===============
Lippo Malls Check. Hist Perf:1.72 (3.2%) 

Time Finished: 20160328 19:35.32