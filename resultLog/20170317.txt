Time Now: 20170317 07:33.55
24 tickers loaded


Starting status check...
Ticker     Name                          Last           STDMov    Hist Perf      Beta           PE Ratio  
^STI       Straits Times Index           3163.52        0.313     nan/nan        1.0            None      
^KLSE      FTSE Bursa Malaysia           1737.14        0.635     nan/nan        1.0            None      
^GSPC      S&P 500 Index                 2381.38        -0.084    nan/nan        1.0            None      
Q01.SI     QAF                           1.38           0.0        0.29/ 0.22    0.3            6.45      
AGS.SI     TheHourGlass                  0.67           0.0       -1.08/-0.65    0.11           9.85      
RW0U.SI    Mapletree G China             0.98           0.0        0.29/ 0.13    0.466          6.9       
D5IU.SI    Lippo Malls                   0.39           0.0       -0.04/-0.02    0.093          39.0      
ES3.SI     Straits Times ETF             3.18           0.449      0.82/ 0.71    0.871          None      
500.SI     Tai Sin Electric              0.43           0.0       -2.05/-1.43    0.308          7.632     
573.SI     Challenger Techonologies      0.47           0.0       -0.01/-0.05    0.035          13.06     
C6L.SI     SIA                           9.97           0.0        0.05/-0.13    0.335          16.32     
E5H.SI     Golden Agri                   0.38           0.0        0.06/ 0.05    1.174          12.26     
J91U.SI    Cambridge Industrial Trust    0.555          0.0       -1.94/-1.08    0.415          110.0     
O23.SI     OSIM Intl                     1.39           0.0       -0.23/-0.14    0.585          None      
D05.SI     DBS Bank                      19.12          0.0        0.01/-0.03    1.029          11.35     
O39.SI     OCBC Bank                     9.63           0.0       -0.34/-0.21    1.266          11.74     
U11.SI     UOB Bank                      21.78          0.0        0.44/ 0.21    1.182          11.77     
1163.KL    Allianz Malaysia              11.4           0.0       -1.4/-0.94     0.558          12.78     
C          Citigroup                     61.15          0.176     -0.51/-0.21    1.976          12.956    
BAC        Bank of America               25.22          0.037     -0.35/-0.17    1.977          17.003    
XOM        ExxonMobil                    82.07          0.021      0.18/ 0.11    0.645          43.187    
CVX        Chevron Corp                  107.86         -0.342    -1.45/-0.66    0.568          None      
Z74.SI     Singapore Telecomms           3.95           0.178      0.12/ 0.05    0.633          16.463    
3395.KL    Berjaya Corporation Berhad    0.39           0.0        0.22/ 0.01    0.571          None      


Recommendation
===============
QAF Sell. Current:1.38 (1.15) 
TheHourGlass Buy. Current:0.67 (0.7) 
Lippo Malls Sell. Current:0.39 (0.38) 
Tai Sin Electric Check. Hist Perf:-2.05 (-4.4%) 
SIA Buy. Current:9.97 (10.0) 
Cambridge Industrial Trust Check. Hist Perf:-1.94 (-2.6%) 
DBS Bank Sell. Current:19.12 (17.0) 
UOB Bank Sell. Current:21.78 (20.0) 
Citigroup Sell. Current:61.15 (48.0) 
Bank of America Sell. Current:25.22 (17.0) 

Time Finished: 20170317 07:38.28Time Now: 20170317 19:40.50
24 tickers loaded


Starting status check...
Ticker     Name                          Last           STDMov    Hist Perf      Beta           PE Ratio  
^STI       Straits Times Index           3169.38        0.07      nan/nan        1.0            None      
^KLSE      FTSE Bursa Malaysia           1745.2         0.255     nan/nan        1.0            None      
^GSPC      S&P 500 Index                 2381.38        0.0       nan/nan        1.0            None      
Q01.SI     QAF                           1.395          0.214      0.66/ 0.45    0.3            6.52      
AGS.SI     TheHourGlass                  0.665          -0.226    -1.34/-0.81    0.106          9.776     
RW0U.SI    Mapletree G China             0.97           -0.604    -0.27/-0.14    0.463          6.83      
D5IU.SI    Lippo Malls                   0.39           0.0       -0.05/-0.02    0.093          39.0      
ES3.SI     Straits Times ETF             3.19           0.149      0.99/ 0.85    0.871          None      
500.SI     Tai Sin Electric              0.45           0.956     -0.15/-0.17    0.312          7.987     
573.SI     Challenger Techonologies      0.475          0.454      0.31/ 0.15    0.031          13.199    
C6L.SI     SIA                           10.01          0.386      0.27/-0.03    0.335          16.385    
E5H.SI     Golden Agri                   0.385          0.247      0.4/ 0.22     1.174          12.421    
J91U.SI    Cambridge Industrial Trust    0.56           0.28      -1.41/-0.8     0.41           110.991   
O23.SI     OSIM Intl                     1.39           0.0       -0.27/-0.17    0.585          None      
D05.SI     DBS Bank                      19.14          0.033     -0.04/-0.06    1.028          11.362    
O39.SI     OCBC Bank                     9.66           0.134     -0.29/-0.18    1.265          11.777    
U11.SI     UOB Bank                      21.95          0.398      0.81/ 0.37    1.184          11.862    
1163.KL    Allianz Malaysia              11.68          0.468     -0.12/-0.38    0.551          13.094    
C          Citigroup                     61.15          -0.0      -0.51/-0.21    1.975          12.96     
BAC        Bank of America               25.22          0.0       -0.35/-0.17    1.975          16.85     
XOM        ExxonMobil                    82.07          0.0        0.18/ 0.11    0.645          43.72     
CVX        Chevron Corp                  107.86         -0.0      -1.45/-0.66    0.567          None      
Z74.SI     Singapore Telecomms           3.99           0.367      0.44/ 0.25    0.61           16.623    
3395.KL    Berjaya Corporation Berhad    0.39           0.0        0.14/-0.1     0.553          None      


Recommendation
===============
QAF Sell. Current:1.395 (1.15) 
TheHourGlass Buy. Current:0.665 (0.7) 
Lippo Malls Sell. Current:0.39 (0.38) 
DBS Bank Sell. Current:19.14 (17.0) 
UOB Bank Sell. Current:21.95 (20.0) 
Citigroup Sell. Current:61.15 (48.0) 
Bank of America Sell. Current:25.22 (17.0) 

Time Finished: 20170317 19:45.18