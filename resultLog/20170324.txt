Time Now: 20170324 07:39.06
24 tickers loaded


Starting status check...
Ticker     Name                          Last           STDMov    Hist Perf      Beta           PE Ratio  
^STI       Straits Times Index           3126.93        0.11      nan/nan        1.0            None      
^KLSE      FTSE Bursa Malaysia           1747.0         -0.039    nan/nan        1.0            None      
^GSPC      S&P 500 Index                 2345.96        -0.053    nan/nan        1.0            None      
Q01.SI     QAF                           1.38           0.0       -0.25/-0.2     0.279          6.45      
AGS.SI     TheHourGlass                  0.66           0.0       -0.22/-0.09    0.109          9.71      
RW0U.SI    Mapletree G China             0.985          0.0        1.09/ 0.53    0.476          6.937     
D5IU.SI    Lippo Malls                   0.39           0.0        0.08/ 0.02    0.128          39.0      
ES3.SI     Straits Times ETF             3.14           0.306     -0.43/-0.38    0.872          None      
500.SI     Tai Sin Electric              0.47           0.0        2.11/ 1.43    0.343          8.158     
573.SI     Challenger Techonologies      0.48           0.0        0.34/ 0.28    0.061          13.33     
C6L.SI     SIA                           10.04          0.0        0.5/ 0.53     0.345          16.43     
E5H.SI     Golden Agri                   0.39           0.0        0.89/ 0.41    1.198          12.58     
J91U.SI    Cambridge Industrial Trust    0.575          0.0        1.99/ 1.15    0.408          115.0     
O23.SI     OSIM Intl                     1.39           0.0        0.32/ 0.19    0.585          None      
D05.SI     DBS Bank                      18.65          0.0       -0.62/-0.2     0.996          11.07     
O39.SI     OCBC Bank                     9.5            0.0        0.01/ 0.02    1.249          11.55     
U11.SI     UOB Bank                      21.61          0.0        0.02/0.0      1.176          11.68     
1163.KL    Allianz Malaysia              11.92          0.0        1.19/ 0.6     0.592          13.36     
C          Citigroup                     58.05          0.157     -0.46/-0.3     1.973          12.36     
BAC        Bank of America               23.07          0.118     -1.42/-0.56    2.017          15.407    
XOM        ExxonMobil                    81.86          0.033      0.39/ 0.04    0.642          43.653    
CVX        Chevron Corp                  107.87         -0.162     0.49/ 0.19    0.547          None      
Z74.SI     Singapore Telecomms           3.89           -0.551    -0.29/-0.11    0.61           16.207    
3395.KL    Berjaya Corporation Berhad    0.385          0.0       -0.46/-0.31    0.5            None      


Recommendation
===============
QAF Sell. Current:1.38 (1.15) 
TheHourGlass Buy. Current:0.66 (0.7) 
Lippo Malls Sell. Current:0.39 (0.38) 
Tai Sin Electric Check. Hist Perf:2.11 (4.4%) 
Cambridge Industrial Trust Check. Hist Perf:1.99 (2.7%) 
DBS Bank Sell. Current:18.65 (17.0) 
UOB Bank Sell. Current:21.61 (20.0) 
Citigroup Sell. Current:58.05 (48.0) 
Bank of America Sell. Current:23.07 (17.0) 

Time Finished: 20170324 07:43.47Time Now: 20170324 19:32.44
24 tickers loaded


Starting status check...
Ticker     Name                          Last           STDMov    Hist Perf      Beta           PE Ratio  
^STI       Straits Times Index           3142.9         0.209     nan/nan        1.0            None      
^KLSE      FTSE Bursa Malaysia           1745.75        -0.038    nan/nan        1.0            None      
^GSPC      S&P 500 Index                 2345.96        0.0       nan/nan        1.0            None      
Q01.SI     QAF                           1.39           0.153     -0.04/-0.06    0.278          6.497     
AGS.SI     TheHourGlass                  0.69           1.347      1.37/ 0.82    0.112          10.151    
RW0U.SI    Mapletree G China             0.98           -0.369     0.71/ 0.34    0.475          6.902     
D5IU.SI    Lippo Malls                   0.395          0.566      0.69/ 0.38    0.13           39.5      
ES3.SI     Straits Times ETF             3.16           0.317     -0.23/-0.2     0.873          None      
500.SI     Tai Sin Electric              0.46           -0.386     1.05/ 0.73    0.323          7.984     
573.SI     Challenger Techonologies      0.48           0.0        0.33/ 0.26    0.057          13.33     
C6L.SI     SIA                           10.05          0.102      0.45/ 0.41    0.342          16.446    
E5H.SI     Golden Agri                   0.385          -0.238     0.3/ 0.13     1.189          12.419    
J91U.SI    Cambridge Industrial Trust    0.575          0.0        1.84/ 1.06    0.4            115.0     
O23.SI     OSIM Intl                     1.39           0.0        0.2/ 0.12     0.585          None      
D05.SI     DBS Bank                      18.79          0.268     -0.5/-0.18     0.999          11.153    
O39.SI     OCBC Bank                     9.57           0.364      0.08/ 0.05    1.247          11.635    
U11.SI     UOB Bank                      21.78          0.394      0.13/ 0.06    1.176          11.772    
1163.KL    Allianz Malaysia              11.9           -0.033     1.1/ 0.57     0.598          13.338    
C          Citigroup                     58.05          0.0       -0.46/-0.3     1.988          12.24     
BAC        Bank of America               23.07          0.0       -1.4/-0.56     2.045          15.41     
XOM        ExxonMobil                    81.86          -0.0       0.38/ 0.04    0.638          43.56     
CVX        Chevron Corp                  107.87         -0.0       0.49/ 0.19    0.543          None      
Z74.SI     Singapore Telecomms           3.91           -0.367    -0.08/ 0.02    0.61           16.29     
3395.KL    Berjaya Corporation Berhad    0.38           -0.266    -0.89/-0.58    0.492          None      


Recommendation
===============
QAF Sell. Current:1.39 (1.15) 
TheHourGlass Buy. Current:0.69 (0.7) 
TheHourGlass Check. Day Delta:4.5% (3.0%) 
Lippo Malls Sell. Current:0.395 (0.38) 
Cambridge Industrial Trust Check. Hist Perf:1.84 (2.7%) 
DBS Bank Sell. Current:18.79 (17.0) 
UOB Bank Sell. Current:21.78 (20.0) 
Citigroup Sell. Current:58.05 (48.0) 
Bank of America Sell. Current:23.07 (17.0) 

Time Finished: 20170324 19:37.11