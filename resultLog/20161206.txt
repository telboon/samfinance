Time Now: 20161206 07:40.15
24 tickers loaded


Starting status check...
Ticker     Name                          Last           STDMov    Hist Perf      Beta           PE Ratio  
^STI       Straits Times Index           2943.05        0.541     nan/nan        1.0            None      
^KLSE      FTSE Bursa Malaysia           1624.97        -0.203    nan/nan        1.0            None      
^GSPC      S&P 500 Index                 2204.71        0.452     nan/nan        1.0            None      
Q01.SI     QAF                           1.32           0.0       -0.34/-0.19    0.365          9.17      
AGS.SI     TheHourGlass                  0.67           0.0       -0.16/-0.1     0.193          9.71      
RW0U.SI    Mapletree G China             0.965          0.0       -0.43/-0.4     0.691          6.78      
D5IU.SI    Lippo Malls                   0.375          0.0        0.33/ 0.17    0.259          None      
ES3.SI     Straits Times ETF             2.98           0.643     -0.3/-0.38     0.897          None      
500.SI     Tai Sin Electric              0.375          0.0        0.35/ 0.1     0.257          6.818     
573.SI     Challenger Techonologies      0.465          0.0       -0.61/-0.7     0.429          9.49      
C6L.SI     SIA                           9.8            0.0       -0.62/-0.41    0.234          14.08     
E5H.SI     Golden Agri                   0.44           0.0       -0.33/-0.03    1.082          16.3      
J91U.SI    Cambridge Industrial Trust    0.54           0.0        0.15/-0.07    0.73           None      
O23.SI     OSIM Intl                     1.39           0.0       -0.65/-0.68    1.296          None      
D05.SI     DBS Bank                      17.97          0.0        0.35/ 0.08    1.279          10.51     
O39.SI     OCBC Bank                     9.2            0.0       -0.61/-0.48    1.294          10.63     
U11.SI     UOB Bank                      20.78          0.0        0.65/ 0.43    1.053          11.19     
1163.KL    Allianz Malaysia              9.61           0.0       -1.25/-0.65    0.437          10.77     
C          Citigroup                     57.28          0.368      1.16/ 0.51    2.01           12.454    
BAC        Bank of America               21.84          0.316      2.22/ 0.9     1.879          15.976    
XOM        ExxonMobil                    87.48          0.291      0.87/ 0.47    0.519          40.956    
CVX        Chevron Corp                  113.25         0.06       1.53/ 0.99    0.863          None      
Z74.SI     Singapore Telecomms           3.77           0.09      -0.0/-0.0      0.382          15.772    
3395.KL    Berjaya Corporation Berhad    0.325          0.0        0.58/ 0.32    0.478          None      


Recommendation
===============
QAF Sell. Current:1.32 (1.15) 
TheHourGlass Buy. Current:0.67 (0.7) 
SIA Buy. Current:9.8 (10.0) 
DBS Bank Sell. Current:17.97 (17.0) 
UOB Bank Sell. Current:20.78 (20.0) 
Citigroup Sell. Current:57.28 (48.0) 
Bank of America Sell. Current:21.84 (17.0) 
Bank of America Check. Hist Perf:2.22 (7.6%) 
Chevron Corp Check. Hist Perf:1.53 (3.6%) 

Time Finished: 20161206 07:45.39Time Now: 20161206 19:40.27
24 tickers loaded


Starting status check...
Ticker     Name                          Last           STDMov    Hist Perf      Beta           PE Ratio  
^STI       Straits Times Index           2949.12        0.68      nan/nan        1.0            None      
^KLSE      FTSE Bursa Malaysia           1629.73        0.039     nan/nan        1.0            None      
^GSPC      S&P 500 Index                 2204.71        0.452     nan/nan        1.0            None      
Q01.SI     QAF                           1.315          -0.1      -0.53/-0.3     0.365          9.135     
AGS.SI     TheHourGlass                  0.67           0.0       -0.17/-0.11    0.193          9.71      
RW0U.SI    Mapletree G China             0.965          0.0       -0.5/-0.45     0.691          6.78      
D5IU.SI    Lippo Malls                   0.37           -0.537    -0.26/-0.25    0.259          None      
ES3.SI     Straits Times ETF             3.0            0.606      0.22/ 0.03    0.897          None      
500.SI     Tai Sin Electric              0.375          0.0        0.33/ 0.07    0.257          6.818     
573.SI     Challenger Techonologies      0.465          0.0       -0.64/-0.74    0.429          9.49      
C6L.SI     SIA                           9.73           -0.209    -1.04/-0.66    0.234          13.979    
E5H.SI     Golden Agri                   0.43           -0.385    -1.0/-0.38     1.082          15.93     
J91U.SI    Cambridge Industrial Trust    0.54           0.0        0.06/-0.14    0.73           None      
O23.SI     OSIM Intl                     1.39           0.0       -0.71/-0.75    1.296          None      
D05.SI     DBS Bank                      18.16          0.222      0.75/ 0.25    1.279          10.621    
O39.SI     OCBC Bank                     9.27           0.372     -0.26/-0.29    1.294          10.711    
U11.SI     UOB Bank                      20.83          0.085      0.66/ 0.45    1.053          11.217    
1163.KL    Allianz Malaysia              9.6            -0.062    -1.41/-0.8     0.437          10.759    
C          Citigroup                     57.28          0.0        1.16/ 0.51    2.01           12.46     
BAC        Bank of America               21.84          0.0        2.22/ 0.9     1.879          15.98     
XOM        ExxonMobil                    87.48          -0.0       0.87/ 0.47    0.519          40.96     
CVX        Chevron Corp                  113.25         0.0        1.53/ 0.99    0.863          None      
Z74.SI     Singapore Telecomms           3.74           -0.181    -0.3/-0.2      0.382          15.646    
3395.KL    Berjaya Corporation Berhad    0.33           0.772      1.09/ 0.58    0.478          None      


Recommendation
===============
QAF Sell. Current:1.315 (1.15) 
TheHourGlass Buy. Current:0.67 (0.7) 
SIA Buy. Current:9.73 (10.0) 
DBS Bank Sell. Current:18.16 (17.0) 
UOB Bank Sell. Current:20.83 (20.0) 
Citigroup Sell. Current:57.28 (48.0) 
Bank of America Sell. Current:21.84 (17.0) 
Bank of America Check. Hist Perf:2.22 (7.6%) 
Chevron Corp Check. Hist Perf:1.53 (3.6%) 

Time Finished: 20161206 19:45.37