Time Now: 20170301 07:34.23
24 tickers loaded


Starting status check...
Ticker     Name                          Last           STDMov    Hist Perf      Beta           PE Ratio  
^STI       Straits Times Index           3096.61        -0.159    nan/nan        1.0            None      
^KLSE      FTSE Bursa Malaysia           1693.77        -0.002    nan/nan        1.0            None      
^GSPC      S&P 500 Index                 2363.64        -0.143    nan/nan        1.0            None      
Q01.SI     QAF                           1.4            0.0       -4.46/-3.02    0.284          9.79      
AGS.SI     TheHourGlass                  0.68           0.0        1.41/ 0.84    0.076          10.0      
RW0U.SI    Mapletree G China             0.985          0.0        0.43/ 0.21    0.474          6.9       
D5IU.SI    Lippo Malls                   0.39           0.0        0.03/ 0.02    0.08           38.5      
ES3.SI     Straits Times ETF             3.1            -0.312    -0.6/-0.53     0.878          None      
500.SI     Tai Sin Electric              0.425          0.0        2.31/ 1.78    0.215          7.456     
573.SI     Challenger Techonologies      0.48           0.0        0.33/ 0.25    0.06           12.31     
C6L.SI     SIA                           9.95           0.0        0.35/ 0.36    0.292          16.28     
E5H.SI     Golden Agri                   0.38           0.0       -1.21/-0.68    1.215          14.07     
J91U.SI    Cambridge Industrial Trust    0.575          0.0       -1.92/-1.03    0.444          115.0     
O23.SI     OSIM Intl                     1.39           0.0        0.19/ 0.12    0.585          None      
D05.SI     DBS Bank                      18.73          0.0        0.4/ 0.2      1.129          11.12     
O39.SI     OCBC Bank                     9.46           0.0       -0.69/-0.42    1.311          11.51     
U11.SI     UOB Bank                      21.5           0.0       -0.17/-0.09    1.196          None      
1163.KL    Allianz Malaysia              11.34          0.0        0.6/ 0.46     0.574          12.71     
C          Citigroup                     59.81          -0.122    -0.56/-0.25    1.984          12.576    
BAC        Bank of America               24.68          0.126     -0.16/-0.07    1.922          16.222    
XOM        ExxonMobil                    81.32          -0.067     0.25/ 0.11    0.652          43.083    
CVX        Chevron Corp                  112.5          0.282      0.94/ 0.48    0.627          None      
Z74.SI     Singapore Telecomms           3.94           0.0       -0.64/-0.39    0.632          16.42     
3395.KL    Berjaya Corporation Berhad    0.375          0.0       -0.71/-0.36    0.561          None      


Recommendation
===============
QAF Sell. Current:1.4 (1.15) 
QAF Check. Hist Perf:-4.46 (-11.1%) 
TheHourGlass Buy. Current:0.68 (0.7) 
Lippo Malls Sell. Current:0.39 (0.38) 
Tai Sin Electric Check. Hist Perf:2.31 (4.9%) 
SIA Buy. Current:9.95 (10.0) 
Cambridge Industrial Trust Check. Hist Perf:-1.92 (-3.4%) 
DBS Bank Sell. Current:18.73 (17.0) 
UOB Bank Sell. Current:21.5 (20.0) 
Citigroup Sell. Current:59.81 (48.0) 
Bank of America Sell. Current:24.68 (17.0) 

Time Finished: 20170301 07:39.26Time Now: 20170301 19:32.13
24 tickers loaded


Starting status check...
Ticker     Name                          Last           STDMov    Hist Perf      Beta           PE Ratio  
^STI       Straits Times Index           3122.77        0.187     nan/nan        1.0            None      
^KLSE      FTSE Bursa Malaysia           1697.69        0.132     nan/nan        1.0            None      
^GSPC      S&P 500 Index                 2363.64        0.0       nan/nan        1.0            None      
Q01.SI     QAF                           1.41           0.127     -4.29/-2.89    0.284          9.86      
AGS.SI     TheHourGlass                  0.68           0.0        1.39/ 0.81    0.076          10.0      
RW0U.SI    Mapletree G China             0.99           0.317      0.49/ 0.24    0.474          6.935     
D5IU.SI    Lippo Malls                   0.395          0.518      0.61/ 0.36    0.08           38.994    
ES3.SI     Straits Times ETF             3.14           0.63      -0.02/-0.01    0.878          None      
500.SI     Tai Sin Electric              0.42           -0.378     1.67/ 1.24    0.215          7.368     
573.SI     Challenger Techonologies      0.48           0.0        0.31/ 0.2     0.06           12.31     
C6L.SI     SIA                           9.92           -0.294    -0.0/-0.01     0.292          16.231    
E5H.SI     Golden Agri                   0.38           0.0       -1.52/-0.85    1.215          14.07     
J91U.SI    Cambridge Industrial Trust    0.58           0.247     -1.62/-0.89    0.444          116.0     
O23.SI     OSIM Intl                     1.39           0.0       -0.0/-0.0      0.585          None      
D05.SI     DBS Bank                      18.82          0.162      0.17/ 0.08    1.129          11.173    
O39.SI     OCBC Bank                     9.53           0.319     -0.96/-0.58    1.311          11.595    
U11.SI     UOB Bank                      21.56          0.179     -0.64/-0.29    1.196          None      
1163.KL    Allianz Malaysia              11.32          -0.032     0.41/ 0.32    0.551          12.688    
C          Citigroup                     59.81          -0.0      -0.56/-0.25    1.971          12.72     
BAC        Bank of America               24.605         -0.09     -0.26/-0.11    1.92           16.33     
XOM        ExxonMobil                    81.32          0.0        0.25/ 0.11    0.633          43.44     
CVX        Chevron Corp                  112.5          0.0        0.95/ 0.48    0.591          None      
Z74.SI     Singapore Telecomms           3.95           0.092     -0.54/-0.33    0.642          16.462    
3395.KL    Berjaya Corporation Berhad    0.385          0.432      0.11/ 0.13    0.548          None      


Recommendation
===============
QAF Sell. Current:1.41 (1.15) 
QAF Check. Hist Perf:-4.29 (-10.5%) 
TheHourGlass Buy. Current:0.68 (0.7) 
Lippo Malls Sell. Current:0.395 (0.38) 
Tai Sin Electric Check. Hist Perf:1.67 (3.7%) 
SIA Buy. Current:9.92 (10.0) 
Golden Agri Check. Hist Perf:-1.52 (-5.0%) 
Cambridge Industrial Trust Check. Hist Perf:-1.62 (-2.5%) 
DBS Bank Sell. Current:18.82 (17.0) 
UOB Bank Sell. Current:21.56 (20.0) 
Citigroup Sell. Current:59.81 (48.0) 
Bank of America Sell. Current:24.605 (17.0) 

Time Finished: 20170301 19:37.03