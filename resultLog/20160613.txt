Time Now: 20160613 07:33.31
12 tickers loaded


Starting status check...
Ticker     Name                          Last           STDMov    Hist Perf      Beta           PE Ratio  
^STI       Straits Times Index           2822.97        0.0       nan/nan        1.0            None      
Q01.SI     QAF                           1.11           0.0        0.6/ 0.4      0.449          11.21     
AGS.SI     TheHourGlass                  0.81           0.0        0.6/ 0.3      0.461          10.95     
RW0U.SI    Mapletree G China             0.995          0.0        0.77/ 0.64    0.747          6.378     
D5IU.SI    Lippo Malls                   0.335          0.0        0.63/ 0.45    0.53           None      
ES3.SI     Straits Times ETF             2.87           0.0       -0.15/-0.08    0.925          None      
500.SI     Tai Sin Electric              0.335          0.0        0.59/ 0.49    0.477          8.171     
573.SI     Challenger Techonologies      0.45           0.0        0.34/ 0.24    0.349          8.49      
C6L.SI     SIA                           10.57          0.0       -0.25/-0.13    0.401          15.39     
E5H.SI     Golden Agri                   0.38           0.0       -0.23/-0.1     1.125          63.33     
J91U.SI    Cambridge Industrial Trust    0.545          0.0        0.53/ 0.36    0.822          None      
O23.SI     OSIM Intl                     1.39           0.0        0.06/ 0.04    1.13           22.79     


Recommendation
===============

Time Finished: 20160613 07:35.37Time Now: 20160613 19:35.55
12 tickers loaded


Starting status check...
Ticker     Name                          Last           STDMov    Hist Perf      Beta           PE Ratio  
^STI       Straits Times Index           2783.28        -0.652    nan/nan        1.0            None      
Q01.SI     QAF                           1.09           -0.816     0.12/ 0.1     0.449          11.008    
AGS.SI     TheHourGlass                  0.8            -0.43      0.34/ 0.21    0.461          10.815    
RW0U.SI    Mapletree G China             0.995          0.0        1.23/ 1.08    0.747          6.378     
D5IU.SI    Lippo Malls                   0.33           -0.688     0.34/ 0.24    0.53           None      
ES3.SI     Straits Times ETF             2.83           -0.852    -0.34/-0.15    0.925          None      
500.SI     Tai Sin Electric              0.335          0.0        0.83/ 0.73    0.477          8.171     
573.SI     Challenger Techonologies      0.45           0.0        0.47/ 0.39    0.349          8.49      
C6L.SI     SIA                           10.61          0.101      0.12/ 0.1     0.401          15.448    
E5H.SI     Golden Agri                   0.365          -0.651    -0.78/-0.33    1.125          60.83     
J91U.SI    Cambridge Industrial Trust    0.55           0.519      1.46/ 1.0     0.822          None      
O23.SI     OSIM Intl                     1.39           0.0        0.33/ 0.2     1.13           22.79     


Recommendation
===============

Time Finished: 20160613 19:38.02