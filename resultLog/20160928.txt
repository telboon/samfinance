Time Now: 20160928 07:39.48
24 tickers loaded


Starting status check...
Ticker     Name                          Last           STDMov    Hist Perf      Beta           PE Ratio  
^STI       Straits Times Index           2860.23        -0.071    nan/nan        1.0            None      
^KLSE      FTSE Bursa Malaysia           1664.72        -0.304    nan/nan        1.0            None      
^GSPC      S&P 500 Index                 2159.93        0.552     nan/nan        1.0            None      
Q01.SI     QAF                           1.24           0.0       -1.35/-0.84    0.491          9.61      
AGS.SI     TheHourGlass                  0.68           0.0       -0.29/-0.13    0.442          9.58      
RW0U.SI    Mapletree G China             1.105          0.0        0.84/ 0.73    0.737          7.674     
D5IU.SI    Lippo Malls                   0.37           0.0        0.03/ 0.02    0.551          None      
ES3.SI     Straits Times ETF             2.91           0.557      0.98/ 0.62    0.918          None      
500.SI     Tai Sin Electric              0.37           0.0       -0.45/-0.35    0.473          6.98      
573.SI     Challenger Techonologies      0.485          0.0       -0.27/-0.17    0.283          8.981     
C6L.SI     SIA                           10.62          0.0        0.18/ 0.1     0.39           12.87     
E5H.SI     Golden Agri                   0.36           0.0        0.04/ 0.02    1.135          40.0      
J91U.SI    Cambridge Industrial Trust    0.545          0.0        0.9/ 0.61     0.822          None      
O23.SI     OSIM Intl                     1.39           0.0        0.03/ 0.02    1.075          22.79     
D05.SI     DBS Bank                      15.3           0.0        0.59/ 0.29    1.244          8.94      
O39.SI     OCBC Bank                     8.59           0.0       -0.04/-0.02    1.185          9.92      
U11.SI     UOB Bank                      18.8           0.0       -0.06/-0.04    1.082          9.74      
1163.KL    Allianz Malaysia              10.1           0.0       -0.73/-0.47    0.311          11.32     
C          Citigroup                     46.37          0.254     -0.32/-0.16    1.781          9.812     
BAC        Bank of America               15.29          0.216     -0.66/-0.3     1.544          12.818    
XOM        ExxonMobil                    83.24          0.05       0.01/-0.0     0.696          33.122    
CVX        Chevron Corp                  98.98          0.083     -0.19/-0.13    1.143          None      
Z74.SI     Singapore Telecomms           3.99           0.222      0.41/ 0.3     0.559          16.423    
3395.KL    Berjaya Corporation Berhad    0.335          0.0       -0.5/-0.32     0.787          None      


Recommendation
===============
QAF Sell. Current:1.24 (1.15) 
TheHourGlass Buy. Current:0.68 (0.7) 
Mapletree G China Sell. Current:1.105 (1.0) 

Time Finished: 20160928 07:44.53Time Now: 20160928 19:32.04
24 tickers loaded


Starting status check...
Ticker     Name                          Last           STDMov    Hist Perf      Beta           PE Ratio  
^STI       Straits Times Index           2858.01        -0.106    nan/nan        1.0            None      
^KLSE      FTSE Bursa Malaysia           1664.96        -0.289    nan/nan        1.0            None      
^GSPC      S&P 500 Index                 2159.93        0.552     nan/nan        1.0            None      
Q01.SI     QAF                           1.23           -0.183    -1.68/-1.05    0.491          9.533     
AGS.SI     TheHourGlass                  0.675          -0.111    -0.59/-0.28    0.442          9.51      
RW0U.SI    Mapletree G China             1.11           0.172      1.06/ 0.93    0.737          7.709     
D5IU.SI    Lippo Malls                   0.375          0.513      0.56/ 0.4     0.551          None      
ES3.SI     Straits Times ETF             2.9            -0.287     0.43/ 0.28    0.918          None      
500.SI     Tai Sin Electric              0.375          0.362      0.04/ 0.04    0.473          7.074     
573.SI     Challenger Techonologies      0.485          0.0       -0.26/-0.16    0.283          8.981     
C6L.SI     SIA                           10.55          -0.343    -0.08/-0.04    0.39           12.785    
E5H.SI     Golden Agri                   0.365          0.691      0.39/ 0.19    1.135          40.556    
J91U.SI    Cambridge Industrial Trust    0.545          0.0        0.93/ 0.63    0.822          None      
O23.SI     OSIM Intl                     1.39           0.0        0.04/ 0.03    1.075          22.79     
D05.SI     DBS Bank                      15.22          -0.174     0.29/ 0.15    1.244          8.893     
O39.SI     OCBC Bank                     8.6            0.055      0.14/ 0.08    1.185          9.932     
U11.SI     UOB Bank                      18.8           0.0       -0.01/-0.01    1.082          9.74      
1163.KL    Allianz Malaysia              10.12          0.142     -0.6/-0.4      0.311          11.342    
C          Citigroup                     46.37          0.0       -0.32/-0.16    1.781          9.71      
BAC        Bank of America               15.29          0.0       -0.66/-0.3     1.544          12.65     
XOM        ExxonMobil                    83.24          0.0        0.01/-0.0     0.696          32.99     
CVX        Chevron Corp                  98.98          -0.0      -0.19/-0.13    1.143          None      
Z74.SI     Singapore Telecomms           4.0            0.296      0.51/ 0.36    0.559          16.465    
3395.KL    Berjaya Corporation Berhad    0.335          0.0       -0.51/-0.33    0.787          None      


Recommendation
===============
QAF Sell. Current:1.23 (1.15) 
QAF Check. Hist Perf:-1.68 (-3.9%) 
TheHourGlass Buy. Current:0.675 (0.7) 
Mapletree G China Sell. Current:1.11 (1.0) 

Time Finished: 20160928 19:37.19