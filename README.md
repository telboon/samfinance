Sam Finance
===========


The goal of this tool is to have a financial framework for analysis of equity holdings regularly. Ideally, this file should be opened every day to update the database and to run the desired analysis/algo.

The whole project is ran under a virtualenv. It ensures the portability of the project.

Do note there's dependancies softwares necessary to run this. Remember to download them!

Author:
Samuel Pua
kahkin@gmail.com
https://www.samuelpua.com
