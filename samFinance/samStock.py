#! /usr/bin/python3
# virtualenv
#
# Dependancies listed in dependancies.txt


from openpyxl import load_workbook, Workbook
from yahoo_finance import Share
from datetime import datetime
from datetime import timedelta
import time
import requests
from yahoo_quote_download.yahoo_quote_download import yqd

class priceData:
   def __init__(self, dates, openP, closeP, highP, lowP, vol, eps):
      self.dates=dates
      self.openP=float(openP)
      self.closeP=float(closeP)
      self.highP=float(highP)
      self.lowP=float(lowP)
      self.vol=float(vol)
      if eps!=None:
         self.eps=float(eps)
      else:
         self.eps=None

class stock:
   def __init__(self, ticker):
      self.ticker=ticker
      self.filename="data/"+ticker+".xlsx"
      self.datas=[]
      self.offsetRead=1
      self.startDate=datetime(2010,1,1)
      self.lastPrice=0
      self.loadFile()

   #function to get list of close prices
   def getClose(self, period=None):
      answer=[]
      if period==None:
         startRan=0
      else:
         startRan=len(self.datas)-period

      for i in range(startRan, len(self.datas)):
         answer.append(self.datas[i].closeP)
      
      return answer

   #function to get list of dates
   def getDates(self, period=None):
      answer=[]
      if period==None:
         startRan=0
      else:
         startRan=len(self.datas)-period

      for i in range(startRan, len(self.datas)):
         answer.append(self.datas[i].dates)
      
      return answer

   def loadFile(self):
      try:
         self.wb=load_workbook(filename=self.filename)
         self.fileExist=True
      except:
         #file does not exist.
         #creating new file and format
         print(self.filename+" does not exist. Creating file...")
         self.wb=Workbook()
         self.ws=self.wb.create_sheet(title="data",index=0)
         self.fileExist=False

      #if file exist, download from Excel file
      #date, open, close, high, low
      if self.fileExist:
         #declare variables for reading
         self.ws=self.wb.get_sheet_by_name("data")

         #reading each value until empty cell
         while self.ws["A1"].offset(self.offsetRead,0).value!=None:
            self.dates=(self.ws["A1"].offset(self.offsetRead,0).value)
            self.openP=(self.ws["A1"].offset(self.offsetRead,1).value)
            self.closeP=(self.ws["A1"].offset(self.offsetRead,2).value)
            self.highP=(self.ws["A1"].offset(self.offsetRead,3).value)
            self.lowP=(self.ws["A1"].offset(self.offsetRead,4).value)
            self.vol=(self.ws["A1"].offset(self.offsetRead,5).value)
            self.eps=(self.ws["A1"].offset(self.offsetRead,6).value)

            self.datas.append(priceData(self.dates,self.openP,self.closeP,self.highP,self.lowP,self.vol, self.eps))
            self.offsetRead+=1

         #global variable
         self.lastPrice=self.ws["J1"].offset(0,1).value
         if self.lastPrice==None:
            if len(self.datas)>0:
               self.lastPrice=float(self.datas[-1].closeP)
            else:
               self.lastPrice=0
         self.lastPrice=float(self.lastPrice)

      #creating & restarting headers
      self.ws["A1"].offset(0,0).value="Date"
      self.ws["A1"].offset(0,1).value="Open"
      self.ws["A1"].offset(0,2).value="Close"
      self.ws["A1"].offset(0,3).value="High"
      self.ws["A1"].offset(0,4).value="Low"
      self.ws["A1"].offset(0,5).value="Volume"
      self.ws["A1"].offset(0,6).value="EPS"

      #global variable
      self.ws["J1"].offset(0,0).value="Last Price"

         #debug print
         #print(self.filename+": "+str(self.offsetRead-1)+" lines read")
   
   #for updating prices from Yahoo Finance
   def update(self, maxTries=2, wait=60):
      #create yahoo finance Share data
      self.shares=Share(self.ticker)

      #update last price
      # old method -- not as updated
      # self.lastPrice=float(self.shares.get_price())

      #new method of updating last price
      tempLast=requests.get("http://finance.yahoo.com/d/quotes.csv?s="+self.ticker+"&f=l1").text
      tempLast=tempLast[0:len(tempLast)-1]
      self.lastPrice=float(tempLast)

      #get cookie just in case of error
      yqd._get_cookie_crumb()

      self.tempData=[]
      if self.offsetRead>10:
      #records exist
         if datetime.now()>self.datas[self.offsetRead-2].dates:
            self.downloadCheck=False

            #try to download until successful
            downloadCounter=0
            while downloadCounter<maxTries: #change to 1 from 5
               try:
                  #old code failed
                  #self.tempData=self.shares.get_historical(self.datas[self.offsetRead-2].dates.strftime("%Y-%m-%d"),datetime.now().strftime("%Y-%m-%d"))
                  #new replacement
                  self.tempData=yqd.load_yahoo_quote(self.ticker,self.datas[self.offsetRead-2].dates.strftime("%Y%m%d"), datetime.now().strftime("%Y%m%d"))
                  self.downloadCheck=True
                  downloadCounter=9999
               except:
                  downloadCounter+=1
                  self.downloadCheck=False
                  print(self.ticker+" download failed. Retrying in "+str(wait)+" sec...")

                  #reset cookie
                  yqd.cookier = yqd.urllib.request.HTTPCookieProcessor()
                  yqd.opener = yqd.urllib.request.build_opener(yqd.cookier)
                  yqd.urllib.request.install_opener(yqd.opener)
                  yqd._get_cookie_crumb()
                  time.sleep(wait)

      else:
      #no records
      #start from startDate
         self.offsetRead=1
         downloadCounter=0
         while downloadCounter<maxTries: #change to 1 from 5
            try:
               #old code failed
               #self.tempData=self.shares.get_historical(self.startDate.strftime("%Y-%m-%d"),datetime.now().strftime("%Y-%m-%d"))
               self.tempData=yqd.load_yahoo_quote(self.ticker,self.startDate.strftime("%Y%m%d"), datetime.now().strftime("%Y%m%d"))
               self.downloadCheck=True
               downloadCounter=9999
            except:
               downloadCounter+=1
               self.downloadCheck=False
               print(self.ticker+" download failed. Retrying in "+str(wait)+" sec...")

               #reset cookie
               yqd.cookier = yqd.urllib.request.HTTPCookieProcessor()
               yqd.opener = yqd.urllib.request.build_opener(yqd.cookier)
               yqd.urllib.request.install_opener(yqd.opener)
               yqd._get_cookie_crumb()
               time.sleep(wait)

      #check if download fails all the time
      if downloadCounter==maxTries:
         return False

      #update datas
      self.curr=0

      for self.i in range(1,len(self.tempData)):

         #try for data error
         try:
            #if file is new
            if (not(self.fileExist) or len(self.datas)==0):
               try:
                  dataError=False
                  self.dates=datetime.strptime(self.tempData[self.i].split(",")[0],"%Y-%m-%d")
                  self.openP=float(self.tempData[self.i].split(",")[1])
                  self.closeP=float(self.tempData[self.i].split(",")[4])
                  self.highP=float(self.tempData[self.i].split(",")[2])
                  self.lowP=float(self.tempData[self.i].split(",")[3])
                  self.vol=float(self.tempData[self.i].split(",")[6])
               except:
                  dataError=True

               if not dataError:
                  self.datas.append(priceData(self.dates,self.openP,self.closeP,self.highP,self.lowP,self.vol,None))
                  self.curr+=1

            #update latest pricing to EOD
            elif self.datas[-1].dates==datetime.strptime(self.tempData[self.i].split(",")[0],"%Y-%m-%d"):
               try:
                  dataError=False
                  self.dates=datetime.strptime(self.tempData[self.i].split(",")[0],"%Y-%m-%d")
                  self.openP=float(self.tempData[self.i].split(",")[1])
                  self.closeP=float(self.tempData[self.i].split(",")[4])
                  self.highP=float(self.tempData[self.i].split(",")[2])
                  self.lowP=float(self.tempData[self.i].split(",")[3])
                  self.vol=float(self.tempData[self.i].split(",")[6])
               except:
                  dataError=True

               if not dataError:
                  self.datas.pop(-1)
                  self.datas.append(priceData(self.dates,self.openP,self.closeP,self.highP,self.lowP,self.vol,None))

            #if date does not already exist
            elif self.datas[-1].dates<datetime.strptime(self.tempData[self.i].split(",")[0],"%Y-%m-%d"):
               try:
                  dataError=False
                  self.dates=datetime.strptime(self.tempData[self.i].split(",")[0],"%Y-%m-%d")
                  self.openP=float(self.tempData[self.i].split(",")[1])
                  self.closeP=float(self.tempData[self.i].split(",")[4])
                  self.highP=float(self.tempData[self.i].split(",")[2])
                  self.lowP=float(self.tempData[self.i].split(",")[3])
                  self.vol=float(self.tempData[self.i].split(",")[6])
               except:
                  dataError=True

               if not dataError:
                  self.datas.append(priceData(self.dates,self.openP,self.closeP,self.highP,self.lowP,self.vol,None))
                  self.curr+=1
         except:
            None

      #new requirement -- Yahoo Finance down at about May 17
      if len(self.datas)==0 or self.datas[-1].dates.strftime("%Y-%m-%d")!=datetime.now().strftime("%Y-%m-%d"):
         self.datas.append(priceData(datetime.strptime(datetime.now().strftime("%Y-%m-%d"),"%Y-%m-%d"),self.lastPrice,self.lastPrice,self.lastPrice,self.lastPrice,0,None))
         self.curr+=1
      else:
         self.datas[-1]=priceData(datetime.strptime(datetime.now().strftime("%Y-%m-%d"),"%Y-%m-%d"),self.lastPrice,self.lastPrice,self.lastPrice,self.lastPrice,0,None)
      #save eps data if exist
      try:
         self.datas[-1].eps=float(self.lastPrice)/float(self.shares.get_price_earnings_ratio())
      except:
         pass
      #debug print # of data updated
      #print(str(self.curr)+" datasets updated")

      for self.i in range(0,self.curr):
         self.ws["A1"].offset(self.offsetRead+self.i,0).value=self.datas[self.offsetRead+self.i-1].dates
         self.ws["A1"].offset(self.offsetRead+self.i,1).value=self.datas[self.offsetRead+self.i-1].openP
         self.ws["A1"].offset(self.offsetRead+self.i,2).value=self.datas[self.offsetRead+self.i-1].closeP
         self.ws["A1"].offset(self.offsetRead+self.i,3).value=self.datas[self.offsetRead+self.i-1].highP
         self.ws["A1"].offset(self.offsetRead+self.i,4).value=self.datas[self.offsetRead+self.i-1].lowP
         self.ws["A1"].offset(self.offsetRead+self.i,5).value=self.datas[self.offsetRead+self.i-1].vol
         self.ws["A1"].offset(self.offsetRead+self.i,6).value=self.datas[self.offsetRead+self.i-1].eps

      #global variable
      self.ws["J1"].offset(0,1).value=self.lastPrice
      self.fileExist=False

      return True


   #update last price only
   def updateLastP(self):
      #create yahoo finance Share data
      #self.shares=Share(self.ticker)

      #update last price
      # old method of getting last price
      # self.lastPrice=float(self.shares.get_price())

      # new method of updating last price
      tempLast=requests.get("http://finance.yahoo.com/d/quotes.csv?s="+self.ticker+"&f=l1").text
      tempLast=tempLast[0:len(tempLast)-1]
      self.lastPrice=float(tempLast)

      #update in file
      self.ws["J1"].offset(0,1).value=self.lastPrice

   def fixStock(self,lastDays=91):
      startDate=datetime.now()-timedelta(days=lastDays)
      #get cookie just in case of error
      yqd._get_cookie_crumb()
      self.tempData=yqd.load_yahoo_quote(self.ticker,startDate.strftime("%Y%m%d"), datetime.now().strftime("%Y%m%d"))

      #go through full datas
      datasCount=0
      tempCount=1
      while (datasCount<len(self.datas)) and (tempCount<len(self.tempData)):
         if self.datas[datasCount].dates<datetime.strptime(self.tempData[tempCount].split(",")[0],"%Y-%m-%d"):
            datasCount+=1
         elif self.datas[datasCount].dates==datetime.strptime(self.tempData[tempCount].split(",")[0],"%Y-%m-%d"):
            datasCount+=1
            tempCount+=1
         elif self.datas[datasCount].dates>datetime.strptime(self.tempData[tempCount].split(",")[0],"%Y-%m-%d"):
            tempCount+=1
            try:
               dataError=False
               self.dates=datetime.strptime(self.tempData[tempCount].split(",")[0],"%Y-%m-%d")
               self.openP=float(self.tempData[tempCount].split(",")[1])
               self.closeP=float(self.tempData[tempCount].split(",")[4])
               self.highP=float(self.tempData[tempCount].split(",")[2])
               self.lowP=float(self.tempData[tempCount].split(",")[3])
               self.vol=float(self.tempData[tempCount].split(",")[6])
            except:
               dataError=True

            if not dataError:
               self.datas.insert(datasCount,priceData(self.dates,self.openP,self.closeP,self.highP,self.lowP,self.vol,None))

   #finally saving the file
   def save(self):
      for self.i in range(0,len(self.datas)):
         self.ws["A1"].offset(1+self.i,0).value=self.datas[self.i].dates
         self.ws["A1"].offset(1+self.i,1).value=self.datas[self.i].openP
         self.ws["A1"].offset(1+self.i,2).value=self.datas[self.i].closeP
         self.ws["A1"].offset(1+self.i,3).value=self.datas[self.i].highP
         self.ws["A1"].offset(1+self.i,4).value=self.datas[self.i].lowP
         self.ws["A1"].offset(1+self.i,5).value=self.datas[self.i].vol
         self.ws["A1"].offset(1+self.i,6).value=self.datas[self.i].eps
      time.sleep(3)
      self.wb.save(self.filename)


