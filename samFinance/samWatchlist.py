#! /usr/bin/python3
# 
# main program to keep track of watchlist

import os
import time
from openpyxl import Workbook
from openpyxl import load_workbook
from .samStock import stock
import samFinance
import numpy as np
from datetime import datetime
from datetime import timedelta

class watchTarget:
   def __init__(self, ticker, name, qty, targetBuy, targetSell, sensitivity, benchmarkTicker):
      self.ticker=ticker
      self.name=name
      self.qty=qty
      try:
         self.targetBuy=float(targetBuy)
      except:
         self.targetBuy=None

      try:
         self.targetSell=float(targetSell)
      except:
         self.targetSell=None

      try:
         self.sensitivity=float(sensitivity)
      except:
         self.sensitivity=0.05

      self.benchmarkTicker=benchmarkTicker

class watchList:
   def __init__(self, filename):
      self.filename=filename+".xlsx"
      self.port=[]
      self.loadFile()
      self.email=""

   # checks & prints status -- possible output to email
   def status(self,indicators=""):
      myIndic=indicators

      #reset indicator status
      betaOn=False
      newBetaOn=False
      PEOn=False
      STDOn=False
      targetOn=False
      histOn=False

      # header with names
      answer=""
      answer+="Ticker"
      answer+=" " * 5
      answer+="Name"
      answer+=" " * 26
      answer+="Last"
      answer+=" " * 11

      html=""
      html+="<table style=\"width:100%; border:1px solid black; border-collapse:collapse;\"><tr>\n"
      html+="<th style=\"border:1px solid black; padding:5px;\">Ticker</th>\n"
      html+="<th style=\"border:1px solid black; padding:5px;\">Name</th>\n"
      html+="<th style=\"border:1px solid black; padding:5px;\">Last</th>\n"

      #finding custom indicators
      while myIndic!="":
         if myIndic.find("Beta")==0:
            myIndic=myIndic[4:]
            betaOn=True

         if myIndic.find("Newbeta")==0:
            myIndic=myIndic[7:]
            newBetaOn=True

         if myIndic.find("Pe")==0:
            myIndic=myIndic[2:]
            PEOn=True

         if myIndic.find("Stdmov")==0:
            myIndic=myIndic[6:]
            STDOn=True

         if myIndic.find("Target")==0:
            myIndic=myIndic[6:]
            targetOn=True

         if myIndic.find("Histperf")==0:
            myIndic=myIndic[8:]
            histOn=True

      if targetOn:
         answer+="Target Buy"
         answer+=" " * 5
         answer+="Target Sell"
         answer+=" " * 4

      #add header for STDMov
      if STDOn:
         answer+="STDMov"
         html+="<th style=\"border:1px solid black; padding:5px;\">STDMov</th>\n"
         answer+=" " * 4

      #add header for historial performance
      if histOn:
         answer+="Hist Perf"
         html+="<th style=\"border:1px solid black; padding:5px;\">Hist Perf</th>\n"
         answer+=" " * 6

      #add header for beta
      if betaOn:
         answer+="Beta"
         html+="<th style=\"border:1px solid black; padding:5px;\">Beta</th>\n"
         answer+=" " * 11

      #add header for new beta (13 weeks)
      if newBetaOn:
         answer+="Beta (13 wk)"
         html+="<th style=\"border:1px solid black; padding:5px;\">Beta (13 wk)</th>\n"
         answer+=" " * 2

      #add header for PE Ratio
      if PEOn:
         answer+="PE Ratio"
         html+="<th style=\"border:1px solid black; padding:5px;\">PE Ratio</th>\n"
         answer+=" " * 2

      #finish off header
      answer+="\n"
      html+="</tr>\n"
   
      #filling content of each stocks
      for i in range(len(self.port)):
         tempStock=stock(self.port[i].ticker)
         answer+=self.port[i].ticker+" " * (11-len(self.port[i].ticker))
         answer+=self.port[i].name+" " * (30 - len(self.port[i].name))
         answer+=str(tempStock.lastPrice)+" " * (15 - len(str(tempStock.lastPrice)))

         html+="<tr>\n"
         html+="<td style=\"border:1px solid black; padding:5px;\">\n"+self.port[i].ticker+"</td>"
         html+="<td style=\"border:1px solid black; padding:5px;\">\n"+self.port[i].name+"</td>"
         html+="<td style=\"border:1px solid black; padding:5px;\">\n"+str(tempStock.lastPrice)+"</td>"

         if targetOn:
            answer+=str(self.port[i].targetBuy)+" " * (15 - len(str(self.port[i].targetBuy)))
            answer+=str(self.port[i].targetSell)+" " * (15 - len(str(self.port[i].targetSell)))
            html+="<td style=\"border:1px solid black; padding:5px;\">\n"+str(self.port[i].targetBuy)+"</td>"
            html+="<td style=\"border:1px solid black; padding:5px;\">\n"+str(self.port[i].targetSell)+"</td>"
         
         #adding content for beta
         if STDOn:
            try:
               workingMov=tempStock.lastPrice-tempStock.datas[-2].closeP
            except:
               print(tempStock.ticker)
               workingMov=0
            workingSTD=np.std(tempStock.getClose()[-61:])
            STDMov=workingMov/workingSTD
            STDMov=round(STDMov,3)
            answer+=str(STDMov)+" "*(10 - len(str(STDMov)))
            html+="<td style=\"border:1px solid black; padding:5px;\">\n"+str(STDMov)+"</td>"

         currBenchmark=stock(self.port[i].benchmarkTicker)
         if histOn:
            histPerf=round(samFinance.algo.histPerf(tempStock,currBenchmark, period=52),2)
            histPerf13=round(samFinance.algo.histPerf(tempStock,currBenchmark, interval=30, period=12),2)
            space1=""
            space2=""
            if histPerf>0:
               space1=" "
            if histPerf13>0:
               space2=" "
            tempAns=space1+str(histPerf)+"/"+space2+str(histPerf13) 
            answer+=tempAns+" "* (15 - len(tempAns))
            html+="<td style=\"border:1px solid black; padding:5px;\">\n"+tempAns+"</td>"
         
         if betaOn:
            workingBeta=round(samFinance.algo.beta(tempStock, currBenchmark, period=52),3)
            answer+=str(workingBeta)+" " * (15 - len(str(workingBeta)))
            html+="<td style=\"border:1px solid black; padding:5px;\">\n"+str(workingBeta)+"</td>"
         if newBetaOn:
            workingBeta=round(samFinance.algo.beta(tempStock, currBenchmark, period=13),3)
            answer+=str(workingBeta)+" " * (15 - len(str(workingBeta)))
            html+="<td style=\"border:1px solid black; padding:5px;\">\n"+str(workingBeta)+"</td>"
         if PEOn:
            if tempStock.datas[-1].eps!=None:
               workingPE=round(tempStock.lastPrice/tempStock.datas[-1].eps,3)
            else:
               workingPE=None
            answer+=str(workingPE)+" " * (10 - len(str(workingPE)))
            html+="<td style=\"border:1px solid black; padding:5px;\">\n"+str(workingPE)+"</td>"

         #finish off each entry
         answer+="\n"
         html+="</tr>\n"

      #close off html
      html+="</body></html>\n"
      print(answer)
      return html, answer

   # check portfolio against buy/sell targets
   # function to watch out for target buy and target sell
   # create msg for email
   def checkTargets(self):
      endMsg=""
      for p in range(len(self.port)):
         tempStock=stock(self.port[p].ticker)
         i=self.port[p]
         
         lastPrice=tempStock.lastPrice

         if i.targetBuy!=None:
            if lastPrice<=i.targetBuy:
               endMsg+=i.name+" Buy. Current:"+str(lastPrice)+" ("+str(i.targetBuy)+") \n"
         if i.targetSell!=None:
            if lastPrice>=i.targetSell:
               endMsg+=i.name+" Sell. Current:"+str(lastPrice)+" ("+str(i.targetSell)+") \n"

         #checking single day move if exceeds sensitivity
         dailyMove=lastPrice/tempStock.datas[-2].closeP-1
         if abs(dailyMove)>=i.sensitivity:
            endMsg+=i.name+" Check. Day Delta:"+str(round(dailyMove*100,1))+"% ("+str(round(i.sensitivity*100,1))+"%) \n"

         #checking single day movements if exceeds 1.5 STDEV
         try:
            workingMov=tempStock.lastPrice-tempStock.datas[-2].closeP
         except:
            workingMov=0
         workingSTD=np.std(tempStock.getClose()[-61:])
         STDMov=workingMov/workingSTD
         if abs(STDMov)>=1.5:
            endMsg+=i.name+" Check. Day STD:"+str(round(STDMov,2))+" ("+str(round(workingMov*100,1))+"%) \n"

         currBenchmark=stock(self.port[p].benchmarkTicker)
         #checking if histPerf exceeds 1.5 STDEV
         histPerf=samFinance.algo.histPerf(tempStock,currBenchmark, period=52)
         weekMov=tempStock.lastPrice/tempStock.datas[samFinance.algo.sDLookup(tempStock,datetime.now()-timedelta(days=7))].closeP-1
         if (abs(histPerf)>=1.5) and (tempStock.ticker!=currBenchmark.ticker):
            endMsg+=i.name+" Check. Hist Perf:"+str(round(histPerf,2))+" ("+str(round(weekMov*100,1))+"%) \n"

      print(endMsg)
      HTMLMsg=endMsg.replace("\n","<br>\n")
      return HTMLMsg, endMsg


   def add(self, ticker, name, qty, targetBuy, targetSell):
      self.port.append(watchTarget(ticker, name, qty, targetBuy, targetSell))

   def remove(self, ticker):
      i=0
      delCount=0
      while i<len(self.port):
         if self.port[i]==ticker:
            self.port.pop(i)
            delCount+=1
         else:
            i+=1
      return delCount

   def save(self):
      #update format
      self.ws["A1"].value="Market Participation"
      self.ws["A2"].value="Index Ticker"

      self.ws["A6"].value="Ticker"
      self.ws["B6"].value="Name"
      self.ws["C6"].value="Quantity"
      self.ws["D6"].value="Buy Target"
      self.ws["E6"].value="Sell Target"
      self.ws["F6"].value="Sensitivity"
      self.ws["G6"].value="Benchmark"
      
      #update content
      self.ws["B1"].value=self.parti
      self.ws["B2"].value=self.marketTicker


      #update individual stocks
      for self.i in range(0,len(self.port)):
         self.ws["A6"].offset(self.i+1,0).value=self.port[self.i].ticker
         self.ws["A6"].offset(self.i+1,1).value=self.port[self.i].name
         self.ws["A6"].offset(self.i+1,2).value=self.port[self.i].qty
         self.ws["A6"].offset(self.i+1,3).value=self.port[self.i].targetBuy
         self.ws["A6"].offset(self.i+1,4).value=self.port[self.i].targetSell
         self.ws["A6"].offset(self.i+1,5).value=self.port[self.i].sensitivity
         self.ws["A6"].offset(self.i+1,6).value=self.port[self.i].benchmarkTicker

      #time to save
      self.wb.save(self.filename)

   def loadFile(self):
      #clear first before loading
      self.port=[]

      try:
         self.wb=load_workbook(filename=self.filename)
         self.fileExist=True
      except:
         self.fileExist=False

      #load data if portfolio exists
      #ask for data if it doesn't
      if self.fileExist:
         self.ws=self.wb.get_sheet_by_name("portfolio")
         self.entryC=0

         #loading global variable
         self.parti=self.ws["B1"].value
         self.marketTicker=self.ws["B2"].value
         self.marketStock=stock(self.marketTicker)

         #loading individual stocks
         while self.ws["A6"].offset(self.entryC+1,0).value!=None:
            self.ticker=self.ws["A6"].offset(self.entryC+1,0).value
            self.name=self.ws["A6"].offset(self.entryC+1,1).value
            self.qty=self.ws["A6"].offset(self.entryC+1,2).value
            self.targetBuy=self.ws["A6"].offset(self.entryC+1,3).value
            self.targetSell=self.ws["A6"].offset(self.entryC+1,4).value
            self.sensitivity=self.ws["A6"].offset(self.entryC+1,5).value
            self.benchmarkTicker=self.ws["A6"].offset(self.entryC+1,6).value

            #add to data
            self.port.append(watchTarget(self.ticker, self.name, self.qty, self.targetBuy, self.targetSell, self.sensitivity, self.benchmarkTicker))

            self.entryC+=1

         print(str(self.entryC)+" tickers loaded")

      #file does not exist. create file format
      else:
         self.wb=Workbook()
         self.ws=self.wb.create_sheet(title="portfolio",index=0)
         self.wb.save(self.filename)

   def update(self, maxTries=2, wait=60):
      answer=""
      for self.i in range(0,len(self.port)):
         tempStock=stock(self.port[self.i].ticker)
         tempStockSuccess=False

         tryCount=0
         while tryCount<1: #change to 1 from 5
            try:
               #print("Update: "+self.port[self.i].ticker)
               tempStockSuccess=tempStock.update(maxTries, wait)
               tryCount=99999
            except:
               tryCount+=1

         if not(tempStockSuccess):
            answer+=tempStock.ticker+" download failed after "+str(maxTries)+" tries.\n"
         
         #finally saving each stock
         tempStock.save()

      return answer

      ############removed update of all benchmarks -- just make sure they are included as stocks###
      #for self.i in range(0,len(self.port)):
      #   tempStock=stock(self.port[self.i].benchmarkTicker)

      #   tryCount=0
      #   while tryCount<1: #change to 1 from 5
      #      try:
      #         #print("Update: "+self.port[self.i].ticker)
      #         tempStock.update()
      #         tryCount=99999
      #      except:
      #         tryCount+=1
      #   tempStock.save()

      tryCount=0
      while tryCount<1: #change to 1 from 5
         try:
            self.marketStock.update()
            tryCount=99999
         except:
            tryCount+=1
      self.marketStock.save()

   def updateLastP(self):
      for self.i in range(0,len(self.port)):
         tempStock=stock(self.port[self.i].ticker)

         tryCount=0
         while tryCount<5:
            try:
               #print("Update: "+self.port[self.i].ticker)
               tempStock.updateLastP()
               tryCount=99999
            except:
               tryCount+=1
         tempStock.save()

         tryCount=00
         tempStock=stock(self.port[self.i].benchmarkTicker)
         while tryCount<5:
            try:
               #print("Update: "+self.port[self.i].ticker)
               tempStock.updateLastP()
               tryCount=99999
            except:
               tryCount+=1
         tempStock.save()


      tryCount=0
      while tryCount<5:
         try:
            self.marketStock.updateLastP()
            tryCount=99999
         except:
            tryCount+=1
      self.marketStock.save()
         
   def fixWL(self, lastDays=91):
      for i in self.port:
         print("Fixing "+i.ticker)
         tempStock=stock(i.ticker)
         tempStock.fixStock(lastDays)
         tempStock.save()

   def chartUpdate(self, startDate=None, endDate=None):
      for i in range(len(self.port)):
         tempStock=stock(self.port[i].ticker)
         chartTitle=self.port[i].name+" ("+self.port[i].ticker+")"
         samFinance.algo.plotPriceChart(tempStock, title=chartTitle, startDate=startDate, endDate=endDate)
