#! /usr/bin/python3
#
# email library within samFinance

from smtplib import SMTP
from smtplib import SMTP_SSL
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from datetime import datetime
from getpass import getpass

defaultLogin=input("Type in SMTP user: ")
defaultPass=getpass("Type in SMTP password: ")
defaultAddr=input("Type in recipent email: ")
defaultServ="samuelpua.com"
defaultFrom=input("Type in sender email: ")

class sendmail():

   def __init__():
      pass

   def send(html, address=defaultAddr, fromAddr=defaultFrom, server=defaultServ, login=defaultLogin, password=defaultPass, text=""):
      if text=="":
         text=html
      mail=SMTP(server,587)
      mail.starttls()
      mail.login(login, password)

      msg=MIMEMultipart("alternative")
      msg["Subject"]="SamFinance: "+str(datetime.now())
      msg["From"]=fromAddr
      msg["To"]=address

      part1=MIMEText(text, "plain")
      part2=MIMEText(html, "html")

      msg.attach(part1)
      msg.attach(part2)

      mail.send_message(msg)

   #test username & password
   def test(address=defaultAddr, fromAddr=defaultFrom, server=defaultServ, login=defaultLogin, password=defaultPass):
      mail=SMTP(server, 587)
      mail.starttls()
      mail.login(login, password)
      
      print("\nEmail Server Login Successful!\n")

