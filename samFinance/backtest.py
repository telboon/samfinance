from . import samFinance
import pprint
import statistics
import numpy
import datetime

#this class is for single action -- useless unless consolidated
class action:
   def __init__(self, timestamp,ticker, action, price, qty):
      self.timestamp=timestamp
      self.ticker=ticker
      self.action=action  # 1=buy; -1=sell
      self.price=price
      self.qty=qty

#this class is used for keeping all actions used by strategy
class histAct:
   def __init__(self):
      self.history=[]
      self.totalTickers=[]
      self.totalHoldings=[]

   def add(self, act):
      self.history.append(act)

   def len(self):
      return len(self.history)

   #show total earnings
   def calcResults(self):
      self.totalTickers=[]
      self.totalHoldings=[]
      totalFunds=0
      for i in range(len(self.history)):
         if self.history[i].ticker in self.totalTickers:
            currChange=self.history[i].action*self.history[i].qty
            self.totalHoldings[self.totalTickers.index(self.history[i].ticker)]+=currChange
            totalFunds+=-1*currChange*self.history[i].price
         else:
            currChange=self.history[i].action*self.history[i].qty
            self.totalTickers.append(self.history[i].ticker)
            self.totalHoldings.append(currChange)
            totalFunds+=-1*currChange*self.history[i].price

      return totalFunds

   #show holdings
   def showHoldings(self):
      self.totalTickers=[]
      self.totalHoldings=[]
      totalFunds=0
      for i in range(len(self.history)):
         if self.history[i].ticker in self.totalTickers:
            currChange=self.history[i].action*self.history[i].qty
            self.totalHoldings[self.totalTickers.index(self.history[i].ticker)]+=currChange
            totalFunds+=-1*currChange*self.history[i].price
         else:
            currChange=self.history[i].action*self.history[i].qty
            self.totalTickers.append(self.history[i].ticker)
            self.totalHoldings.append(currChange)
            totalFunds+=-1*currChange*self.history[i].price

      return self.totalTickers, self.totalHoldings

   #statistics for holdings
   def stats(self, startDate=None, endDate=None):
      if len(self.history)==0:
         return False
      if startDate==None:
         startDate=self.history[0].timestamp
      if endDate==None:
         endDate=self.history[-1].timestamp

      self.calcResults()
      tempHoldings=[0]*len(self.totalHoldings)
      tempPrice=[0]*len(self.totalHoldings)

      #start of tracking portfolio money
      bank=[0]
      workingAction=0
      currBank=0

      #start going through dates
      workingDate=startDate
      while workingDate<=endDate:
         #going through all actions for the day
         while workingAction<len(self.history) and self.history[workingAction].timestamp<=workingDate:
            currBank-=self.history[workingAction].qty*self.history[workingAction].price*self.history[workingAction].action
            tempPrice[self.totalTickers.index(self.history[workingAction].ticker)]=self.history[workingAction].price
            tempHoldings[self.totalTickers.index(self.history[workingAction].ticker)]+=self.history[workingAction].qty*self.history[workingAction].action
            workingAction+=1

         #added EOD money * worth
         bank.append(currBank + samFinance.algo.sumproduct(tempPrice, tempHoldings))
         workingDate+=datetime.timedelta(days=1)

      #create bankChange
      bankChange=[]
      for i in range(len(bank)-1):
         bankChange.append(bank[i+1]-bank[i])

      fakeBankChange=[]

      for i in range(len(bankChange)):
         if bankChange[i]!=0:
            fakeBankChange.append(bankChange[i])

      #time to work on statistics
      print("***************** Statistics for daily changes *************************")
      print("Start Date: "+str(startDate))
      print("End Date: "+str(endDate))
      print("Mean: "+str(statistics.mean(bankChange)))
      print("Stdev: "+str(statistics.stdev(bankChange)))
      print("Count: "+str(len(fakeBankChange)))
      print("Number of Trades: "+str(len(self.history)))
      print("10%tile: "+str(numpy.percentile(fakeBankChange,10)))
      print("25%tile: "+str(numpy.percentile(fakeBankChange,25)))
      print("50%tile: "+str(numpy.percentile(fakeBankChange,50)))
      print("75%tile: "+str(numpy.percentile(fakeBankChange,75)))
      print("90%tile: "+str(numpy.percentile(fakeBankChange,90)))
      print("***************** Statistics for daily changes *************************")

      #done and return full population
      return bank

#all strategies goes in this class
class strategy:
   def __init__(self, holdings=0, trans=10000):
      self.holdings=holdings
      self.trans=trans
      self.history=histAct()

   def reset(self, holdings=0, trans=10000):
      self.holdings=holdings
      self.trans=trans
      self.history=histAct()

   def histPerfNeg(self, workingStock, indexStock, startDate=None, endDate=None):
      histPerfResults=0
      if len(workingStock.datas)==0 or len(indexStock.datas)==0:
         return 0

      if startDate==None:
         startDate=max(workingStock.datas[0].dates,indexStock.datas[0].dates)
      if endDate==None:
         endDate=min(workingStock.datas[-1].dates,indexStock.datas[-1].dates)


      #### Start doing work ###
      workingDate=startDate
      cycleCounter=0

      while workingDate<=endDate:
         cycleCounter+=1
         if cycleCounter==30:
            cycleCounter=0

         histIndic=samFinance.algo.histPerf(workingStock, indexStock, bEndDate=workingDate)
         if histIndic<-1.5:
            ########## Data for buying ######################
            stockPrice=samFinance.algo.sPLookup(workingStock, workingDate)
            qty=round(self.trans/stockPrice,0)
            histPerfResults-=stockPrice*qty
            self.history.add(action(workingDate, workingStock.ticker, 1, stockPrice, qty))
            ########## Data for buying ######################
            ########## Data for selling ######################
            workingDate+=datetime.timedelta(days=7)
            stockPrice=samFinance.algo.sPLookup(workingStock, workingDate)
            histPerfResults+=stockPrice*qty
            self.history.add(action(workingDate, workingStock.ticker, -1, stockPrice, qty))
            ########## Data for selling ######################
         workingDate+=datetime.timedelta(days=1)

      #done with daily cycles
      self.holdings+=histPerfResults
      return histPerfResults
   def histPerfPos(self, workingStock, indexStock, startDate=None, endDate=None):
      histPerfResults=0
      if len(workingStock.datas)==0 or len(indexStock.datas)==0:
         return 0

      if startDate==None:
         startDate=max(workingStock.datas[0].dates,indexStock.datas[0].dates)
      if endDate==None:
         endDate=min(workingStock.datas[-1].dates,indexStock.datas[-1].dates)


      #### Start doing work ###
      workingDate=startDate
      cycleCounter=0

      while workingDate<=endDate:
         cycleCounter+=1
         if cycleCounter==30:
            cycleCounter=0

         histIndic=samFinance.algo.histPerf(workingStock, indexStock, bEndDate=workingDate)
         if histIndic>1.5:
            ########## Data for buying ######################
            stockPrice=samFinance.algo.sPLookup(workingStock, workingDate)
            qty=round(self.trans/stockPrice,0)
            histPerfResults-=stockPrice*qty
            self.history.add(action(workingDate, workingStock.ticker, 1, stockPrice, qty))
            ########## Data for buying ######################
            ########## Data for selling ######################
            workingDate+=datetime.timedelta(days=7)
            stockPrice=samFinance.algo.sPLookup(workingStock, workingDate)
            histPerfResults+=stockPrice*qty
            self.history.add(action(workingDate, workingStock.ticker, -1, stockPrice, qty))
            ########## Data for selling ######################
         workingDate+=datetime.timedelta(days=1)

      #done with daily cycles
      self.holdings+=histPerfResults
      return histPerfResults
