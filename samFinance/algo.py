#! /usr/bin/python3
#
# class to store functions for algo
#
# 

from .samStock import stock
from .samWatchlist import watchList
from datetime import timedelta
from datetime import datetime
from scipy import stats
import numpy as np
from openpyxl import load_workbook
import copy
import matplotlib

import matplotlib.pyplot as plt

class indicator:
   def __init__(self, myStock=None, startDate=None, endDate=None):
      self.dates=[]
      self.indicator=[]
      if myStock!=None:
         for i in range(range(myStock.datas)):
            if (startDate==None or myStock.datas[i].dates>startDate):
               if (endDate==None or myStock.datas[i].dates<endDate):
                  self.add(myStock.datas[i].dates, myStock.datas[i].closeP)

   def add(self, plusDate, plusIndicator):
      self.dates+=[plusDate]
      self.indicator+=[plusIndicator]

def plotDist(myList, num=None, binSize=50, title=""):
   f, ax1=plt.subplots()
   ax1.hist(myList, bins=binSize, normed=True, histtype='stepfilled')

   ax1.set_title(title)

   if num!=None:
      ax2=ax1.twinx()
      ax2.plot([num,num], [0.5,1], '-r')

   plt.show()


def plotPriceChart(myStock, indicator=None, startDate=None, endDate=None, binSize=50, title=None, filename=None):
   #if start & end date does not exist, create them -- plot full chart
   if startDate==None:
      startDate=myStock.datas[0].dates
   if endDate==None:
      endDate=myStock.datas[len(myStock.datas)-1].dates

   stockDate=myStock.getDates()
   stockClose=myStock.getClose()

   indicators=copy.deepcopy(indicator)

   #pop the ones we don't need
   i=0
   while i < (len(stockDate)):
      if stockDate[i]<startDate or stockDate[i]>endDate:
         stockDate.pop(i)
         stockClose.pop(i)
      else:
         i+=1

   if indicator!=None:
      i=0
      while i < (len(indicators.dates)):
         if indicators.dates[i]<startDate or indicators.dates[i]>endDate:
            indicators.dates.pop(i)
            indicators.indicator.pop(i)
         else:
            i+=1
   
   #time to plot with subplots, twinx and show
   try:
      f, axarr=plt.subplots(2)
   except:
      plt.switch_backend("Agg")
      f, axarr=plt.subplots(2)

   #make title if title is not defined
   if title==None:
      title=myStock.ticker

   axarr[0].set_title(title)

   axarr[0].plot(stockDate, stockClose)

   #plot indicator
   if indicator!=None:
      ax2=axarr[0].twinx()
      ax2.plot(indicators.dates, indicators.indicator,'-r')

   #plot histogram
   axarr[1].hist(stockClose, bins=binSize, normed=True, histtype='stepfilled')

   #plot latest price in histogram
   ax2=axarr[1].twinx()
   ax2.plot([myStock.lastPrice,myStock.lastPrice], [0.5,1], '-r')

   #plot png
   if filename==None:
      filename="graphs/"+myStock.ticker+".png"
   plt.savefig(filename)

   #show everything (or at least try to)
   try:
      plt.show()
   except:
      pass

#convert prices to return
def convertReturn(myStock,startDate=None, endDate=None, interval=7):
   #make a series of prices
   if startDate==None:
      startDate=myStock.datas[0].dates

   if endDate==None:
      endDate=myStock.datas[-1].dates

   dates=[]
   ans=[]

   #initiate start date
   bWorking=startDate
   tempBef=0.0
   tempAft=0.0

   while bWorking+timedelta(days=interval)<=endDate:
      tempBef=myStock.datas[sDLookup(myStock,bWorking)].closeP
      tempAft=myStock.datas[sDLookup(myStock,bWorking+timedelta(days=interval))].closeP
      try:
         dates.append(bWorking)
         ans.append(tempAft/tempBef-1)
      except:
         if tempBef==0 and tempAft==0:
            ans.append(0)
         elif tempBef==0:
            ans.append(9999.9)
         else:
            ans.append(0)
            print("Weird error in calculation of return")
            print(tempBef)
            print(tempAft)
      #next round
      bWorking+=timedelta(days=1)

   #return tuple of stuff
   return dates, ans
      

def plotBetaIndicator(myStock, myIndex, pStartDate=None, pEndDate=None, pInterval=7, pPeriod=52):
   if pStartDate==None:
      startDate=max(myStock.datas[0].dates, myIndex.datas[0].dates)
   else:
      startDate=pStartDate

   #set end date as min of user defined, and the last date of Stocks and Index
   if pEndDate!=None:
      endDate=min(myStock.datas[len(myStock.datas)-1].dates, myIndex.datas[len(myIndex.datas)-1].dates, pEndDate)
   else:
      endDate=min(myStock.datas[len(myStock.datas)-1].dates, myIndex.datas[len(myIndex.datas)-1].dates)

   #run beta for all grades if dates exists
   workDate=startDate
   answer=indicator()
   while workDate<=endDate:
      if min(myStock.datas[0].dates, myIndex.datas[0].dates)+timedelta(days=pInterval*pPeriod)>workDate:
         answer.add(workDate, 0)
      else:
         answer.add(workDate, beta(myStock, myIndex, bEndDate=workDate, interval=pInterval, period=12))

      workDate+=timedelta(days=1)

   return answer

#data retrieval by date
def sDLookup(mystock, fDate):
   startFind=mystock.datas[0].dates
   
   working=0
   while working+1<len(mystock.datas) and mystock.datas[working].dates < fDate:
      working+=1

   if mystock.datas[working].dates > fDate and working>0:
      working-=1
   return working

#returns price of stock
def sPLookup(mystock, fDate):
   pos=sDLookup(mystock, fDate)
   return mystock.datas[pos].closeP

#beta calculator 
def beta(myStock, indexStock, bStartDate=None, bEndDate=None, interval=7, period=0):

   if len(myStock.datas)==0 or len(indexStock.datas)==0:
      return 0
   else:
      tolDays=period*interval

      if bStartDate==None:
         #determine start and end date for calculation of beta
         if myStock.datas[0].dates<indexStock.datas[0].dates:
            startDate=indexStock.datas[0].dates
         else:
            startDate=myStock.datas[0].dates
      else:
         startDate=bStartDate

      if bEndDate==None:
         #determine end date
         if myStock.datas[len(myStock.datas)-1].dates>indexStock.datas[len(indexStock.datas)-1].dates:
            endDate=indexStock.datas[len(indexStock.datas)-1].dates
         else:
            endDate=myStock.datas[len(myStock.datas)-1].dates
      else:
         endDate=bEndDate

      if tolDays!=0:
         startDate=endDate-timedelta(days=tolDays)

      #create data set for regression
      myS=convertReturn(myStock, startDate, endDate, interval)[1]
      indexS=convertReturn(indexStock, startDate, endDate, interval)[1]

      betaReg=stats.linregress(indexS, myS)

      return betaReg.slope

def rSq(myStock, indexStock, bStartDate=None, bEndDate=None, interval=7, period=0):

   if len(myStock.datas)==0 or len(indexStock.datas)==0:
      return 0
   else:
      tolDays=period*interval

      if bStartDate==None:
         #determine start and end date for calculation
         if myStock.datas[0].dates<indexStock.datas[0].dates:
            startDate=indexStock.datas[0].dates
         else:
            startDate=myStock.datas[0].dates
      else:
         startDate=bStartDate

      if bEndDate==None:
         #determine end date
         if myStock.datas[len(myStock.datas)-1].dates>indexStock.datas[len(indexStock.datas)-1].dates:
            endDate=indexStock.datas[len(indexStock.datas)-1].dates
         else:
            endDate=myStock.datas[len(myStock.datas)-1].dates
      else:
         endDate=bEndDate

      if tolDays!=0:
         startDate=endDate-timedelta(days=tolDays)

      #create data set for regression
      myS=convertReturn(myStock, startDate, endDate, interval)[1]
      indexS=convertReturn(indexStock, startDate, endDate, interval)[1]

      linReg=stats.linregress(indexS, myS)

      return linReg.rvalue*linReg.rvalue

def fileToTicker(universeFile):
   #clear first before loading
   universeOut=[]
   nameOut=[]

   try:
      wb=load_workbook(filename=universeFile+".xlsx")
      fileExist=True
   except:
      fileExist=False

   #load data if portfolio exists
   if fileExist:
      ws=wb.get_sheet_by_name("portfolio")
      entryC=0

      #loading individual stocks
      while ws["A6"].offset(entryC+1,0).value!=None:
         ticker=ws["A6"].offset(entryC+1,0).value
         name=ws["A6"].offset(entryC+1,1).value
         entryC+=1
         universeOut.append(ticker)
         nameOut.append(name)

      print(str(entryC)+" tickers loaded")

   else:
      print("File does not exist")
   return universeOut, nameOut

def getKey(sortList):
   return sortList[1]

def sortByValue(name, value, reverse=False):
   newList=[]
   for i in range(len(name)):
      newList.append([name[i], value[i]])

   newList=sorted(newList, key=getKey, reverse=reverse)

   for i in range(len(newList)):
      name[i]=newList[i][0]
      value[i]=newList[i][1]

def correlAnalysis(myStock, universe, bStartDate=None, bEndDate=None, interval=7, period=0):

   rSqList=[]
   betaList=[]

   #obtaining ticker for universe
   universeList, nameList=fileToTicker(universe)

   #calculate beta & rSq
   for i in range(len(universeList)):
      workingStock=stock(universeList[i])
      rSqList.append(rSq(myStock, workingStock, bStartDate, bEndDate, interval, period))
      betaList.append(beta(myStock, workingStock, bStartDate, bEndDate, interval, period))

   #sort by r square for name (proxy soort for rSq)
   workingList=list(rSqList)
   sortByValue(universeList, workingList, reverse=True)

   #sort by rsquare for name (proxy)
   workingList=list(rSqList)
   sortByValue(nameList, workingList, reverse=True)

   #sort by rSq for beta
   sortByValue(betaList, rSqList, reverse=True)

   #prints top 3 correlation
   for i in range(4):
      print(nameList[i])
      print(str(round(rSqList[i]*100,1))+"%")
      print(round(betaList[i],2))
      print()

def histPerf(myStock, indexStock, bStartDate=None, bEndDate=None, interval=7, period=52, debug=False):

   if len(myStock.datas)==0 or len(indexStock.datas)==0:
      return 0
   else:
      tolDays=period*interval

      if bStartDate==None:
         #determine start and end date for calculation
         if myStock.datas[0].dates<indexStock.datas[0].dates:
            startDate=indexStock.datas[0].dates
         else:
            startDate=myStock.datas[0].dates
      else:
         startDate=bStartDate

      if bEndDate==None:
         #determine end date
         if myStock.datas[len(myStock.datas)-1].dates>indexStock.datas[len(indexStock.datas)-1].dates:
            endDate=indexStock.datas[len(indexStock.datas)-1].dates
         else:
            endDate=myStock.datas[len(myStock.datas)-1].dates
      else:
         endDate=bEndDate

      if tolDays!=0:
         startDate=endDate-timedelta(days=tolDays)

      #create data set for regression
      myS=convertReturn(myStock, startDate, endDate, interval)[1]
      indexS=convertReturn(indexStock, startDate, endDate, interval)[1]
      testS=convertReturn(indexStock, startDate, endDate, interval)[0]

      linReg=stats.linregress(indexS, myS)

      estErrList=[]
      for i in range(len(myS)):
         if debug:
            print(str(i)+": "+str(testS[i]))
            print(str(i)+": "+str(indexS[i]))
            print(str(i)+": "+str(myS[i]))
         estErrList.append(myS[i]-(linReg.slope*indexS[i]+linReg.intercept))
      estErr=np.std(estErrList)

      lastReturn=sPLookup(myStock, endDate)/sPLookup(myStock, endDate-timedelta(days=interval))-1
      lastMktReturn=sPLookup(indexStock, endDate)/sPLookup(indexStock, endDate-timedelta(days=interval))-1

      #Choose either principle -- include intercept or not
      #estReturn=linReg.slope*lastMktReturn+linReg.intercept
      estReturn=linReg.slope*lastMktReturn
      weekErr=lastReturn-estReturn

      #for debug
      if debug:
         print("Last Return: "+str(lastReturn))
         print("Last Mkt Return: "+str(lastMktReturn))
         print("Stderr: "+str(linReg.stderr))
         print("y="+str(linReg.slope)+"x+"+str(linReg.intercept))
         print("R2: "+str(linReg.rvalue * linReg.rvalue))
         print("est Return: "+str(estReturn))

      return weekErr/estErr

      #return linReg.rvalue*linReg.rvalue

def sumproduct(a,b):
   total=0
   if len(a)!=len(b):
      return False
   else:
      for i in range(len(a)):
         total+=a[i]*b[i]
   return total
